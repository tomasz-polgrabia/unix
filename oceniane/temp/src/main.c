#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include <signal.h>
#include "lib.h"

volatile sig_atomic_t sigDestroy = 0;
volatile sig_atomic_t destroyed = 0;
volatile sig_atomic_t finished = 0;
volatile sig_atomic_t *childs = NULL;
volatile sig_atomic_t n = 0;
volatile sig_atomic_t saper = 0;

void init()
{
	struct timeval t;
	gettimeofday (&t, NULL);
	srand(t.tv_usec+getpid()+getppid());
}

void terrorystaHandler(int nr)
{
	debug("Signal %d received\n", nr);
	if (nr == sigDestroy)
	{
		debug("Bomb destroyed\n");
		exit(0);
	}
}

void proxyHandler(int nr)
{
	debug ("I have got signal %d\n", nr);

	switch (nr)
	{
		case SIGCHLD:
			{
				debug("SIGCHLD\n");
				pid_t pid;
				for (;;) {
				pid = waitpid(0, NULL, WNOHANG);
				if (pid > 0)
				{
					debug("Child process %d finished\n", pid);
					kill (saper, SIGUSR1);
					if (++finished >= n)
						kill(saper, SIGTERM);
				}

				if (0 == pid) return;
				if (0 >= pid) {
				if (ECHILD == errno) return;
					fprintf (stderr, "waitpid error %s\n", strerror(errno));
					}
				}
			}

			break;
	default:
		if (SIGRTMIN <= nr && nr <= SIGRTMAX)
		{
			for (int i = 0; i < n; i++)
			{
				if (childs[i] > 0)
					kill(childs[i], nr);
			}
		}
		break;
	}

}

void saperHandler(int nr)
{
	debug("Saper signal received %d\n", nr);
	switch (nr)
	{
		case SIGUSR1:
			printf("YEEEEEEEEEEEEEEEE: I have destroyed terrorist\n");
			++destroyed;
			break;
		case SIGTERM:
			debug("Proxy request to die\n");
			printf ("I have destroyed %d terrorists\n", destroyed);
			exit(0);
			break;
	}
}

int terrorystaMain()
{
	debug("Terrorysta launched\n");
	init();

	sigDestroy  = SIGRTMIN + rand()%(SIGRTMAX-SIGRTMIN+1);
	assert (SIGRTMIN <= sigDestroy && sigDestroy <= SIGRTMAX);

	printf("[%d]Numer sygnalu do rozbrojenia to %d\n", getpid(), sigDestroy);

	for (int i = SIGRTMIN; i<=SIGRTMAX; i++)
	{
		if (setHandler(i, terrorystaHandler, 0) <0)
		{
			perror("setHandler");
			return 1;
		}
	}

	struct timespec t;
	memset(&t, 0, sizeof(t));

	t.tv_sec = 10;

	SafeSleep2(t);

	printf ("KABOOM\n");

	debug("Terrorysta exited with success\n");
	return 1;
}

int saperMain()
{
	debug("Saper launched\n");
	struct timespec t;

	memset (&t, 0, sizeof(t));
	t.tv_sec = 1;
	union sigval val;
	memset (&val, 0, sizeof(val));

	if (setHandler (SIGTERM, saperHandler, 0) < 0)
	{
		perror("sethandler");
		return 1;
	}

	if (setHandler (SIGUSR1, saperHandler, 0) < 0)
	{
		perror("setHandler");
		return 1;
	}

	for (int i = 0; i < 10; i++)
	{
		int nr = SIGRTMIN + rand()%(SIGRTMAX-SIGRTMIN+1);
		assert (SIGRTMIN <= nr && nr <= SIGRTMAX);
		printf("Sending signal %d to parent \n", nr);
		sigqueue(getppid(), nr, val);
		SafeSleep2(t);
	}

	printf ("I have killed %d terrorists\n", destroyed);

	debug("Saper exited\n");
	return 0;
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		fprintf(stderr, "Nie podales parametru n\n");
		return 1;
	}

	n = atoi(argv[1]);
	childs = (sig_atomic_t *)malloc(sizeof(sig_atomic_t)*n);
	assert (childs != NULL);
	memset ((void *)childs, 0, sizeof(childs));
	debug("Parametr wejsciowy n to %d\n", n);

	for (int i = SIGRTMIN; i <= SIGRTMAX; i++)
	{
		if (setHandler (i, proxyHandler, 0) < 0)
		{
			perror("setHandler");
			return 1;
		}
	}

	setHandler (SIGCHLD, proxyHandler, 0);

	debug("Uruchamiam terrorystow\n");

	for (int i = 0; i < n; i++)
	{
		pid_t child = fork();
		if (child < 0)
		{
			fprintf(stderr, "Fork failed with error code %d (%s)\n", errno, strerror(errno));
			return 1;
		}

		if (child == 0)
		{
			exit(terrorystaMain());
			return 1;
		}
		printf ("%d.te dziecko %d\n", i, child);

		childs[i] = child;
	}

	debug("Uruchamiam sapera\n");

	saper = fork();
	if (saper < 0)
	{
		fprintf(stderr, "Fork failed with error code %d (%s)\n", errno, strerror(errno));
		return 1;
	}

	if (saper == 0)
		exit(saperMain());

	debug("Czekam az dzieci zakoncza sie\n");

	pid_t pid;
	for (;;) {
		TEMP_FAILURE_RETRY(pid = waitpid(0, NULL, 0));
		if (0 >= pid) {
			if (ECHILD == errno) break;
			fprintf (stderr, "waitpid failed with error %s\n",
					strerror(errno));
			return 1;
		}
	 }

	debug("Waiting finished quitting\n");

	return 0;
}
