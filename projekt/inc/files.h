#ifndef __FILES_H__
#define __FILES_H__

#define _GNU_SOUCE

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

int SafeOpen(const char *pathname, int flags, mode_t mode);
int SafeClose(int fd);
void SafeFree(void *ptr);

#endif
