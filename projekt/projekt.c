#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <aio.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>

#define BLOCK_SIZE 5
#define ALIGN(value) ((value-1)/BLOCK_SIZE)+1;

sig_atomic_t writes = 0;
sig_atomic_t reads = 0;
sig_atomic_t last = -1;
sig_atomic_t LastBlock = -1;

int SafeOpen(const char *pathname, int flags, mode_t mode)
{
	return TEMP_FAILURE_RETRY(open(pathname, flags, mode));
}

int SafeClose(int fd)
{
	return TEMP_FAILURE_RETRY(close(fd));
}

void SafeFree(void *ptr)
{
	if (ptr != NULL)
		free(ptr);
}

/*
void dump(char *s, int length)
{
	for (int i = 0; i < length-1; i++)
		printf ("0x%x ", s[i]);
	printf ("0x%x\n", s[length-1]);
}
*/

sig_atomic_t enqueue = 1;

int handleInterrupt(struct aiocb** blocks)
{
		if (last == SIGRTMIN)
		{
				char *buff = (char *)blocks[LastBlock][2].aio_buf;
				char *buff1 = (char *)blocks[LastBlock][0].aio_buf;
				char *buff2 = (char *)blocks[LastBlock][1].aio_buf;

				for (int i = 0; i < BLOCK_SIZE; i++)
				{
					char c1 = buff1[i];
					char c2 = buff2[BLOCK_SIZE - i - 1];
					buff[i] = c1 ^ c2;
				}
/*				
				printf ("LastBlock: %d, Blocks 1: %s, 2: %s\n", 
						LastBlock,
						(char *)blocks[LastBlock][0].aio_buf,
						(char *)blocks[LastBlock][1].aio_buf);
				printf ("LastBlock: %d, offset: %d, Buff: ", (int)LastBlock,
						(int)blocks[LastBlock][2].aio_offset);
	*/			
				// dump(buff, BLOCK_SIZE);

				struct aiocb *tab2[1] = { &blocks[LastBlock][2] };
				struct sigevent event;
				
				memset (&event, 0, sizeof(event));
				
				event.sigev_notify = SIGEV_SIGNAL;
				event.sigev_signo = SIGRTMIN+3;
				event.sigev_value.sival_ptr = blocks;
				
				if (lio_listio(LIO_NOWAIT, tab2, 1, &event) < 0)
				{
					perror("lio_listio");
					exit(1);
				}
				++writes;				

		}
		return 0;
}

int executeIo(	int *ileZakolejkowano, struct aiocb **blocks,
	int blocks3)
{
	// executing aio
	struct sigevent event;
	memset (&event, 0, sizeof(event));
	event.sigev_notify = SIGEV_SIGNAL;
	event.sigev_signo = SIGRTMIN;
	*ileZakolejkowano = 0;
	
	 // sleep(5);	
	for (int i = 0; i < blocks3 && enqueue; i++)
	{
		// printf ("Enqueuing %d\n", i);		
		struct aiocb *tab[2] = { &blocks[i][0], &blocks[i][1] };
		event.sigev_value.sival_ptr = blocks[i];
		
		if (lio_listio(LIO_NOWAIT, tab, 2, &event) < 0)
		{
			perror("lio_listio");
			return 1;
		}
		
		++reads;
		++*ileZakolejkowano;
		// sleep(5);		
	}	
	
	return 0;
}

void handleInterrupts(sigset_t *setWaiting, struct aiocb** blocks)
{
	// int ile = 0;
	while (reads > 0 || writes > 0)
	{
		// printf ("Waiting for signals\n");
		if (sigsuspend(setWaiting) < 0 && errno != EINTR)
		{
			perror("sigsuspend");
			exit(1);
		}
		// printf ("Signal %d handled nr %d.\n",
 		//	   last,++ile);

		handleInterrupt(blocks);
		// printf ("Finished\n");
	}	
}

int prepareBlocks(struct aiocb ** blocks, int blocks3,
	int fds[3])
{
	
	// printf ("Running\n");
	int opcodes[] = { LIO_READ, LIO_READ, LIO_WRITE };
	for (int i = 0; i < blocks3; i++)
	{
		if ((blocks[i] = (struct aiocb *)malloc(sizeof(struct aiocb)*3)) \
				== NULL)
		{
			fprintf (stderr, "No memory, sorry\n");
			return 1;
		}
		int kOutputBlock = (i & 1) ? i-1 : i+1;		
		
		// printf ("Blocks %d saving to %d\n", i, kOutputBlock);
		
		memset(blocks[i], 0, sizeof(struct aiocb)*3);
		for (int j = 0; j < 3; j++)
		{
			blocks[i][j].aio_fildes = fds[j];
			blocks[i][j].aio_offset = (j == 2) ? kOutputBlock*BLOCK_SIZE
			: i * BLOCK_SIZE;
			blocks[i][j].aio_buf = (char *)malloc(sizeof(char)*BLOCK_SIZE);

			memset((void *)blocks[i][j].aio_buf, 0, sizeof(char)*BLOCK_SIZE);
			if (blocks[i][j].aio_buf == NULL)
			{
				printf ("No memory\n");
				return 1;
			}
			blocks[i][j].aio_nbytes = BLOCK_SIZE;
			blocks[i][j].aio_sigevent.sigev_notify = SIGEV_NONE;
			
			blocks[i][j].aio_lio_opcode = opcodes[j];
		}
		
	}
	
	return 0;
}

void sigHandler(int no, siginfo_t *info, void *ptr)
{
	last = no;
	// printf ("Signal %d handled\n", no);

	switch (no)
	{
		case SIGINT:
			enqueue = 0;
			// printf ("SIGINT handled\n");
		case SIGTERM:
			enqueue = 0;
			// printf ("SIGTERM handled\n");
		default:
			if (SIGRTMIN <= no && no <= SIGRTMIN+3)
			{
			//	printf ("Mine signal handled\n");
			}
			else
			{
				printf ("Unknown signal handled\n");
			}

			if (no == SIGRTMIN) // obsłużone bloki read
			{
				--reads;
				sigval_t value = info->si_value;
				struct aiocb * blocks = (struct aiocb *)value.sival_ptr;
			/*	printf ("block: %d\n read, 1: %s, 2: %s\n",
					(int)blocks[0].aio_offset / BLOCK_SIZE,
					(char *)blocks[0].aio_buf, (char *)blocks[1].aio_buf);
					*/

				LastBlock = (int)blocks[0].aio_offset / BLOCK_SIZE;
				
			} else
			if (no == SIGRTMIN+3)
			{
				LastBlock = -1;
				--writes;
				// printf ("lio_return write\n");
			}
			break;
	}
}

int setHandlers()
{
	struct sigaction sig;
	memset(&sig, 0, sizeof(sig));
	sig.sa_sigaction = sigHandler;
	sig.sa_flags = SA_SIGINFO;
	sigfillset(&sig.sa_mask);

	if (sigaction(SIGRTMIN,&sig, NULL) < 0)
		return -1;
	if (sigaction(SIGRTMIN+3,&sig, NULL) < 0)
		return -1;						
	if (sigaction(SIGINT, &sig, NULL) < 0)
		return -1;
	if (sigaction(SIGTERM, &sig, NULL) < 0)
		return -1;

	return 0;
}

void usage()
{
	fprintf (stderr, "Proper usage: ");
	fprintf (stderr, "./aio.out INPUT1.txt INPUT2.txt OUTPUT.txt\n");
}

int prepareSets(sigset_t *waiting, sigset_t *blocking)
{
	
	memset (&waiting, 0, sizeof(sigset_t));
	memset (&blocking, 0, sizeof(sigset_t));
	
	sigfillset(waiting);
	sigdelset(waiting, SIGRTMIN);
	sigdelset(waiting, SIGRTMIN+3);
	sigdelset(waiting, SIGINT);
	sigdelset(waiting, SIGTERM);
	sigdelset(waiting, SIGFPE);
	sigdelset(waiting, SIGSEGV);
	
	sigfillset(blocking);
	sigdelset(blocking, SIGINT);
	sigdelset(blocking, SIGTERM);
	sigdelset(blocking, SIGFPE);
	sigdelset(blocking, SIGSEGV);	
	
	return 0;
}

int openResources(char **argv, int fds[2],
				  int *size1, int *size2)
{
	
	if ((fds[0] = SafeOpen(argv[1], O_RDONLY, 0)) < 0)
	{
		perror("open input1");
		return 1;
	}

	if ((fds[1] = SafeOpen(argv[2], O_RDONLY,0)) < 0)
	{
		perror("open input2");
		return 1;
	}

	if ((fds[2] = SafeOpen(argv[3], O_CREAT | O_RDWR,0755)) < 0)
	{
		perror("open output");
		return 1;
	}

	*size1 = lseek(fds[0], 0, SEEK_END);
	if (size1 < 0)
	{
		perror("lseek");
		return 1;
	}

	*size2 = lseek(fds[1], 0, SEEK_END);
	if (size2 < 0)
	{
		perror("lseek");
		return 1;
	}

	if (lseek(fds[0], 0, SEEK_SET) != 0)
	{
		perror("lseek");
		return 1;
	}

	if (lseek(fds[1], 0, SEEK_SET) != 0)
	{
		perror("lseek");
		return 1;
	}
	
	return 0;
	
}

int closeResources(int fds[2])
{
	if (SafeClose(fds[0]) < 0)
	{
		perror("close input1");
		return 1;
	}

	if (SafeClose(fds[1]) < 0)
	{
		perror("close input2");
		return 1;
	}

	if (SafeClose(fds[2]) < 0)
	{
		perror("close output");
		return 1;
	}	
	
	return 0;
}


int main(int argc, char **argv)
{

	if (setHandlers() < 0)
	{
		perror("setHandlers");
		return 1;
	}
	
	sigset_t setWaiting;
	sigset_t setBlocking;
	
	prepareSets(&setWaiting, &setBlocking);

	if (sigprocmask(SIG_BLOCK, &setBlocking, NULL) < 0)
	{
		perror("sigprocmask");
		exit(1);
	}

	if (argc != 4)
	{
		usage();
		return 1;
	}

	int fds[3];
	int size1,size2;
	
	openResources(argv,fds,&size1,&size2);

	// printf ("Size of files: %d %d\n", size1, size2);

	int blocks1 = ALIGN(size1);
	int blocks2 = ALIGN(size2);
	int blocks3 = (blocks1 < blocks2) ? blocks1 : blocks2;

	// printf ("Blocks count: %d,%d - common: %d\n", blocks1, blocks2, blocks3);

	struct aiocb **blocks = NULL;
	blocks = (struct aiocb **)malloc(sizeof(struct aiocb*)*blocks3);
	if (blocks == NULL)
	{
		fprintf (stderr, "No memory, sorry\n");
		return 1;
	}
	
	int ileZakolejkowano = 0;
	prepareBlocks(blocks, blocks3, fds);
	executeIo(&ileZakolejkowano, blocks, blocks3);
	
	handleInterrupts(&setWaiting, blocks);

	for (int i = 0; i < ileZakolejkowano; i++)
	{
		/*
		 * printf ("Got: %s - %s\n", (char *)blocks[i][0].aio_buf,
 			   	(char *)blocks[i][1].aio_buf);		
 			  */
		for (int j = 0; j < 3; j++)
			free((void *)blocks[i][j].aio_buf);
		free(blocks[i]);
	}


	free(blocks);
	
	closeResources(fds);

	return 0;
}
