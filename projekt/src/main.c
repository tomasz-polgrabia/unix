#include <stdio.h>
#include <aio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define ALIGN(value, blkSize) ((value-1)/blkSize)+1
#define ERR(arg) if ((arg) < 0) \
		{ \
			perror("ERR"); \
			exit(2); \
		}

sig_atomic_t writes = 0;
sig_atomic_t reads = 0;
sig_atomic_t last = -1;
sig_atomic_t LastOffset = -1;
sig_atomic_t enqueue = 1;

int deallocateBlocks(struct aiocb **blocks) {
	int i;
	for (i = 0; i < 3; i++) {
		free((void *) blocks[0][i].aio_buf);
	}

	free(blocks);

	return 0;
}

int SafeOpen(const char *pathname, int flags, mode_t mode) {
	return TEMP_FAILURE_RETRY(open(pathname, flags, mode));
}

int SafeClose(int fd) {
	return TEMP_FAILURE_RETRY(close(fd));
}

void SafeFree(void *ptr) {
	if (ptr != NULL )
		free(ptr);
}

int handleInterrupt(struct aiocb** blocks, int blkSize) {
	if (last == SIGRTMIN) {
		char *buff = (char *) blocks[LastOffset/blkSize][2].aio_buf;
		char *buff1 = (char *) blocks[LastOffset/blkSize][0].aio_buf;
		char *buff2 = (char *) blocks[LastOffset/blkSize][1].aio_buf;
		int i = 0;
		struct aiocb *tab2[1];
		struct sigevent event;

		tab2[0] = &blocks[LastOffset/blkSize][2];


		for (i = 0; i < blkSize; i++) {
			char c1 = buff1[i];
			char c2 = buff2[blkSize - i - 1];
			buff[i] = c1 ^ c2;
		}	
		memset(&event, 0, sizeof(event));

		event.sigev_notify = SIGEV_SIGNAL;
		event.sigev_signo = SIGRTMIN + 3;
		event.sigev_value.sival_ptr = blocks;

		ERR(lio_listio(LIO_NOWAIT, tab2, 1, &event) < 0);
		++writes;

	}
	return 0;
}

int executeIo(int *countEnqueued, struct aiocb **blocks, int blocks3) {
	/* executing aio */
	struct sigevent event;
	int i;
	memset(&event, 0, sizeof(event));
	event.sigev_notify = SIGEV_SIGNAL;
	event.sigev_signo = SIGRTMIN;
	*countEnqueued = 0;

	/*  sleep(5); */
	for (i = 0; i < blocks3 && enqueue; i++) {
		struct aiocb *tab[2];
		tab[0] = &blocks[i][0];
		tab[1] = &blocks[i][1];
		event.sigev_value.sival_ptr = blocks[i];

		ERR(lio_listio(LIO_NOWAIT, tab, 2, &event));

		++reads;
		++*countEnqueued;
	}

	return 0;
}

void handleInterrupts(sigset_t *setWaiting, struct aiocb** blocks,
		int blkSize) {
	while (reads > 0 || writes > 0) {
		ERR(sigsuspend(setWaiting) < 0 && errno != EINTR);

		handleInterrupt(blocks,blkSize);
	}
}

int prepareBlocks(struct aiocb *** blocks, int blocks3, int fds[3],
		int blkSize, int blkCommon) {

	int opcodes[] = { LIO_READ, LIO_READ, LIO_WRITE };
	int i,j,kOutputBlock;
	char *str[3];

	*blocks = (struct aiocb **) malloc(sizeof(struct aiocb*) * blkCommon);
	if (blocks == NULL ) {
		fprintf(stderr, "No memory, sorry\n");
		return 1;
	}


	for (i = 0; i < blocks3; i++) {
		if (((*blocks)[i] = (struct aiocb *) malloc(sizeof(struct aiocb) * 3))
				== NULL ) {
			fprintf(stderr, "No memory, sorry\n");
			return 1;
		}
		kOutputBlock = (i & 1) ? i - 1 : i + 1;

		str[0] = (char *)malloc(sizeof(char)
			*blkSize*blocks3);
		str[1] = (char *)malloc(sizeof(char)
			*blkSize*blocks3);
		str[2] = (char *)malloc(sizeof(char)
			*blkSize*blocks3);

		if (str[0] == NULL || str[1] == NULL 
			|| str[2] == NULL)
		{
			perror("malloc");
			exit(2);
		}

		memset((*blocks)[i], 0, sizeof(struct aiocb) * 3);
		for (j = 0; j < 3; j++) {
			(*blocks)[i][j].aio_fildes = fds[j];
			(*blocks)[i][j].aio_offset =
				(j == 2) ? 
				kOutputBlock * blkSize : i * blkSize;
			(*blocks)[i][j].aio_buf = (char *)
				&str[j][blkSize*i];
			memset((void *) (*blocks)[i][j].aio_buf, 0, sizeof(char) * blkSize);
			(*blocks)[i][j].aio_nbytes = blkSize;
			(*blocks)[i][j].aio_sigevent.sigev_notify = SIGEV_NONE;

			(*blocks)[i][j].aio_lio_opcode = opcodes[j];
		}

	}

	return 0;
}

void sigHandler(int no, siginfo_t *info, void *ptr) {
	sigval_t value;
	struct aiocb *blocks;
	last = no;
	ptr = ptr;

	switch (no) {
	case SIGINT:
		enqueue = 0;
		break;
	case SIGTERM:
		enqueue = 0;
		break;
	default:

		if (no == SIGRTMIN) /* obsłużone bloki read */
		{
			--reads;
			value = info->si_value;
			blocks = (struct aiocb *) value.sival_ptr;
			LastOffset = (int) blocks[0].aio_offset;

		} else if (no == SIGRTMIN + 3) {
			LastOffset = -1;
			--writes;
		}
		break;
	}
}

int setHandlers() {
	struct sigaction sig;
	memset(&sig, 0, sizeof(sig));
	sig.sa_sigaction = sigHandler;
	sig.sa_flags = SA_SIGINFO;
	sigfillset(&sig.sa_mask);

	ERR(sigaction(SIGRTMIN,&sig, NULL));
	ERR(sigaction(SIGRTMIN+3,&sig, NULL));
	ERR(sigaction(SIGINT, &sig, NULL));
	ERR(sigaction(SIGTERM, &sig, NULL));

	return 0;
}

void usage(char **argv) {
	fprintf(stderr, "Proper usage: ");
	fprintf(stderr, "%s INPUT1.txt INPUT2.txt OUTPUT.txt\n", argv[0]);
}

int prepareSets(sigset_t *waiting, sigset_t *blocking) {

	memset(waiting, 0, sizeof(sigset_t));
	memset(blocking, 0, sizeof(sigset_t));

	sigfillset(waiting);
	sigdelset(waiting, SIGRTMIN);
	sigdelset(waiting, SIGRTMIN + 3);
	sigdelset(waiting, SIGINT);
	sigdelset(waiting, SIGTERM);
	sigdelset(waiting, SIGFPE);
	sigdelset(waiting, SIGSEGV);

	sigfillset(blocking);
	sigdelset(blocking, SIGINT);
	sigdelset(blocking, SIGTERM);
	sigdelset(blocking, SIGFPE);
	sigdelset(blocking, SIGSEGV);

	return 0;
}

int openResources(char **argv, int fds[2], int *size1, int *size2) {

	ERR(fds[0] = SafeOpen(argv[1], O_RDONLY, 0));
	ERR(fds[1] = SafeOpen(argv[2], O_RDONLY,0));
	ERR(fds[2] = SafeOpen(argv[3], O_CREAT | O_RDWR,0755));

	ERR(*size1 = lseek(fds[0], 0, SEEK_END));
	ERR(*size2 = lseek(fds[1], 0, SEEK_END));
	ERR(lseek(fds[0], 0, SEEK_SET))
	ERR(lseek(fds[1], 0, SEEK_SET));

	return 0;

}

int closeResources(int fds[2]) {
	if (SafeClose(fds[0]) < 0) {
		perror("close input1");
		return 1;
	}

	if (SafeClose(fds[1]) < 0) {
		perror("close input2");
		return 1;
	}

	if (SafeClose(fds[2]) < 0) {
		perror("close output");
		return 1;
	}

	return 0;
}

void parseArgs(int argc, char **argv, int *blkSize)
{
	if (argc != 5) {
		usage(argv);
		exit(1);
	}

	*blkSize = atoi(argv[4]);

	if (*blkSize <= 0)
	{
		usage(argv);
		exit(2);
	}


}

int main(int argc, char **argv) {

	sigset_t setWaiting,setBlocking;
	int fds[3];
	int s1, s2, blkSize, blk1,blk2,blkCommon,countEnqueued = 0;
	struct aiocb **blocks = NULL;

	parseArgs(argc,argv,&blkSize);

	ERR(setHandlers());	
	prepareSets(&setWaiting, &setBlocking);
	ERR(sigprocmask(SIG_BLOCK, &setBlocking, NULL) < 0);
	ERR(openResources(argv,fds,&s1,&s2));

	if (blk1 < blk2)
		blkCommon = ALIGN(s1,blkSize); 
  else
		blkCommon = ALIGN(s2,blkSize);

	prepareBlocks(&blocks, blkCommon, fds, blkSize, blkCommon);
	ERR(executeIo(&countEnqueued, blocks, blkCommon));

	handleInterrupts(&setWaiting, blocks, blkSize);
	ERR(deallocateBlocks(blocks));

	ERR(closeResources(fds));

	return 0;
}
