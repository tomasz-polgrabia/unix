#include <stdio.h>
#include <aio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define ALIGN(value, blkSize) ((value-1)/blkSize)+1
#define ERR(arg) if ((arg) < 0) \
		{ \
			perror("ERR"); \
			exit(2); \
		}

#define min(x,y) (x < y) ? x : y

#define MAX_BLOCKS 2

sig_atomic_t writes = 0;
sig_atomic_t reads = 0;
sig_atomic_t LastSignalCode = -1;
sig_atomic_t LastIdx = -1;
sig_atomic_t enqueue = 1;

int deallocateBlocks(struct aiocb **blocks, int *freeResources) {
	int i;
	for (i = 0; i < 3; i++) {
		free((void *) blocks[0][i].aio_buf);
	}

	for (i = 0; i < MAX_BLOCKS; i++)
		free(blocks[i]);

	free(blocks);
	free(freeResources);

	return 0;
}

int SafeOpen(const char *pathname, int flags, mode_t mode) {
	return TEMP_FAILURE_RETRY(open(pathname, flags, mode));
}

int SafeClose(int fd) {
	return TEMP_FAILURE_RETRY(close(fd));
}

void SafeFree(void *ptr) {
	if (ptr != NULL )
		free(ptr);
}

int handleInterrupt(struct aiocb** blocks, int blkSize, int *freeResources) {
	/* FIXME */
	if (LastSignalCode == SIGRTMIN) {
		char *buff = (char *) blocks[LastIdx][2].aio_buf;
		char *buff1 = (char *) blocks[LastIdx][0].aio_buf;
		char *buff2 = (char *) blocks[LastIdx][1].aio_buf;
		int kOutputBlock;
		int i = 0;
		struct aiocb *tab2[1];
		struct sigevent event;

		tab2[0] = &blocks[LastIdx][2];
		kOutputBlock = freeResources[LastIdx];
		kOutputBlock = (kOutputBlock & 1) ?  kOutputBlock - 1 : kOutputBlock+1;
		blocks[LastIdx][2].aio_offset = kOutputBlock*blkSize;

		for (i = 0; i < blkSize; i++) {
			char c1 = buff1[i];
			char c2 = buff2[blkSize - i - 1];
			buff[i] = c1 ^ c2;
		}

		memset(&event, 0, sizeof(event));

		event.sigev_notify = SIGEV_SIGNAL;
		event.sigev_signo = SIGRTMIN + 3;
		event.sigev_value.sival_int = LastIdx;

		ERR(lio_listio(LIO_NOWAIT, tab2, 1, &event) < 0);
		--reads;
		++writes;

	} else
	{
		--writes;
		freeResources[LastIdx] = -1;
	}
	return 0;
}

int executeIo(int *countEnqueued, int *freeResources, struct aiocb **blocks,
		int blkCommon, int blkSize, sigset_t *setWaiting) {
	/* executing aio */
	/* FIXME */
	struct sigevent event;
	int i, free;
	memset(&event, 0, sizeof(event));
	event.sigev_notify = SIGEV_SIGNAL;
	event.sigev_signo = SIGRTMIN;
	*countEnqueued = 0;

	/*  sleep(5); */
	for (i = 0; i < blkCommon && enqueue; i++) {
		struct aiocb *tab[2];
		/* FIXME */
		/* search for free resources */
		for (free = 0; free < MAX_BLOCKS && freeResources[free] >= 0; free++);

		if (free >= MAX_BLOCKS) {
			fprintf(stderr, "To nie powinno sie zdarzyc\n");
			abort();
		}

		freeResources[free] = i; /* wpisujemy to idx dla bloku */

		blocks[free][0].aio_offset = blocks[free][1].aio_offset = blkSize*i;

		tab[0] = &blocks[free][0];
		tab[1] = &blocks[free][1];
		event.sigev_value.sival_int = free;

		ERR(lio_listio(LIO_NOWAIT, tab, 2, &event));

		++reads;
		++*countEnqueued;

		while (reads + writes >= MAX_BLOCKS) {
			ERR(sigsuspend(setWaiting) < 0 && errno != EINTR);
			handleInterrupt(blocks, blkSize, freeResources);
		}

	}

	return 0;
}

void handleInterrupts(sigset_t *setWaiting, struct aiocb** blocks,
		int *freeResources, int blkSize) {
	while (reads > 0 || writes > 0) {
		ERR(sigsuspend(setWaiting) < 0 && errno != EINTR);

		handleInterrupt(blocks, blkSize,freeResources);
	}
}

int prepareBlocks(struct aiocb *** blocks, int **free, int fds[3], int blkSize,
		int blkCommon) {

	int opcodes[] = { LIO_READ, LIO_READ, LIO_WRITE };
	int i, j;
	char *str[3];

	int blocksCount = min(blkCommon, MAX_BLOCKS);

	str[0] = (char *) malloc(sizeof(char) * blkSize * blocksCount);
	str[1] = (char *) malloc(sizeof(char) * blkSize * blocksCount);
	str[2] = (char *) malloc(sizeof(char) * blkSize * blocksCount);

	if (str[0] == NULL || str[1] == NULL || str[2] == NULL ) {
		perror("malloc");
		exit(2);
	}

	*blocks = (struct aiocb **) malloc(sizeof(struct aiocb*) * blocksCount);
	if (blocks == NULL ) {
		fprintf(stderr, "No memory, sorry\n");
		exit(1);
	}

	*free = (int *) malloc(sizeof(int) * blocksCount);
	if (*free == NULL ) {
		fprintf(stderr, "No memory, sorry\n");
		exit(1);
	}

	for (i = 0; i < blocksCount; i++)
		(*free)[i] = -1;

	for (i = 0; i < blocksCount; i++) {
		if (((*blocks)[i] = (struct aiocb *) malloc(sizeof(struct aiocb) * 3))
				== NULL ) {
			fprintf(stderr, "No memory, sorry\n");
			return 1;
		}

		/* kOutputBlock = (i & 1) ? i - 1 : i + 1; */

		memset((*blocks)[i], 0, sizeof(struct aiocb) * 3);
		for (j = 0; j < 3; j++) {
			(*blocks)[i][j].aio_fildes = fds[j];
/*			(*blocks)[i][j].aio_offset =
					(j == 2) ? kOutputBlock * blkSize : i * blkSize; */
			(*blocks)[i][j].aio_buf = (char *) &str[j][blkSize * i];
			memset((void *) (*blocks)[i][j].aio_buf, 0, sizeof(char) * blkSize);
			(*blocks)[i][j].aio_nbytes = blkSize;
			(*blocks)[i][j].aio_sigevent.sigev_notify = SIGEV_NONE;

			(*blocks)[i][j].aio_lio_opcode = opcodes[j];
		}

	}

	return 0;
}

void sigHandler(int no, siginfo_t *info, void *ptr) {
	sigval_t value = info->si_value;
	LastSignalCode = no;
	LastIdx = value.sival_int;
	ptr = (char *) ptr + 1;

	switch (no) {
	case SIGINT:
		enqueue = 0;
		break;
	case SIGTERM:
		enqueue = 0;
		break;
	}

}

int setHandlers() {
	struct sigaction sig;
	memset(&sig, 0, sizeof(sig));
	sig.sa_sigaction = sigHandler;
	sig.sa_flags = SA_SIGINFO;
	sigfillset(&sig.sa_mask);

	ERR(sigaction(SIGRTMIN,&sig, NULL));
	ERR(sigaction(SIGRTMIN+3,&sig, NULL));
	ERR(sigaction(SIGINT, &sig, NULL));
	ERR(sigaction(SIGTERM, &sig, NULL));

	return 0;
}

void usage(char **argv) {
	fprintf(stderr, "Proper usage: ");
	fprintf(stderr, "%s INPUT1.txt INPUT2.txt OUTPUT.txt\n", argv[0]);
}

int prepareSets(sigset_t *waiting, sigset_t *blocking) {

	memset(waiting, 0, sizeof(sigset_t));
	memset(blocking, 0, sizeof(sigset_t));

	sigfillset(waiting);
	sigdelset(waiting, SIGRTMIN);
	sigdelset(waiting, SIGRTMIN + 3);
	sigdelset(waiting, SIGINT);
	sigdelset(waiting, SIGTERM);
	sigdelset(waiting, SIGFPE);
	sigdelset(waiting, SIGSEGV);

	sigfillset(blocking);
	sigdelset(blocking, SIGINT);
	sigdelset(blocking, SIGTERM);
	sigdelset(blocking, SIGFPE);
	sigdelset(blocking, SIGSEGV);

	return 0;
}

int openResources(char **argv, int fds[2], int *size1, int *size2) {

	ERR(fds[0] = SafeOpen(argv[1], O_RDONLY, 0));
	ERR(fds[1] = SafeOpen(argv[2], O_RDONLY,0));
	ERR(fds[2] = SafeOpen(argv[3], O_CREAT | O_RDWR,0755));

	ERR(*size1 = lseek(fds[0], 0, SEEK_END));
	ERR(*size2 = lseek(fds[1], 0, SEEK_END));
	ERR(lseek(fds[0], 0, SEEK_SET))
	ERR(lseek(fds[1], 0, SEEK_SET));

	return 0;

}

int closeResources(int fds[2]) {
	if (SafeClose(fds[0]) < 0) {
		perror("close input1");
		return 1;
	}

	if (SafeClose(fds[1]) < 0) {
		perror("close input2");
		return 1;
	}

	if (SafeClose(fds[2]) < 0) {
		perror("close output");
		return 1;
	}

	return 0;
}

void parseArgs(int argc, char **argv, int *blkSize) {
	if (argc != 5) {
		usage(argv);
		exit(1);
	}

	*blkSize = atoi(argv[4]);

	if (*blkSize <= 0) {
		usage(argv);
		exit(2);
	}

}

int main(int argc, char **argv) {

	sigset_t setWaiting, setBlocking;
	int fds[3];
	int s1, s2, blkSize, blkCommon, countEnqueued = 0;
	int *freeResources = NULL;
	struct aiocb **blocks = NULL;

	parseArgs(argc, argv, &blkSize);

	ERR(setHandlers());
	prepareSets(&setWaiting, &setBlocking);
	ERR(sigprocmask(SIG_BLOCK, &setBlocking, NULL) < 0);
	ERR(openResources(argv, fds, &s1, &s2));

	if (s1 < s2)
		blkCommon = ALIGN(s1,blkSize);
	else
		blkCommon = ALIGN(s2,blkSize);

	prepareBlocks(&blocks, &freeResources, fds, blkSize, blkCommon);
	ERR(executeIo(&countEnqueued,freeResources, blocks, blkCommon,
			blkSize, &setWaiting));

	handleInterrupts(&setWaiting, blocks, freeResources, blkSize);
	ERR(deallocateBlocks(blocks, freeResources));

	ERR(closeResources(fds));

	return 0;
}
