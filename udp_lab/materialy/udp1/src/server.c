#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <time.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>
#include <fcntl.h>
#include "generate.h"
#define ERR(source) (perror(source),\
		     fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
		     		     exit(EXIT_FAILURE))
#define MALLOCERR (fprintf(stderr,"malloc failed:\n%s:%d\n",__FILE__,__LINE__),\
		exit(EXIT_FAILURE))
#define BACKLOG 3
#define MAXBUF 576
#define MAXADDR 5

struct connections
{
	int next_free;
	struct sockaddr_in addr[MAXADDR];
	socklen_t sizes[MAXADDR];
};

int sethandler( void (*f)(int), int sigNo) 
{
	struct sigaction act;
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = f;
	if (-1==sigaction(sigNo, &act, NULL))
		return -1;
	return 0;
}

int make_socket(int domain, int type)
{
	int sock;
	sock = socket(domain,type,0);
	if(sock < 0) ERR("socket");
	return sock;
}

int bind_inet_socket(uint16_t port,int type)
{
	struct sockaddr_in addr;
	int socketfd,t=1;
	int flags;
	socketfd = make_socket(PF_INET,type);
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR,&t, sizeof(t))) ERR("setsockopt");
	if(bind(socketfd,(struct sockaddr*) &addr,sizeof(addr)) < 0)  ERR("bind");
	if((flags = fcntl(socketfd, F_GETFL)) < 0) ERR("fcntl F_GETFL");
	flags |= O_NONBLOCK;
	if(fcntl(socketfd, F_SETFL, flags) < 0) ERR("fcntl F_SETFL");
	if(SOCK_STREAM==type)
		if(listen(socketfd, BACKLOG) < 0) ERR("listen");
	return socketfd;
}

void sendAll(int fd, struct connections* conns, char** image)
{
	int16_t i, j, k, offset;
	char buf[MAXBUF];
	for(i = 0; i < IMAGE_Y; ++i)
		for(j = 0; j < IMAGE_X; ++j)
			for(k = 0; k < conns->next_free; ++k)
			{
				offset = 0;
				*((int16_t*)(buf+offset)) = htons(j);
				offset += sizeof(int16_t);
				*((int16_t*)(buf+offset)) = htons(i);
				offset += sizeof(int16_t);
				buf[offset] = image[i][j];
				offset += sizeof(char);
				if(TEMP_FAILURE_RETRY(sendto(fd,buf,offset,0,&(conns->addr[k]), conns->sizes[k]))<0) ERR("send:");
			}
}

void tryRegister(int fd, struct connections* conns)
{
	int i;
	char buf[MAXBUF];
	while(conns->next_free != MAXADDR)
		if(TEMP_FAILURE_RETRY(recvfrom(fd, buf, MAXBUF, 0, &(conns->addr[conns->next_free]), &(conns->sizes[conns->next_free]))) >= 0)
		{
			fprintf(stderr, "registered addr: ");
			for(i = 0; i < conns->sizes[conns->next_free]; ++i)
				fprintf(stderr, "%d,", *((uint8_t*)(&(conns->addr[conns->next_free])) + i));
			fprintf(stderr, "\n");
			++conns->next_free;
		}
		else
			break;
}

int doServer(int fd)
{
	char** image;
	int i;
	struct timespec delay;
	struct connections conns;

	conns.next_free = 0;
	memset(conns.addr, 0, MAXADDR * sizeof(struct sockaddr_in));
	for(i = 0; i < MAXADDR; ++i)
		conns.sizes[i] = sizeof(struct sockaddr_in);

	if((image = (char**)malloc( IMAGE_Y * sizeof(char*) )) == NULL) MALLOCERR;
	for(i = 0; i < IMAGE_Y; ++i)
		if((image[i] = (char*)malloc( IMAGE_X * sizeof(char) )) == NULL) MALLOCERR;
	for(;;)
	{
		delay.tv_sec = 0;
		delay.tv_nsec = 100000000; /* 100 ms */
		tryRegister(fd, &conns);
		generate(image);
		sendAll(fd, &conns, image);
		TEMP_FAILURE_RETRY(nanosleep(&delay, &delay));
	}
	for(i = 0; i < IMAGE_Y; ++i)
		free(image[i]);
	free(image);
}

void usage(char * name)
{
	fprintf(stderr,"USAGE: %s port\n",name);
}

int main(int argc, char** argv)
{
	int fd;
	if(argc!=2)
	{
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	if(sethandler(SIG_IGN,SIGPIPE)) ERR("Seting SIGPIPE:");
	fd=bind_inet_socket(atoi(argv[1]),SOCK_DGRAM);
	doServer(fd);
	if(TEMP_FAILURE_RETRY(close(fd))<0)ERR("close");
	fprintf(stderr,"Server has terminated.\n");
	return EXIT_SUCCESS;
}
