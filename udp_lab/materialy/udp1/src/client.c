#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "generate.h"

#define ERR(source) (perror(source),\
		     fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
		     exit(EXIT_FAILURE))
#define HERR(source) (fprintf(stderr,"%s(%d) at %s:%d\n",source,h_errno,__FILE__,__LINE__),\
		     exit(EXIT_FAILURE))
#define MALLOCERR (fprintf(stderr,"malloc failed:\n%s:%d\n",__FILE__,__LINE__),\
		exit(EXIT_FAILURE))

#define MAXBUF 5
#define SPACE_SIZE 100

int sethandler( void (*f)(int), int sigNo) 
{
	struct sigaction act;
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = f;
	if (-1==sigaction(sigNo, &act, NULL))
		return -1;
	return 0;
}

int make_socket(void)
{
	int sock;
	sock = socket(PF_INET,SOCK_DGRAM,0);
	if(sock < 0) ERR("socket");
	return sock;
}

struct sockaddr_in make_address(char *address, uint16_t port)
{
	struct sockaddr_in addr;
	struct hostent *hostinfo;
	addr.sin_family = AF_INET;
	addr.sin_port = htons (port);
	hostinfo = gethostbyname(address);
	if(hostinfo == NULL)HERR("gethostbyname");
	addr.sin_addr = *(struct in_addr*) hostinfo->h_addr;
	return addr;
}

void usage(char * name)
{
	fprintf(stderr,"USAGE: %s address port\n",name);
}

void clearImage(char** img)
{
	int i, j;
	for(i = 0; i < IMAGE_Y; ++i)
		for(j = 0; j < IMAGE_X; ++j)
			img[i][j] = ' ';
}

void doClient(int fd, struct sockaddr_in addr)
{
	char buf[MAXBUF];
	char** image;
	int16_t x, y, prev_y = 0;
	char c;
	int i;
	socklen_t size = sizeof(addr); 

	if((image = (char**)malloc( IMAGE_Y * sizeof(char*) )) == NULL) MALLOCERR;
	for(i = 0; i < IMAGE_Y; ++i)
	{
		if((image[i] = (char*)malloc( (1 + IMAGE_X) * sizeof(char) )) == NULL) MALLOCERR;
		image[i][IMAGE_X] = '\0';
	}
	clearImage(image);

	/* register at server */
	if(TEMP_FAILURE_RETRY(sendto(fd,"0",1,0,&addr,sizeof(addr)))<0) ERR("sendto:");
	for(;;)
	{
		if(TEMP_FAILURE_RETRY(recvfrom(fd, buf, MAXBUF, 0, &addr, &size)) < 0) ERR("recvfrom:");
		x = ntohs(((int16_t*)buf)[0]);
		y = ntohs(((int16_t*)buf)[1]);
		c = buf[sizeof(int16_t) << 1];
		if(y < prev_y)
		{
			printf("image:\n");
			for(i = 0; i < SPACE_SIZE; ++i)
				printf("\n");
			for(i = 0; i < IMAGE_Y; ++i)
				printf("%s\n", image[i]);
			clearImage(image);
		}
		prev_y = y;
		image[y][x] = c;		
	}
	for(i = 0; i < IMAGE_Y; ++i)
		free(image[i]);
	free(image);
}

int main(int argc, char** argv) 
{
	int fd;
	struct sockaddr_in addr;
	if(argc!=3) 
	{
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	if(sethandler(SIG_IGN,SIGPIPE)) ERR("Seting SIGPIPE:");
	fd = make_socket();
	addr = make_address(argv[1],atoi(argv[2]));
	doClient(fd,addr);

	if(TEMP_FAILURE_RETRY(close(fd))<0)ERR("close");
	return EXIT_SUCCESS;
}

