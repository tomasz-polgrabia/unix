#include <math.h>
#include <stdio.h>

#include "generate.h"

void generate(char** image)
{
	const char* symbols = "WHUIi;:.`";
	static double z = 1.0;
	static double l = 1.0;
	double s;
	int image_x = 0;
	int image_y = 0;
	double y, x, r, i, t, c;
	int k;

	if(z < 1e-8)
		z = l = 1.0;
	z *= 0.97;
	s = sin( l += 0.01) * z;

	for(y = 1; -1 < y; ++image_y)
	{
		image_x = 0;
		y -= 2.0 / (IMAGE_Y - 1); /* 0.07; */
		for(x = 2; -1 < x ; image[image_y][image_x++] = symbols[k % 9] )
		{
			x -= 3.0 / (IMAGE_X - 1); /*0.04;*/
			r = 0.0;
			i = 0.0;
			for(k = 99; --k && r*r + i*i<4; )
			{
				t = 2*r*i + y*( c = cos(l)*z ) - x*s;
				r = r*r - i*i + y*s + x*c - 1.5;
				i = t;
			}
		}
	}
}
