#include "header.h"
#include <sys/select.h>
#define MSGLENGHT 6
#define	TIMEINTERVAL 5

char alarmed;
void sig_handler(int sigID){
	if(sigID==SIGALRM){
	alarmed=1;
	}
}


void ClientWork( int socketfd,struct sockaddr_in srv_addr,char count){
	int received=0, gotmessage, msgsize=MSGLENGHT*sizeof(char);
	char i, msg[MAXBUF]="PING\0", buf[MAXBUF];
	fd_set set;
	for(i = 0; i < count; i++){
		printf("PING %d\n",i);
		msg[MSGLENGHT-1]=i;
		if(TEMP_FAILURE_RETRY(sendto(socketfd,msg,msgsize,0,&srv_addr,sizeof(srv_addr)))<0) ERR("sendto:");
		FD_SET(socketfd,&set);;
		gotmessage=0;
		alarmed=0;
		sethandler(sig_handler,SIGALRM);
		alarm(TIMEINTERVAL);
		for(;;){
			while(recv(socketfd,buf,MAXBUF,0)<0 ){
				if(errno!=EINTR) ERR("recv:");
				if(alarmed) break;
			}
			if(alarmed) break;
			if(!strcmp(buf,"PONG") && buf[MSGLENGHT-1]==i ) {
				printf("Odebrano\n");
				received++;
				gotmessage=1;
				break;
			}
		}
		if(!gotmessage)
			printf("Uplynal czas oczekiwania\n");
	}
	printf("Wyslano %d odebrano %d\n",count,received);
}

int main(int argc, char** argv) {
	char* host;
	uint16_t port;
	struct sockaddr_in srv_addr;
	int socketfd,repeat;
	if(argc!=4){
		fprintf(stderr,"Nie prawidlowe argumenty. Wywolanie  ./klient Adres_serwera port ilosc_powtorzen\n");
		return EXIT_FAILURE;
	}
	repeat=atoi(argv[3]);
	if(repeat>=128) { /*ilosc powtorzen*/
		fprintf(stderr,"Ilosc powtorzen za duża\n");
	 	return EXIT_FAILURE;
	}
	host=argv[1];
	port =(uint16_t) atoi(argv[2]);
	socketfd =  bind_to_inet_UDP(0);
	srv_addr = make_address(host, port);
	ClientWork(socketfd,srv_addr,(char)repeat);
	return EXIT_SUCCESS;
}
