#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>
#include <fcntl.h>


#define ERR(source) (fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),perror(source), kill(0,SIGKILL), exit(EXIT_FAILURE))
#define MAXBUF 576
#define MAXADDR 5

/* Kod pochodzi z tutoriali i przykładów*/

int sethandler( void (*f)(int), int sigNo);
int make_socket(int domain, int type);
int bind_to_inet_UDP(uint16_t port);
struct sockaddr_in make_address(char *address, uint16_t port);


