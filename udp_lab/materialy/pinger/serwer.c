#include "header.h"

void MySleep(int seconds)
{
	int tt;
	for (tt = seconds; tt > 0; tt = sleep(tt));
}

void SerwerWork(int socketfd)
{
	struct sockaddr_in addr;
	char buf[MAXBUF];
	char response[MAXBUF] = "PONG\0";
	int los;
	socklen_t size = sizeof(addr),responseLen = 6*sizeof(char);
	for(;;){
		if(TEMP_FAILURE_RETRY(recvfrom(socketfd,buf,MAXBUF,0,(struct sockaddr*)&addr,&size))<0) ERR("read:");
		if(!strcmp(buf,"PING")){
			response[5]=buf[5]; /*przepisanie ID*/
			/*sztuczne wprowadzenie zakłocen i opoznien*/
			los=rand()%4;
			MySleep(los);
			los=rand()%3;

			/*======*/
			if(los!=0)
			{
				printf("Wysylanie : PONG %d\n",response[5]);
				for(;;){
					if(TEMP_FAILURE_RETRY(sendto(socketfd,response,responseLen,0,&addr,sizeof(addr)))<0){
						if(errno == ECONNRESET || errno== EPIPE) continue;
						ERR("sendto:");					
					} 
					break;
				}
			}
		}
	}
}


int main(int argc, char** argv) {

	uint16_t port;
	int socketfd ;
	if(argc!=2){
		fprintf(stderr,"Nieprawidlowe argumenty: wywołanie : ./serwer port \n");
		return EXIT_FAILURE;
	}
	port = atoi(argv[1]);
	if(port<=0)
	{
		fprintf(stderr,"Nieprawidlowy numer portu \n");
		return EXIT_FAILURE;
	}
	sethandler(SIG_IGN,SIGPIPE);
	socketfd = bind_to_inet_UDP(port);
	SerwerWork(socketfd);
	return EXIT_SUCCESS;
}
