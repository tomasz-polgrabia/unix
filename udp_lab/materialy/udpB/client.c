#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#define ERR(source) (perror(source),\
		     fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
		     exit(EXIT_FAILURE))
#define HERR(source) (fprintf(stderr,"%s(%d) at %s:%d\n",source,h_errno,__FILE__,__LINE__),\
		     exit(EXIT_FAILURE))

#define MAXBUF 576

volatile sig_atomic_t last_signal=0 ;

void sigalrm_handler(int sig) {
	last_signal=sig;
}

int sethandler( void (*f)(int), int sigNo) {
	struct sigaction act;
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = f;
	if (-1==sigaction(sigNo, &act, NULL))
		return -1;
	return 0;
}

int make_socket(void){
	int sock;
	sock = socket(PF_INET,SOCK_DGRAM,0);
	if(sock < 0) ERR("socket");
	return sock;
}

struct sockaddr_in make_address(char *address, uint16_t port){
	struct sockaddr_in addr;
	struct hostent *hostinfo;
	addr.sin_family = AF_INET;
	addr.sin_port = htons (port);
	hostinfo = gethostbyname(address);
	if(hostinfo == NULL)HERR("gethostbyname");
	addr.sin_addr = *(struct in_addr*) hostinfo->h_addr;
	return addr;
}

void usage(char * name){
	fprintf(stderr,"USAGE: %s domain port file \n",name);
}

int sendAndConfirm(int fd,struct sockaddr_in addr,char *buf1, char *buf2, ssize_t size){
	struct itimerval ts;
	int counter=0;
	ssize_t rs;
	do{
		counter++;
		/*
		 * SIGPIPE is treated as ordinary error here
		 */
		if(TEMP_FAILURE_RETRY(sendto(fd,buf1,size,0,&addr,sizeof(addr)))<0) ERR("sendto:");
		memset(&ts, 0, sizeof(struct itimerval));
		ts.it_value.tv_usec=500000;
		setitimer(ITIMER_REAL,&ts,NULL);
		last_signal=0;
		while((rs=recv(fd,buf2,size,0))<0){
			if(EINTR!=errno)ERR("recv:");
			if(SIGALRM==last_signal) break;
		}
	}while(rs<0&&counter<=5);
	if(rs<0) return -1;
	return 0;
}

void doClient(int fd, struct sockaddr_in addr, char *file){
	char buf[MAXBUF];
	char buf2[MAXBUF];
	int32_t chunkNo,blocks;
	int nofile=1;
	*((int32_t*)buf)=htonl(-1);
	strncpy(buf+sizeof(int32_t),file,MAXBUF-sizeof(int32_t));
	if(0==sendAndConfirm(fd,addr,buf,buf2,MAXBUF)){
		blocks=ntohl(*((int32_t*)buf2));
		if(blocks>0){
			nofile=0;
			for(chunkNo=0;chunkNo<blocks;chunkNo++){
				*((int32_t*)buf)=htonl(chunkNo);
				if(0==sendAndConfirm(fd,addr,buf,buf2,MAXBUF))
					printf("Block No %d : \n%s\n",chunkNo+1,buf2);
				else break;
			}
		}
	}
	if(nofile) printf("File '%s' does not exists or can not be read",file);
}

int main(int argc, char** argv) {
	int fd;
	char *file;
	struct sockaddr_in addr;
	if(argc!=4) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	if(sethandler(SIG_IGN,SIGPIPE)) ERR("Seting SIGPIPE:");
	if(sethandler(sigalrm_handler,SIGALRM)) ERR("Seting SIGALRM:");
	file=argv[3];
	fd = make_socket();
	addr=make_address(argv[1],atoi(argv[2]));
	doClient(fd,addr,file);
	if(TEMP_FAILURE_RETRY(close(fd))<0)ERR("close");
	return EXIT_SUCCESS;
}


