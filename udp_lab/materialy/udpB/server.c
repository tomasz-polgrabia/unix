#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>
#include <fcntl.h>
#define ERR(source) (perror(source),\
		     fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
		     exit(EXIT_FAILURE))

#define BACKLOG 3
#define MAXBUF 576
#define MAXADDR 5

int sethandler( void (*f)(int), int sigNo) {
	struct sigaction act;
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = f;
	if (-1==sigaction(sigNo, &act, NULL))
		return -1;
	return 0;
}

int make_socket(int domain, int type){
	int sock;
	sock = socket(domain,type,0);
	if(sock < 0) ERR("socket");
	return sock;
}

int bind_inet_socket(uint16_t port,int type){
	struct sockaddr_in addr;
	int socketfd,t=1;
	socketfd = make_socket(PF_INET,type);
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR,&t, sizeof(t))) ERR("setsockopt");
	if(bind(socketfd,(struct sockaddr*) &addr,sizeof(addr)) < 0)  ERR("bind");
	if(SOCK_STREAM==type)
		if(listen(socketfd, BACKLOG) < 0) ERR("listen");
	return socketfd;
}

ssize_t bulk_read(int fd, char *buf, size_t count){
	int c;
	size_t len=0;
	do{
		c=TEMP_FAILURE_RETRY(read(fd,buf,count));
		if(c<0) return c;
		if(0==c) return len;
		buf+=c;
		len+=c;
		count-=c;
	}while(count>0);
	return len ;
}

void doServer(int fd){
	struct sockaddr_in addr;
	char buf[MAXBUF],*fname;
	socklen_t size=sizeof(addr);;
	int file,send;
	off_t fs;
	ssize_t ws;
	int32_t blocks,chunkNo;
	for(;;){
		if(TEMP_FAILURE_RETRY(recvfrom(fd,buf,MAXBUF,0,&addr,&size))<0) ERR("read:");
		chunkNo=ntohl(*((int32_t*)buf));
		fname=buf+sizeof(int32_t);
		send=blocks=-1;
		if((file=TEMP_FAILURE_RETRY(open(fname,O_RDONLY)))>=0){
			fs=lseek(file,0,SEEK_END);	
			blocks=fs/MAXBUF+(fs%MAXBUF?1:0);
		}
		if(-1==chunkNo) *((int32_t*)buf)=htonl(blocks);
		else{
			if(chunkNo>=blocks) send=0;  /*also true for file==-1*/
			else{
				lseek(file,chunkNo*MAXBUF,SEEK_SET);
				if((ws=bulk_read(file,buf,MAXBUF))<0) ERR("read:");
				memset(buf+ws,0,MAXBUF-ws);
			}
		}
		if(file>=0&&TEMP_FAILURE_RETRY(close(file))<0)ERR("close");
		if(send&&TEMP_FAILURE_RETRY(sendto(fd,buf,MAXBUF,0,&addr,size))<0&&(EPIPE!=errno)) ERR("send:");
	}
}

void usage(char * name){
	fprintf(stderr,"USAGE: %s port\n",name);
}

int main(int argc, char** argv) {
	int fd;
	if(argc!=2) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	if(sethandler(SIG_IGN,SIGPIPE)) ERR("Seting SIGPIPE:");
	fd=bind_inet_socket(atoi(argv[1]),SOCK_DGRAM);
	doServer(fd);
	if(TEMP_FAILURE_RETRY(close(fd))<0)ERR("close");
	fprintf(stderr,"Server has terminated.\n");
	return EXIT_SUCCESS;
}

