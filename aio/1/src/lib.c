#include "lib.h"

char *sigNames[SIG_SIZE] = 
{
	"", // 0
	"SIGHUP", // 1
	"SIGINT", // 2
	"SIGQUIT", // 3
	"SIGILL", // 4
	"",  // 5
	"SIGABRT", // 6
	"", // 7
	"SIGFPE", // 8
	"SIGKILL", // 9
	"SIGUSR1", // 10
	"SIGSEGV", // 11
	"SIGUSR2", // 12
	"SIGPIPE", // 13
	"SIGALRM", // 14
	"SIGTERM", // 15
	"SIGUSR1", // 16
	"SIGUSR2", // 17
	"SIGCHLD", // 18
	"SIGCONT", // 19

};


void SafeSleep(int k)
{
	int tt,t = k;
	for (tt = t; tt > 0; tt = sleep(tt));
}

#ifdef DEBUG

void plog(char *fmt, ...)
{
	va_list args;
	fprintf (stderr,"[%d]: ", getpid());
	va_start(args,fmt);
	vfprintf(stderr,fmt,args);
	va_end(args);
}

#endif

int setHandler(int nr, void (* handler)(int nr), int flags)
{
	struct sigaction action; 
	memset (&action, 0, sizeof(action));
	action.sa_handler = handler;
	action.sa_flags = flags;
	return sigaction(nr, &action,NULL);
}

int SafeSleep2(struct timespec t)
{
	struct timespec t2 = t;
	int result = -1;
	while ((result = nanosleep(&t2,&t2)) < 0 && errno == EINTR)
		printf ("Breaked: remained %ds %luns\n", (int)t2.tv_sec,
				t2.tv_nsec);
	return result;
}

void block(sigset_t *setOld)
{
	sigset_t set;
	sigfillset(&set);
	sigprocmask(SIG_BLOCK, &set, setOld);
}

void unblock(sigset_t *setOld)
{
	sigset_t set;
	sigfillset(&set);
	sigprocmask(SIG_UNBLOCK, &set, setOld);
}

