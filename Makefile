DIRS = $(shell ls -d */)

.PHONY: all clean

all:
	for i in $(DIRS) ; \
	do \
		cd $$i ; make; \
		cd .. ; \
	done 

clean:
	for i in $(DIRS) ; \
	do \
		cd $$i ; make clean ; \
		cd .. ; \
	done

