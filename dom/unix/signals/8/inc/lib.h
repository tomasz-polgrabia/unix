#ifndef __LIB_H__
#define __LIB_H__

#ifdef DEBUG
	#define debug(...) plog(__VA_ARGS__)
#else
	#define debug(...)
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <time.h>
#include <errno.h>

void SafeSleep(int k);
void plog(char *fmt, ...);
int setHandler(int nr, void (* handler)(int nr), int flags);
int SafeSleep2(struct timespec t);
void block(sigset_t *setOld);
void unblock(sigset_t *setOld);

#define SIG_SIZE 30

extern char *sigNames[SIG_SIZE];

#endif

