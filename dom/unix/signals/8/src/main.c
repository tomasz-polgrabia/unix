#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/time.h>
#include "lib.h"

volatile sig_atomic_t handled = 0;

void sigHandler(int nr)
{
	handled = 1;
	int max = sizeof(sigNames) / sizeof(char *);
	if (nr < max) 
		plog ("Otrzymalem sygnal %s o numerze %d\n",
 			   	sigNames[nr], nr);
	else
		plog ("Otrzymalem sygnal o numerze %d\n", nr);
	switch (nr)
	{
		case SIGINT:
			plog ("Wychodze na zyczenie operatora\n");
			exit(0);
			break;
	}
}

void sigHandler2(int nr,siginfo_t *info, void *ptr)
{
	debug("Signal handled\n");
	debug ("Info 0x%x\n", info);
	if (info != NULL)
	{
		debug("Signal number: %d\n", info->si_signo);
		debug("Errno value: %d\n", info->si_errno);
		debug("Signal code: %d\n", info->si_code);
		//debug("Trap number: %d\n", info->si_trapno);
		debug("PID of process %d\n", info->si_pid);
		debug("Real user ID of process %d\n", info->si_uid);
		debug("Exit value of signal %d\n", info->si_status);
		debug("User time consumed %d\n", info->si_utime);
		debug("System time consumed %d\n", info->si_stime);
		debug("Signal value: %d\n", info->si_value);
		debug("POSIX.1b signal: %d\n", info->si_int);
		debug("POSIX.1b signal (ptr) : %x\n", info->si_ptr);
		debug("Timer overrun count; POSIX.1b timers: %d\n",
				info->si_overrun);
		debug("Timer ID: %d\n", info->si_timerid);
		debug("Memory location (fault): 0x%x\n", info->si_addr);
		//debug("Band event: %l\n", info->si_band);
		debug("File descriptor: %d\n", info->si_fd);
		// debug("Least siginificant bit of address %d\n",
		//		info->si_addr_lsb);
	}
}	

void setHandler2(int nr, void (*handler)(int,siginfo_t *,void *), int flags)
{
	struct sigaction sa;
	memset (&sa, 0, sizeof(sa));
	sa.sa_sigaction = sigHandler2;
	sa.sa_flags = flags | SA_SIGINFO;
	sigaction (nr, &sa,NULL);
}

int main (int argc, char **argv)
{	
	setHandler2(SIGTERM,sigHandler2,0);

	debug("Waiting 10 seconds\n");
	SafeSleep(10);
	debug("Finished waiting\n");

	while (1)
	{
		fd_set set;
		FD_ZERO(&set);
		FD_SET(STDIN_FILENO,&set);
		struct timeval timeout;
		memset (&timeout, 0, sizeof(timeout));
		int val = select(STDIN_FILENO+1, &set, NULL, NULL, &timeout);
		if (val < 0)
		{
			perror("select");
			return 1;
		} else
		{
			if (FD_ISSET(STDIN_FILENO, &set))
				debug ("TAK\n");
			else
				debug ("NIE\n");
		}

		debug ("Tick\n");
		SafeSleep(1);

	}

	return 0;
}
