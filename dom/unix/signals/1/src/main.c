#define _POSIX_SOURCE
#define DEBUG

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/time.h>
#include <stdarg.h>
#include <asm/siginfo.h>
#include <errno.h>
#include "lib.h"

unsigned char enabled = 0;
volatile int idx = -1;

void usage(char *program)
{
	fprintf (stderr, "%s n k p l\n", program);
	fprintf (stderr, "n - liczba potomnych procesów\n");
	fprintf (stderr, "k - przerwa przed SIGUSR1\n");
	fprintf (stderr, "p - przerwa przed SIGUSR2\n");
	fprintf (stderr, "l - liczba wyswietlen Success przez dziecko\n");
	fprintf (stderr, "EOF\n");
}

void sigHandler (int nr)
{
	plog("Child %d signal handler started\n", idx);
	
	switch (nr)
	{
		case SIGUSR1:
			plog("Signal SIGUSR1 catched\n");
			enabled = 1;
			break;
		case SIGUSR2:
			plog("Signal SIGUSR2 catched\n");
			enabled = 0;
			break;
		default:
			break;
	}
	plog("Child %d signal handler finished\n", idx);
}

void HandleChildrens()
{
	while(1)
	{
		int pid = waitpid(-1,NULL,WNOHANG);
		if (pid == 0)
			return;
		if (pid < 0)
		{
			if (errno == ECHILD)
				return;
			perror ("waitpid: ");
		}
		else
			plog ("Odebrałem dziecko %d z przedszkola ze statusem\n",
				pid);
	}

}

void SafeExit(int nr)
{
	plog("Starting to close...\n");
	HandleChildrens();
	plog ("Enabled: %x\n", enabled);
	exit(nr);

}

void parentHandler(int nr)
{
	plog("parentHandler started\n");
	switch (nr)
	{
		case SIGCHLD:
			plog ("Signal SIGCHLD catched by parent\n");
			HandleChildrens();
			break;
		case SIGTERM:
			plog("Signal SIGTERM catched by parent\n");
			if (kill(0, SIGTERM) < 0)
				perror("kill");
			SafeExit(0);	
			break;
		case SIGINT:
			plog("Signal SIGINT catched by parent\n");
			SafeExit(0);
			break;
		case SIGUSR1:
			plog("Signal SIGUSR1 catched by parent\n");
			fprintf (stderr, "finished\n");
			break;
		case SIGUSR2:
			plog("Signal SIGUSR2 catched by parent\n");
			break;
	}
	plog("parentHandler finished\n");
}

void childMain(int nr, int l)
{
	idx = nr;
	setHandler(SIGUSR1, sigHandler, 0);
	setHandler(SIGUSR2, sigHandler, 0);
	setHandler(SIGTERM, SIG_DFL, 0);
	setHandler(SIGINT, SIG_DFL, 0);
	plog("Starting child process with number: %d\n", nr);
	struct timeval t;
	gettimeofday(&t,NULL);
	time_t val = (int)t.tv_usec;
	srand(val);
	for (int i = 0; i < l; i++)
	{
		int los = rand()%6+5;
		SafeSleep(los);
		if (enabled)
			printf ("Success [%d]: %d\n", getpid(),los);
	}
}

int main (int argc, char **argv)
{
	setHandler (SIGTERM, parentHandler, 0);
	setHandler (SIGINT, parentHandler, 0);
	setHandler (SIGUSR1, parentHandler, 0);
	setHandler (SIGUSR2, parentHandler, 0);
	setHandler (SIGCHLD, parentHandler, 0);
	srand(time(0));
	if (argc != 5)
	{
		usage(argv[0]);
		return 1;
	}

	int n = atoi(argv[1]);
	int k = atoi(argv[2]);
	int p = atoi(argv[3]);
	int l = atoi(argv[4]);

	printf ("n: %d, k: %d, p: %d, l: %d\n", n, k, p,l);

	if (n <= 0 || k <= 0 || p <=0 || l <= 0)
	{
		usage(argv[0]);
		return 1;
	}

	for (int i = 0; i < n; i++)
	{
		plog ("Creating new subprocess %d\n", i);
		fflush(stdout);
		int pid = fork();
		if (pid < 0)
		{
			perror("fork error");
			return 1;
		}
		else
			if (pid == 0)
			{
				plog ("Jestem dziecko: %d\n", getpid());
				childMain(i, l);
				return 0;
			}
			else
				plog ("Kontynuuję rodzic, dziecko: %d\n",
						pid);
	}

	while (1)
	{
		SafeSleep(k);
		if (kill(0,SIGUSR1) < 0)
			perror("Kill: ");
		SafeSleep(p);
		if (kill(0,SIGUSR2) < 0)
			perror("Kill: ");
	}

	return 0;
}
