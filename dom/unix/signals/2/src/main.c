#define _GNU_SOURCE

#include <stdio.h>
#include <sys/types.h>
#include <signal.h>

#include <stdlib.h>
#include <ctype.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include "lib.h"

volatile int idx = -1;
volatile int prev = -1;
volatile int next = -1;
volatile int todoSIG1 = 0;
volatile int todoSIG2 = 0;
volatile int nLimit;
volatile int kLimit;
volatile int cycle = 1;

void usage()
{
	fprintf (stderr, "Poprawne wywolanie: ./program n k\n");
	fprintf (stderr, "n - liczba procesow\n");
	fprintf (stderr, "k - liczba cykli\n");
	fprintf (stderr, "1<n<10 , 1<k<10\n");
	fprintf (stderr, "EOL\n");
}

void SafeNanosSleep()
{
	sleep(1);
	/*
	struct timespec tt;
	for (tt = t; nanosleep(&tt,&tt);)
		if (EINTR == errno)
			perror("Nanosleep");
	*/
}

void SafeExit()
{
	if (kill(0, SIGTERM) < 0)
		perror("kill");
	waitpid (next, NULL, 0);
	exit(0);
}

void parentHandler(int nr)
{
	printf ("Rodzic\n");
	switch (nr)
	{
		case SIGUSR1:
			printf ("Rodzic, dostalem SIGUSR1\n");
			SafeExit();
			break;
		case SIGTERM:
			SafeExit();
			break;
	}
}

void childHandler(int nr)
{
	plog ("Child handler start %d\n", idx);
	sleep (1);
	switch (nr)
	{
		case SIGTERM:
			printf ("[C%d]: Request to die\n", idx);
			printf ("[C%d]: Waiting for child...\n", idx);
			if (idx < nLimit && waitpid(next, NULL, 0) < 0
 				   	&& errno != ECHILD)
				perror("waitpid: ");
			exit(0);
			break;
		case SIGINT:
	
			break;
		case SIGUSR1:
			printf ("Ja dziecko %d dostalem SIGUSR1,wysylam do %d SIGUSR1\n",
				idx, idx+1);
			if (idx < nLimit )
			{
				plog ("Moje dziecko to %d\n", next);
				kill (next,SIGUSR1);
			}
			else
			{
				plog ("cycle: %d\n", cycle);
				kill(prev,SIGUSR2);
			}
		break;
		case SIGUSR2:
			plog ("Moj rodzic to %d\n", prev);
				if (idx > 1)
				{
					printf ("Ja dziecko %d dostalem SIGUSR2,wysylam do %d SIGUSR2\n",
 						  	idx, idx-1);
					kill (prev,SIGUSR2);
				}
				else
				{
					printf ("Ja dziecko %d dostalem SIGUSR2, zaczynam cykl\n",
							idx);
					printf ("Cycle: %d\n", ++cycle);
					if (cycle > kLimit)
					{
						printf ("Odsylam do rodzica %d\n", prev);
						kill(prev,SIGUSR1);
					}
					else
						kill (next,SIGUSR1);
				}

			break;
	}
	plog ("Child handler end %d\n", idx);
	sleep (1);
}

int childMain(pid_t parent, int nr, int n, int k)
{
	printf ("Child %d started\n", nr);
	idx = nr;
	prev = parent;
	nLimit = n;
	kLimit = k;

	setHandler(SIGTERM,childHandler,0);
	setHandler(SIGUSR1,childHandler,0);
	setHandler(SIGUSR2,childHandler,0);
	plog ("Dziecko %d, rodzic %d, n: %d, k: %d\n",
			nr,parent,n,k);

	if (nr < n)
	{
		plog ("Dziecko %d rozmnazam\n", idx);
		pid_t rodzic = getpid();
		pid_t p = fork();	
		next = p;
		if (p < 0)
		{
			perror("fork: ");
			return 2;
		}

		if (p == 0)
		{
			// dziecko
			exit(childMain(rodzic, nr+1,n,k));
		}
		else
		{
			// rodzic
			
			if (next <= 0)
				return 0;

			while (1); // wait to iniftium

			return 0;

		}
	}
	else
	{

		plog ("Ja ostatnie dziecko %d wchodze w petle do zabicia\n", idx);

		while (1);
	}

	while (1);

	return 0;

}

int main (int argc, char **argv)
{
	setHandler(SIGTERM, parentHandler, 0);
	setHandler(SIGUSR1, parentHandler, 0);
	if (argc != 3)
	{
		usage();
		return 1;
	}

	/*
	if (isdigit(argv[1]) || isdigit(argv[2]))
	{
		usage();
		return 1;
	}
	*/
	int n = atoi(argv[1]);
	int k = atoi(argv[2]);

	if (n <= 1 || n >= 10 || k <= 1 || k >= 10)
	{
		usage();
		return 1;
	}

	plog ("Ja rodzic, tworze 1. dziecko\n");
	pid_t rodzic = getpid();

	pid_t p = fork();
	if (p < 0)
	{
		perror("fork: ");
		return 2;
	}

	if (p == 0)
		exit(childMain(rodzic, 1,n,k));
	else
		next = p;

	sleep (1);
	kill (next,SIGUSR1);

	plog ("Ja rodzic, czekam na dziecko\n");

	pid_t child = waitpid(next,NULL,0);
	if (child < 0)
		perror("waitpid");

	plog ("Ja rodzic skonczyem czekac na dziecko %d\n", child);

	return 0;
}
