#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/time.h>
#include <assert.h>
#include "lib.h"

volatile sig_atomic_t handled = 0;
volatile pid_t terrorysta = -1;
volatile pid_t saper = -1;
volatile int s1;

void sigHandler(int nr)
{
	plog("Handled %d\n", nr);
}

void init()
{
	struct timeval time;
	gettimeofday(&time, NULL);
	srand(time.tv_usec);
	srand48(time.tv_usec);
}

void sigTerroristHandler(int nr)
{
	if (nr == s1)
	{
		debug("Destroyed bomb, sending usr2 to proxy\n");
		if (kill(getppid(), SIGUSR2) < 0)
			perror("kill");
		exit(1);
		assert(1 == 0);
	}
}

void sigSaperHandler(int nr)
{
	switch (nr)
	{
		case SIGUSR1:
			printf ("PAWNED\n");
			exit(0);
			break;
		case SIGUSR2:
			debug("Loosed\n");
			exit(0);
			break;
	}
}

void sigProxyHandler(int nr)
{
	switch (nr)
	{
		case SIGUSR1:
				debug("PROXY: recv USR1, send USR2 to saper %d\n", saper);
				if (kill(saper, SIGUSR2) < 0)
					perror("kill");
				break;
		case SIGUSR2:
				debug("PROXY: recv USR2, send USR1 to saper %d\n", saper);
				if (kill(saper, SIGUSR1) < 0)
					perror("kill");
			break;
		case SIGCHLD:
		{
			debug("SIGCHLD\n");
			pid_t child = -1;
			for (;;) {
				child = waitpid(0, NULL, WNOHANG);

				if (0 == child) return;
				if (0 >= child) {
				if (ECHILD == errno) return;
				}

			}

			}
		break;
		default:
			if (SIGRTMIN <= nr && nr <= SIGRTMAX)
			{
				if (terrorysta <= 0)
					return;
				debug("Parent received signal %d, transfer it to terrorist %d\n",
						nr, terrorysta);
			//	if (kill(terrorysta, SIGRTMIN) < 0)
			//		perror("kill");
			
				union sigval value;
 			   	memset (&value, 0, sizeof(value));	
				if (sigqueue (terrorysta, nr, value) < 0)
					perror ("sigqueue");
				debug("Enqueued\n");

			}
		break;
	}
}

int terroristMain()
{
	debug("Terrorist launched\n");
	init();

	s1 = rand()%(SIGRTMAX-SIGRTMIN+1) + SIGRTMIN;
	debug("Sygnal rozbrojenia to: %d\n", s1);

	setHandler(s1, sigTerroristHandler, 0);

	debug("Generating t to sleep\n");

	double tNormed = drand48();
	double t = 1.0 + tNormed*9.0;

	debug("Generated t = %lf\n", t);

	struct timespec tSleep;
	memset(&tSleep, 0, sizeof(tSleep));
	tSleep.tv_sec = (int)t;
	tSleep.tv_nsec = (long int)((t - tSleep.tv_sec)*1e9);

	debug("It means %ds %luns\n", tSleep.tv_sec, tSleep.tv_nsec);

	SafeSleep2(tSleep);

	debug("Sleeping finished\n");

	printf ("KABOOM\n");

	if (kill(getppid(), SIGUSR1) < 0)
		perror("kill");

	debug("Terrorist exited\n");
	return 0;
}

int saperMain()
{
	debug("Saper launched\n");
	init();

	setHandler(SIGUSR1, sigSaperHandler, 0);
	setHandler(SIGUSR2, sigSaperHandler, 0);

	
	while (1)
	{
		int nr = rand()%(SIGRTMAX-SIGRTMIN+1) + SIGRTMIN;
		debug("Saper issues to parent %d signal %d\n", 
				getppid(), nr);
		assert (SIGRTMIN <= nr && nr <= SIGRTMAX);
		if (kill (getppid(), nr) < 0)
			perror("kill");
		SafeSleep(1);
	}
	
	debug("Saper exited\n");
	return 0;
}

int main ()
{
	debug("Posrednik launched\n");	
	init();

	debug("Setting signals handlers from RTMIN to RTMAX and USR1, CHILD\n");
	for (int i = SIGRTMIN; i <= SIGRTMAX; i++)
		setHandler(i, sigProxyHandler, 0);

	setHandler(SIGCHLD, sigProxyHandler, 0);
	setHandler(SIGUSR1, sigProxyHandler, 0);
	setHandler(SIGUSR2, sigProxyHandler, 0);

	plog ("Uruchamiam sapera\n");

	saper = fork();
	if (saper < 0)
	{
		fprintf (stderr, "Fork failed with error: '%s'\n",
				strerror(errno));
		return 1;
	}

	if (saper == 0)
	{
		// saper
		exit(saperMain());
	}

	plog ("Uruchamiam terrorystę\n");

	terrorysta = fork();
	if (terrorysta < 0)
	{
		fprintf (stderr, "Fork failed with error: '%s'\n",
				strerror(errno));
		return 1;
	}

	if (terrorysta == 0)
	{
		// terrorysta
		exit(terroristMain());
	}

	int res = -1;

	debug("Waiting for terrorysta\n");
	TEMP_FAILURE_RETRY(res = waitpid(terrorysta, NULL, 0));
	if (res < 0)
	{
		fprintf (stderr, "waitpid for terrorysta failed with error: '%s'\n",
				strerror(errno));
	}

	debug("Waiting for terrorysta finished\n");

	debug("Waiting for saper\n");

	TEMP_FAILURE_RETRY(res = waitpid(saper, NULL, 0));
	if (res < 0)
	{
		fprintf (stderr, "waitpid for saper failed with error: '%s'\n",
				strerror(errno));
	}

	debug("Waiting for saper finished\n");

	debug("Posrednik exited\n");
	return 0;
}
