#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include "lib.h"

#ifdef DEBUG
	#define debug(fmt, ...) plog(fmt, ##__VA_ARGS__)
#else
	#define debug(...)
#endif

volatile sig_atomic_t handled = 0;

char *sigNames[] = 
{
	"", // 0
	"SIGHUP", // 1
	"SIGINT", // 2
	"SIGQUIT", // 3
	"SIGILL", // 4
	"",  // 5
	"SIGABRT", // 6
	"", // 7
	"SIGFPE", // 8
	"SIGKILL", // 9
	"SIGUSR1", // 10
	"SIGSEGV", // 11
	"SIGUSR2", // 12
	"SIGPIPE", // 13
	"SIGALRM", // 14
	"SIGTERM", // 15
	"SIGUSR1", // 16
	"SIGUSR2", // 17
	"SIGCHLD", // 18
	"SIGCONT", // 19

};

void sigHandler(int nr)
{
	handled = 1;
	int max = sizeof(sigNames) / sizeof(char *);
	if (nr < max) 
		plog ("Otrzymalem sygnal %s o numerze %d\n",
 			   	sigNames[nr], nr);
	else
		plog ("Otrzymalem sygnal o numerze %d\n", nr);
	switch (nr)
	{
		case SIGINT:
			plog ("Wychodze na zyczenie operatora\n");
			exit(0);
			break;
	}
}

int SafeSleep2(struct timespec t)
{
	struct timespec t2 = t;
	int result = -1;
	while ((result = nanosleep(&t2,&t2)) < 0 && errno == EINTR)
		printf ("Breaked: remained %ds %luns\n", (int)t2.tv_sec,
				t2.tv_nsec);
	return result;
}

void block(sigset_t *setOld)
{
	sigset_t set;
	sigfillset(&set);
	sigprocmask(SIG_BLOCK, &set, setOld);
}

void unblock(sigset_t *setOld)
{
	sigset_t set;
	sigfillset(&set);
	sigprocmask(SIG_UNBLOCK, &set, setOld);
}


int main (int argc, char **argv)
{
	debug("blocking signal handling for me\n");
	block(NULL);
	debug("Falling asleep\n");
	debug("a %d\n", 1);

	setHandler(SIGINT, sigHandler, 0);

	struct timespec t;
	t.tv_sec = 10;
	t.tv_nsec = 0;
	SafeSleep2(t);

	debug("unblocking signal handling\n");
	unblock(NULL);
	debug("Falling asleep\n");

	SafeSleep2(t);

	return 0;
}
