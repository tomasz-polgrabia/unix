#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include "lib.h"

volatile sig_atomic_t handled = 0;

char *sigNames[] = 
{
	"", // 0
	"SIGHUP", // 1
	"SIGINT", // 2
	"SIGQUIT", // 3
	"SIGILL", // 4
	"",  // 5
	"SIGABRT", // 6
	"", // 7
	"SIGFPE", // 8
	"SIGKILL", // 9
	"SIGUSR1", // 10
	"SIGSEGV", // 11
	"SIGUSR2", // 12
	"SIGPIPE", // 13
	"SIGALRM", // 14
	"SIGTERM", // 15
	"SIGUSR1", // 16
	"SIGUSR2", // 17
	"SIGCHLD", // 18
	"SIGCONT", // 19

};

void sigHandler(int nr)
{
	handled = 1;
	int max = sizeof(sigNames) / sizeof(char *);
	if (nr < max) 
		plog ("Otrzymalem sygnal %s o numerze %d\n",
 			   	sigNames[nr], nr);
	else
		plog ("Otrzymalem sygnal o numerze %d\n", nr);
	switch (nr)
	{
		case SIGINT:
			plog ("Wychodze na zyczenie operatora\n");
			exit(0);
			break;
	}
}

int SafeSleep2(struct timespec t)
{
	struct timespec t2 = t;
	int result = -1;
	while ((result = nanosleep(&t2,&t2)) < 0 && errno == EINTR)
		printf ("Breaked: remained %ds %luns\n", (int)t2.tv_sec,
				t2.tv_nsec);
	return result;
}

int main (int argc, char **argv)
{
	for (int i = 0; i < 30; i++)
		if (setHandler(i,sigHandler,0) < 0)
			perror("setHandler: ");
	struct timespec t1;
	sigset_t set,set2;
	sigfillset(&set);
	sigfillset(&set2);

	if (sigprocmask(SIG_SETMASK, &set, &set2) < 0)
	{
		fprintf (stderr, "Error %d", i);
		perror("sigprocmask");
		return 1;
	}

	plog ("Probuje poczekac 5200ms\n");

	t1.tv_sec = 5;
	t1.tv_nsec = 200 * 1e6;

	if (SafeSleep2(t1) < 0)
		perror ("SafeSleep2:");

	if (sigprocmask(SIG_SETMASK, &set2, NULL) < 0)
	{
		perror("sigprocmask");
		return 1;
	}

	sigemptyset(&set);

	if (sigsuspend(&set) < 0 && errno != EINTR)
	{
		perror("sigsuspend");
		return 1;
	}

	sigfillset(&set);
	if (sigprocmask(SIG_SETMASK, &set, NULL) < 0)
	{
		perror("sigprocmask");
		return 1;
	}

	plog ("Program got signal\n");

	return 0;
}
