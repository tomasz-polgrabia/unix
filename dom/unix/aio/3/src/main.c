#include <string.h>
#include <aio.h>

int main ()
{	
	struct aioinit ainit;

	memset (&ainit,0, sizeof(ainit));
	ainit.aio_threads = 4;
	ainit.aio_num = 4;
	ainit.aio_idle_time = 1;
	
	aio_init(&ainit);
	return 0;
}
