#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "lib.h"

#include <aio.h>

volatile sig_atomic_t handled = 0;

void sigHandler(int nr)
{
	handled = 1;
}

int main (int argc, char **argv)
{	
	if (argc != 2)
	{
		puts("Nie podales nazwy pliku");
		return 1;
	}

	int fd = open (argv[1], O_RDONLY);
	if (fd < 0)
	{
		perror ("open");
		return 1;
	}

	setHandler(SIGUSR1,sigHandler, 0);


	struct aioinit ainit;

	memset (&ainit,0, sizeof(ainit));
	ainit.aio_threads = 4;
	ainit.aio_num = 4;
	ainit.aio_idle_time = 1;
	
	aio_init(&ainit);


	struct aiocb aread;
	char buffer[1000];
	memset(&aread, 0, sizeof(aread));
	memset(&buffer, 0, sizeof(buffer));
	aread.aio_fildes = fd;
	aread.aio_offset = 0;
	aread.aio_buf = buffer;
	aread.aio_nbytes = 500;
	aread.aio_reqprio = 1;
	aread.aio_sigevent.sigev_notify = SIGEV_NONE;
	
	aio_read(&aread);
	puts ("Waiting for data\n");
	
	int c = 0;
	while(aio_error(&aread) == EINPROGRESS)
	{
		SafeSleep(1);
		printf ("\033[1K\rReading %d", c++);
		fflush (stdout);
	}
	
	ssize_t size = aio_return (&aread);
	if (size < 0)
	{
		perror ("aio_return");
		return 1;
	}
		printf ("\nWczytano %s o dl %d\n", buffer, size);

	if (close(fd) < 0)
	{
		perror ("close");
		return 1;
	}


	return 0;
}
