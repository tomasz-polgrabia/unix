#ifndef __USER_CLIENT__
#define __USER_CLIENT__

#include <stdio.h>
#include <stdlib.h>
#include <thread_info.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>

#include <sockets.h>

#include <signal.h>

#include <pthread.h>

#define BUFFER_LENGTH 0x1000

#endif
