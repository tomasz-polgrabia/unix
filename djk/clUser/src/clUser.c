#include <clUser.h>

void usage()
{
	printf("Poprawne uzycie: ./program [host] [port]\n");
}

int fd = -1;
struct thread_info info;

void *
socketListener(void *arg)
{
	char buffer[BUFFER_LENGTH];
	struct thread_info *info = (struct thread_info *) arg;
	int *fd = info->arg;

	printf ("Listener fd: %d\n", *fd);

	while (1)
	{
		memset (buffer, 0, BUFFER_LENGTH);
		printf ("Waiting for data\n");
		int count = TEMP_FAILURE_RETRY(recv(*fd, buffer, BUFFER_LENGTH, 0));
		if (count < 0)
		{
			perror("recv");
			pthread_exit(NULL);
		}
		printf ("Finished Waiting for data\n");

		if (count == 0)
		{
			printf ("Closing\n");
			close(0);
			close(1);
			close(*fd);
			break;
		}

		printf("%s\n", buffer);
	}

	return NULL ;
}

void sigHandler(int nr)
{
	switch (nr)
	{
	case SIGINT:
		printf("SIGINT\n");
		close(0);
		close(1);
		fprintf (stderr, "Closing fd: %d\n", fd);
		if (close(fd) < 0)
		{
			perror("close");

		}
		if (info.thread_id > 0)
			pthread_cancel(info.thread_id);
		fprintf (stderr, "Closed\n");
		break;
	case SIGTERM:
		break;
	}
}

void setHandlers()
{
	struct sigaction action;

	memset(&action, 0, sizeof(action));
	action.sa_handler = sigHandler;
	sigfillset(&action.sa_mask);

	if (sigaction(SIGINT, &action, NULL ) < 0)
	{
		perror("sigaction");
		abort();
	}

	if (sigaction(SIGTERM, &action, NULL ) < 0)
	{
		perror("sigaction");
		abort();
	}

}

void readForCommands(char buffer[BUFFER_LENGTH], int fd)
{
	int result = 1;
	memset (buffer, 0, sizeof(char)*BUFFER_LENGTH);
	while ((result = read(STDIN_FILENO,buffer,BUFFER_LENGTH)) > 0)
	{
		printf ("Sending: %s\n", buffer);
		if (send(fd, buffer, strlen(buffer), 0) < 0)
		{
			break;
		}
		memset (buffer, 0, sizeof(char)*BUFFER_LENGTH);

	}

	fprintf (stderr, "Finished reading\n");
}

int createListener(int *fd, pthread_attr_t* attr, struct thread_info* info)
{
	pthread_attr_init(attr);
	pthread_attr_setdetachstate(attr, PTHREAD_CREATE_JOINABLE);
	info->arg = fd;
	if (pthread_create(&(info->thread_id), attr, socketListener, info) < 0)
	{
		perror("pthread_create");
		exit(3);
	}
	return 0;
}

int createConnection(int port, struct sockaddr_in* addr, char* host)
{
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0)
	{
		perror("socket");
		abort();
	}

	MakeSocketStruct(inet_addr(host), port, &*addr);
	if (connect(fd, (struct sockaddr*) &*addr, sizeof(*addr)) < 0)
	{
		perror("connect");
		exit(1);
	}

	return fd;
}

void closeConnection(int fd)
{
	TEMP_FAILURE_RETRY(close(fd));
}

void DestroyAttrAndWaitForListener(pthread_attr_t* attr,
		struct thread_info* info)
{
	pthread_attr_destroy(attr);
	fprintf (stderr, "Join listener\n");

	void *ret = NULL;

	fprintf (stderr, "Thread id: %d\n", (int)info->thread_id);

	if ((errno = pthread_join(info->thread_id, &ret )) != 0)
	{
		perror("pthread_join");
	}
}

void checkForArguments(int argc)
{
	if (argc != 3)
	{
		usage();
		exit(1);
	}
}

int main(int argc, char **argv)
{
	char buffer[BUFFER_LENGTH];
	pthread_attr_t attr;
	char *host = NULL;
	int port = -1;
	struct sockaddr_in addr;

	setHandlers();

	checkForArguments(argc);

	host = argv[1];
	port = atoi(argv[2]);

	printf("Host: %s\nPort: %d\n", host, port);

	fd = createConnection(port, &addr, host);
	createListener(&fd, &attr, &info);
	readForCommands(buffer, fd);

	fprintf (stderr, "Destroying\n");
	DestroyAttrAndWaitForListener(&attr, &info);
	closeConnection(fd);

	return 0;
}
