#include <commandHandler.h>

void closeResources(comListParam* param)
{
	;
	if (TEMP_FAILURE_RETRY(mq_close(param->msgQueue)) < 0)
	{
		perror("mq_close");
		abort();
	}
	free(param);
}

static void command_canceled(void *arg)
{
	debug("Canceling thread\n");
	struct thread_info *info = (struct thread_info *) arg;
	comListParam *param = (comListParam *) info->arg;

	free(info);
	free(param);
}

void logoutUser(serverState *state2, int id)
{
	int i = 0;
	if (id < 1)
		return;

	for (i = 0; i < state2->usersBlocks * USER_BLOCK_SIZE; i++)
	{
		if (state2->users[i].id == id)
		{
			state2->users[i].loggedIn = 0;
			return;
		}
	}
}

void *
commandHandler(void *arg)
{
	struct thread_info *info = (struct thread_info *) arg;
	comListParam *param = (comListParam *) info->arg;
	serverState *state2 = param->serverState;
	pthread_cleanup_push(command_canceled,arg)
				;

				char buffer[INPUT_BUFFER_LENGTH];
				char longBuffer[LONG_INPUT_BUFFER_LENGTH];
				memset(buffer, 0, sizeof(buffer));
				memset(longBuffer, 0, sizeof(longBuffer));

				char messageName[100];
				sprintf(messageName, "/eofeof_%d", param->idx + 1);

				struct mq_attr attr;
				memset(&attr, 0, sizeof(attr));

				attr.mq_maxmsg = 30;
				attr.mq_msgsize = INPUT_BUFFER_LENGTH;

				mqd_t messageQueue = TEMP_FAILURE_RETRY(
						mq_open(messageName, O_WRONLY | O_CREAT, 0777, NULL ));

				param->msgQueue = messageQueue;

				debug("Input opening message queue: %s\n", messageName);

				if (messageQueue < 0)
				{
					perror("input mq_open");
					abort();
				}

				while (1)
				{
					memset(buffer, 0, sizeof(char) * INPUT_BUFFER_LENGTH);
					int res = bulk_recv(param->fd, buffer, INPUT_BUFFER_LENGTH,
							0); // FIXME bulk_recv

					if (res < 0)
					{
						if (errno == EBADF)
							return NULL ;
						perror("recv error: ");
						break;
					}
					if (res == 0)
					{
						// shutdown handler
						handleShutdown(state2,param->idx); // FIXME
						debug("Sending message to the output thread to exit\n");
						char message[INPUT_BUFFER_LENGTH];
						memset(message, 0, sizeof(message));
						// FIXME zamień to na posixowe

						if (mq_send(messageQueue, message, 1, 0) < 0)
						{
							perror("mq_gsnd");
							exit(3);
						}
						break;
					}
					else
					{
						if (pthread_mutex_lock(&mutex) < 0)
						{
							perror("Error locking");
							abort();
						}debug("(%d), Received message: %s\n", res, buffer);
						// teraz musimy pollować za sendem

						switch (state2->clients[param->idx].state)
						{
						case BUG_LONG_DESCRIPTION:
							handleLongContent(buffer, param->idx, longBuffer,
									state2->clients);
							if (state2->clients[param->idx].state == READY)
							{
								if (mq_send(messageQueue, buffer,
										strlen(buffer), 0) < 0)
								{
									perror("mq_gsnd");
									exit(3);
								}
							}
							break;
						case COMMENT_INPUT:
							handleCommentInput(buffer, buffer, longBuffer,
									&state2->clients[param->idx], state2, param);
							break;
						default:
//							handleInteractiveMode(res, buffer, param->idx,
//									state2->clients, state2->users, longBuffer, param,
//									messageQueue);
							handleInteractiveMode(res,buffer, param->idx, state2->clients, state2,longBuffer,param,messageQueue);
						}

						if (pthread_mutex_unlock(&mutex) < 0)
						{
							perror("Error locking");
							abort();
						}

					}

				}

				pthread_cleanup_pop(0);

	debug("Command finished\n");

	if ((errno = pthread_mutex_lock(&mutex)) != 0)
	{
		perror("pthread_mutex_lock");
	}

	closeResources(param);

	struct thread_info *temp = threadsToJoin.next;
	threadsToJoin.next = info;
	if (temp != NULL)
		temp->prev = info;
	info->prev = &threadsToJoin;
	info->next = temp;

	if ((errno = pthread_mutex_unlock(&mutex)) != 0)
	{
		perror("pthread_mutex_lock");
	}

	pthread_exit(NULL);
	return NULL ;
}

void handleShutdown(serverState *state2, int i)
{
	if (pthread_mutex_lock(&mutex) < 0)
	{
		perror("Error locking");
		abort();
	}

	logoutUser(state2, state2->clients[i].id);

	// shutdown handler
	debug("Shutdown of %d\n", state2->clients[i].fd);
	--state2->clientsCount;
	close(state2->clients[i].fd);
	state2->clients[i].fd = -1;
	state2->clients[i].id = -1;
	state2->clients[i].state = LOGGED_OUT;

	if (pthread_mutex_unlock(&mutex) < 0)
	{
		perror("Error locking");
		abort();
	}

}

int handleInteractiveMode(int res, char *buffer, int idx, client *clients,
		serverState *state2, char *longContent, comListParam *param, mqd_t queue)
{
	res = executeRequest(buffer, &clients[idx], state2->users, state2->users, param, queue);
	switch (res)
	{
	case REQUEST_RES_NORMAL:
		break;
	case REQUEST_RES_QUIT:
		--state2->clientsCount;
		debug("Cleaning after exit\n");
		break;
	case REQUEST_RES_USER_ARG_ERROR:
		debug("Uzytkownik wydal niepoprawna komende\n");
		break;
	case REQUEST_RES_BUG_ACCEPT:
		debug("Uzytkownik zaakceptował wysyłanie buga\n");
		int idSupport = -1;
		handleSendingBug(state2, &clients[idx], clients, buffer, state2->users, &idSupport,
				param, queue);
		// TODO wyślij to kolejką msgsnd
		memset(longContent, 0, sizeof(char) * 0x4000);
		break;
	case REQUEST_RES_BUG_DEFERRED:
		debug("Uzytkownik odrzucił wysyłanie buga\n");
		memset(longContent, 0, sizeof(char) * 0x4000);
		break;
	default:
		debug("Nieznany wynik funkcji, aborting\n");
		abort();
	}
	return res;
}

void sndMessage(char *temp, comListParam* param, mqd_t queue)
{
	char message[INPUT_BUFFER_LENGTH];
	int size = INPUT_BUFFER_LENGTH;
	memset(&message, 0, sizeof(message));
	memcpy(message, temp, strlen(temp));

	if (TEMP_FAILURE_RETRY(mq_send(queue, (char *) &message, size, 0)) < 0)
	{
		perror("mq_send");
		abort();
	}

}

int executeRequest(char *str, client *c, user *users, user *loggedUsers,
		comListParam *param, mqd_t queue)
{
	debug("Debug (str): %s\n", str);
	char temp[INPUT_BUFFER_LENGTH];
	memset(temp, 0, sizeof(temp));
	char *trimmedRequest = trim(str);

	if (strlen(trimmedRequest) <= 0)
	{
		debug("Dlugosc danych jest zerowa");
		sprintf(str, "Unknown command\n");
		return REQUEST_RES_USER_ARG_ERROR;
	}

	char *params = strstr(str, " ");
	char *command = str;

	if (params != NULL )
	{
		params[0] = '\0';
		params = trim(params + 1);
	}
	else
	{
		params = command + strlen(command);
	}

	for (int i = 0; i < MAX_COMMANDS; i++)
	{
		if (strlen(jTable[i].command) <= 0 || jTable[i].execute == NULL )
			continue;

		debug("Porownuje %s z %s\n",command,jTable[i].command);

		int length = strlen(command);
		if (length != strlen(jTable[i].command))
			continue;

		if (strncasecmp(command, jTable[i].command, length) == 0)
		{
			int res = jTable[i].execute(c,param->serverState,params,temp,param);
			memset(str, 0, INPUT_BUFFER_LENGTH);
			sndMessage(temp, param, queue);
			return res;
		}
	}

	sprintf(str, "Unknown command\n");
	sndMessage(str, param, queue);

	return REQUEST_RES_USER_ARG_ERROR;

}

int helpExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param)
{
	if (c->state != READY)
	{
		sprintf(output, "Not supposed to be used here\n");
		return 0;
	}

	sprintf(output,
			"List of commands\nhelp: prints help\nuser [login] request to log as [login]\n");
	sprintf(output + strlen(output),
			"pass [password] give password to auhorize\n");
	sprintf(output + strlen(output), "list: list bugs\n");
	sprintf(output + strlen(output), "logout: logouts\n");
	sprintf(output + strlen(output), "kind: kind of bug\n");
	sprintf(output + strlen(output), "tool: tool affected in bug\n");
	sprintf(output + strlen(output), "description: short description\n");
	sprintf(output + strlen(output), "comment [idBug]: comments bug\n");
	sprintf(output + strlen(output), "abort: reset state\n");
	sprintf(output + strlen(output), "quit: quits\n");
	sprintf(output + strlen(output), "status: status of client\n");
	sprintf(output + strlen(output),
			"bugaccept [YES|NO]: accept or defer bug\n");
	sprintf(output + strlen(output), "view [idBug] : views bug of id\n");
	sprintf(output + strlen(output), "accept [idBug]: states bug as solved\n");
	sprintf(output + strlen(output),
			"comments [idBug]: list comments of bug\n");
	sprintf(output + strlen(output),
			"showComment [idComment] : shows comment with id\n");

	return 0;
}

int abortExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param)
{
	if (c->state != READY)
	{
		c->state = READY;
		sprintf(output, "ABORTED - ready\n");
	}
	else
	{
		sprintf(output, "Currently in ready state\n");
	}
	return 0;
}

int quitExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param)
{
	if (c->state != READY)
	{
		sprintf(output, "Not supposed to be used here\n");
	}
	else
	{

		if (c->id > 0)
		{
			for (int i = 0; i < state->usersBlocks * USER_BLOCK_SIZE; i++)
			{
				if (state->users[i].id == c->id)
				{
					state->users[i].loggedIn = 0;
				}
			}
		}

		c->id = -1;
		c->rank = LOGGED_OUT;
		c->state = READY;
		if (TEMP_FAILURE_RETRY(close(c->fd)) < 0)
			return -1;
	}

	return 1;
}

