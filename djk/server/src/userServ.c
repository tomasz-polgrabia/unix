#include <userServ.h>

int userExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	char *login = trim(params);
	debug("[u %d] User %s wants login\n", c->fd, login);

	if (c->state != READY)
	{
		debug("[u %d] Not supposed to be used here\n", c->fd);
		sprintf(output, "Not supposed to be used here\n");
		return 0;
	}

	if (c->rank == LOGGED_OUT)
	{
		int found = 0;

		debug("[u %d], szukanie wolnego slotu w tablicy\n", c->fd);
		for (int i = 0; i < state2->usersBlocks * USER_BLOCK_SIZE; i++)
		{
			if (state2->users[i].id >= 1) // FIXME check
			{
				// znaleźliśmy wolny slot
				if (strcasecmp(login, state2->users[i].login) == 0)
				{
					if (state2->users[i].loggedIn)
					{
						debug("[u %d] login currently used, aborted\n", c->fd);
						sprintf(output, "Login currently used, aborted\n");
						return 0;
					}

					found = 1;
					c->id = state2->users[i].id;
					break; // istnieje w naszej bazie
				}
			}
		}

		if (found)
		{
			sprintf(output, "Login request OK, give password\n");
			c->state = LOGIN_REQUEST;
		}
		else
		{
			debug("Creating user: %s\n", params);

			for (int i = 0; i < state2->usersBlocks * USER_BLOCK_SIZE; i++)
			{
				if (state2->users[i].id < 1)
				{
					// free slot

					int idUser = state2->userCounter++;
					state2->users[i].id = idUser;
					state2->users[i].loggedIn = 0;
					state2->users[i].createPassword = 1;
					strcpy(state2->users[i].login, trim(params));
					state2->users[i].handlingBugs = 0;
					c->id = idUser;
					c->u.createPassword = 1;

					debug("Slot %d.\n", i);

					break;

				}
			}

			if (c->id < 0)
			{
				sprintf(output,"Max users exceeded\n");
				return 0;
			}

			c->state = LOGIN_REQUEST;

			sprintf(output, "Creating new user: %s\n", login);
			return 0;
		}
	}
	else
		sprintf(output, "You are already logged in or trying logging in\n");

	sprintf(output + strlen(output), "Bugs for you: ");

	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
	{
		if (c->id == state2->bugs[i].idSupport)
			sprintf(output + strlen(output), "%d,", state2->bugs[i].id);
	}
	sprintf(output + strlen(output), "\n");

	return 0;
}

int passExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	debug("User fd: %d wants give the password\n", c->fd);

	char *password = trim(params);

	if (c->state == LOGIN_REQUEST)
	{
		int idx = 0;
		int found = 0;

		for (idx = 0; idx < state2->usersBlocks * USER_BLOCK_SIZE; idx++)
			if (state2->users[idx].id == c->id)
			{
				if (state2->users[idx].loggedIn)
				{
					debug("Fd: %d, login currently is used", c->fd);
					sprintf(output, "Login currently used, aborted\n");
					c->id = -1;
					c->state = READY;
					c->rank = LOGGED_OUT;
					return 0;
				}
				found = 1;
				break;
			}
		debug("Client id: %d\n", c->id);
		debug("Slot: %d\n", idx);

		debug("Found: %d, create: %d\n", found, state2->users[idx].createPassword);
		if (found && state2->users[idx].createPassword)
		{
			debug("Creating user\n");
			memset (state2->users[idx].password, 0, sizeof(char)*MAX_PASSWORD_LENGTH);
			strcpy(state2->users[idx].password, trim(params));
			sprintf(output, "Created new user, id: %d\n", state2->users[idx].id);
			c->state = READY;
			c->rank = LOGGED_IN;
			c->u = state2->users[idx];
			c->u.createPassword = 0;
			state2->users[idx].createPassword = 0;
			return 0;
		}

		if (!found || strcmp(state2->users[idx].password, password) != 0)
		{
			debug("User fd: %d, authentication failed\n", c->fd);
			c->state = READY;
			sprintf(output, "Authorization failed\n");
		}
		else
		{
			debug("User fd: %d, was not logged\n", c->fd);
			c->state = READY;
			c->rank = LOGGED_IN;
			c->u = state2->users[idx];
			state2->users[idx].loggedIn = 1;
			sprintf(output, "You are logged in now \n");
		}
	}
	else
		sprintf(output, "Not expected here\n");

	return 0;
}

int logoutExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{

	debug("User id: %d, state: 0x%x, requested logout action\n", c->id, c->state);

	if (c->state != READY)
	{
		sprintf(output, "Command not expected in current context\n");
		return 0;
	}

	int found = 0;

	if (c->rank == LOGGED_IN)
	{

		for (int i = 0; i < state2->usersBlocks * USER_BLOCK_SIZE; i++)
		{
			if (state2->users[i].id == c->id)
			{
				debug("User id: %d found in users table\n", c->id);
				assert(state2->users[i].loggedIn);
				state2->users[i].loggedIn = 0;
				memset(&c->u, 0, sizeof(user));
				found = 1;
				c->id = -1;
				c->rank = LOGGED_OUT;
				break;
			}
		}

		if (!found)
		{
			debug("User id: %d not found in users table\n", c->id);
			sprintf(output, "User not found, aborting\n");
			return 0;
		}

		sprintf(output, "You are now logged out\n");
	}
	else
	{
		debug("User id: %d was not logged\n", c->id);
		sprintf(output, "You were not logged\n");
	}

	return 0;
}

