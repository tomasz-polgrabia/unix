#include <outputSender.h>

sig_atomic_t lastTicketfree = 0;

void closeResources2(comListParam* param)
{
	;
	if (TEMP_FAILURE_RETRY(mq_close(param->msgQueue)) < 0)
	{
		perror("mq_close");
		abort();
	}
	free(param);
}

static void command_canceled(void *arg)
{
	debug("Canceling thread\n");
	struct thread_info *info = (struct thread_info *) arg;
	comListParam *param = (comListParam *) info->arg;

	debug("Output: Closintg resources\n");
	closeResources2(param);
	free(info);
}

void *
outputHandler(void *arg)
{
	struct thread_info *info = (struct thread_info *) arg;
	comListParam *param = (comListParam *) info->arg;

	pthread_cleanup_push(command_canceled,arg);

	char messageName[100];
	sprintf(messageName, "/eofeof_%d", param->idx + 1);
	debug ("Output opening message queue: %s\n", messageName);

	mqd_t messageQueue = TEMP_FAILURE_RETRY(
			mq_open(messageName, O_RDONLY | O_CREAT, 0777, NULL ));

	param->msgQueue = messageQueue;

	if (messageQueue < 0)
	{
		perror("output mq_open");
		abort();
	}

	char message[INPUT_BUFFER_LENGTH];
	debug("Waiting for message\n");

	while (1)
	{
		memset(&message, 0, sizeof(message));

		if (TEMP_FAILURE_RETRY(
				mq_receive(messageQueue, (char *) &message, INPUT_BUFFER_LENGTH,
						NULL )) < 0)
		{
			perror("mq_receive");
			abort();
		}

		debug("Output thread: got message '%s' to send\n", message);

		int fd = param->fd;

		debug("FD: %d\n", fd);

		int count = bulk_send(fd, message, strlen(message) + 1, 0);

		debug("Message sent to the fd: %d\n", fd);
		//debug("Exited\n");
		if (count < 0)
		{
			if (errno == EPIPE)
			{
				debug("SIGPIPE handled\n");
				break;
			}
			if (errno == EBADF || errno == ENOTSOCK)
			{
				break;
			}
			perror("send");
			break;
		}

	}

	pthread_cleanup_pop(0);

	debug("output exiting\n");

	if ((errno = pthread_mutex_lock(&mutex)) != 0)
	{
		perror("pthread_mutex_lock");
	}

	closeResources2(param);

	struct thread_info *temp = threadsToJoin.next;
	threadsToJoin.next = info;
	if (temp != NULL)
		temp->prev = info;
	info->prev = &threadsToJoin;
	info->next = temp;

	if ((errno = pthread_mutex_unlock(&mutex)) != 0)
	{
		perror("pthread_mutex_lock");
	}

	pthread_exit(NULL);
	return NULL ;
}

void reallocateIfNeeded(serverState *state2)
{
	if (state2->bugsCount >= state2->bugsBlocks * BUG_BLOCK_SIZE)
	{
		debug("Bugs count: %d\n", state2->bugsCount);
		debug("Reallocation bugs, blocks: %d\n", state2->bugsBlocks);
		++state2->bugsBlocks;
		debug("after Reallocation bugs, blocks: %d\n", state2->bugsBlocks);
		state2->bugs = realloc(state2->bugs, sizeof(bug) * state2->bugsBlocks * BUG_BLOCK_SIZE);
		if (state2->bugs == NULL )
		{
			perror("Allocation failed");
			abort();
		}

		memset(&state2->bugs[(state2->bugsBlocks - 1) * BUG_BLOCK_SIZE], 0,
				sizeof(bug) * BUG_BLOCK_SIZE);

		state2->longContent = realloc(state2->longContent,
				sizeof(char *) * state2->bugsBlocks * BUG_BLOCK_SIZE);

		for (int i = (state2->bugsBlocks - 1) * BUG_BLOCK_SIZE;
				i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
		{
			state2->longContent[i] = (char *) malloc(
					sizeof(char) * LONG_INPUT_BUFFER_LENGTH);
			if (state2->longContent[i] == NULL )
			{
				perror("Allocation failed");
				abort();
			}
		}

	}
}

int findSupporter(serverState *state2, int minBugs, int idSupporter, user* users, int* idxSupporter)
{
	for (int i = 0; i < state2->usersBlocks * USER_BLOCK_SIZE; i++)
	{
		if (strncmp(users[i].login, "sup", 3) == 0)
		{
			debug("Supporter id: %d, login: %s has %d bugs to handle\n",
					users[i].id, users[i].login, users[i].handlingBugs);
			// FIXME pierwszy lepszy, a powinno być, który ma najmniej
			if (minBugs > users[i].handlingBugs)
			{
				minBugs = users[i].handlingBugs;
				debug("Supporter id: %d, login: %s got bug\n",
						users[i].id, users[i].login);
				idSupporter = users[i].id;
				*idxSupporter = i;
			}
		}
	}
	return idSupporter;
}

void findClient(serverState *state2, client *c, int idSupporter, char message[INPUT_BUFFER_LENGTH], int idxBug, mqd_t queue,
		client* clients)
{
	for (int i = 0; i < state2->clientsBlocks * CLIENT_BLOCK_SIZE; i++)
	{
		if (clients[i].id == idSupporter)
		{
			debug("New bug id: %d, support Id: %d, Idx: %d\n", state2->bugs[idxBug].id, idSupporter, i);
			memset (message, 0, sizeof(char)*INPUT_BUFFER_LENGTH);
			debug("IMPORTANT: message1: %s\n", message);
			sprintf(message, "New bug: %d\n", state2->bugs[idxBug].id);
			debug("IMPORTANT: message2: %s\n", message);

			debug("Sending notification to the supported id: %d, fd: %d\n", idSupporter, clients[i].fd);

			if (bulk_send(clients[i].fd, message, strlen(message), 0) < 0)
			{
				perror("bulk_send");
				abort();
			}

			debug("[%s] I queuing message to the sender thread: '%s'\n", c->u.login, message);

			if (mq_send(queue, message, INPUT_BUFFER_LENGTH, 0) < 0)
			{
				perror("lol");
				abort();
			}

			return;
		}
	}
}

void fillBug(serverState *state2, int idxBug, client* c)
{
	memcpy(&(state2->bugs[idxBug]), &(c->tempBug), sizeof(bug));
	memset(&c->tempBug, 0, sizeof(bug));
	state2->bugs[idxBug].id = state2->bugCounter++;
	state2->bugs[idxBug].idAuthor = c->id;
	state2->bugs[idxBug].idSupport = -1;
}

void FillUser(serverState *state2, int idxSupporter, int idxBug, int idSupporter, user* users)
{
	users[idxSupporter].handlingBugs++;
	state2->bugs[idxBug].idSupport = idSupporter;
}

int handleSendingBug(serverState *state2, client *c, client *clients, char *output, user *users,
		int *idSuphport, comListParam *param, mqd_t queue)
{
	int idxBug = -1, idSupporter = -1;

	reallocateIfNeeded(state2);

	++state2->bugsCount;

	debug("Last ticket free: %d\n", lastTicketfree);
	//debug("Id of supported of this bug: %d\n", state2->bugs[lastTicketfree].idSupport);

	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
	{
		debug("Loop iteration: %d, state2->bugs[lastTicketfree].idSupport = %d\n",
				lastTicketfree, state2->bugs[lastTicketfree].idSupport);
		if (state2->bugs[lastTicketfree].idSupport < 1)
		{
			debug("free slot found on the idx: %d\n", lastTicketfree);
			// free slot found
			idxBug = lastTicketfree;
			break;
		}
		lastTicketfree = (lastTicketfree + 1) % (state2->bugsBlocks * BUG_BLOCK_SIZE);
	}

	if (idxBug < 0)
	{
		debug("Too many bugs, I cannot take it\n");
		sprintf(output, "Too many bugs, I cannot take it\n");
		return -1;
	}

	lastTicketfree = (lastTicketfree + 1) % (state2->bugsBlocks * BUG_BLOCK_SIZE);

	debug("Bug placed in table on the index: %d\n", idxBug);

	debug("Placing bug on the idx: %d\n", idxBug);

	fillBug(state2,idxBug, c);
	int minBugs = MAX_INT;

	idSupporter = -1;
	int idxSupporter = -1;

	idSupporter = findSupporter(state2, minBugs, idSupporter, users, &idxSupporter);

	if (idSupporter < 0)
	{
		sprintf(output, "WTF no support man\n");
		return 0;
	}

	FillUser(state2,idxSupporter, idxBug, idSupporter, users);
	char message[INPUT_BUFFER_LENGTH];
	memset(message, 0, sizeof(char) * INPUT_BUFFER_LENGTH);

	findClient(state2,c,idSupporter, message, idxBug, queue, clients);
	memset(message, 0, sizeof(char) * INPUT_BUFFER_LENGTH);

	sprintf(output, "HANDLED Sending bug, I gave him id: %d\n",
			state2->bugs[idxBug].id);
	return 0;
}

