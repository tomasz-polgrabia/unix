/*
 * server.c
 *
 *  Created on: 04-05-2013
 *      Author: duga
 */

#include <server.h>

#include <execinfo.h>

struct thread_info threadsToJoin;

#define DUMP_SIZE 100

pthread_mutex_t mutex;

int memoryCounter = 0;
int allocationCounter = 0;
int freeingCounter = 0;
// Grochowska 142

void dump()
{
	int nptrs;
	void *buffer[DUMP_SIZE];
	char **strings;

	nptrs = backtrace(buffer, DUMP_SIZE);

	strings = backtrace_symbols(buffer, nptrs);

	if (strings == NULL )
	{
		perror("backtrace_symbols");
		abort();
	}

	for (int i = 1; i < nptrs; i++)
	{
		printf("[%d] %s\n", i - 1, strings[i]);
	}

	free(strings);

}

jumpTable jTable[] =
{
{ .command = "help", .execute = helpExecutor },
{ .command = "user", .execute = userExecutor },
{ .command = "pass", .execute = passExecutor },
{ .command = "list", .execute = listExecutor },
{ .command = "bug", .execute = bugExecutor },
{ .command = "logout", .execute = logoutExecutor },
{ .command = "kind", .execute = kindExecutor },
{ .command = "tool", .execute = toolExecutor },
{ .command = "description", .execute = shortDescExecutor },
{ .command = "comment", .execute = commentExecutor },
{ .command = "abort", .execute = abortExecutor },
{ .command = "quit", .execute = quitExecutor },
{ .command = "status", .execute = statusExecutor },
{ .command = "bugaccept", .execute = bugAcceptExecutor },
{ .command = "view", .execute = viewExecutor },
{ .command = "accept", .execute = acceptExecutor },
{ .command = "comments", .execute = commentsExecutor },
{ .command = "showComment", .execute = showCommentExecutor }


};

//serverState state =
//{
//		.bugCounter = 0,
//		.bugs = NULL,
//		.bugsBlocks = 1,
//		.bugsCount = 0,
//		.clientPrefix = "cli_",
//		.supportPrefix = "sup_",
//		.commentsCounter = 0,
//		.comments = NULL,
//		.commentsBlocks = 1,
//		.commentsCount = 0,
//		.clients = NULL,
//		.clientsBlocks = 1,
//		.clientsCount = 0,
//		.users = NULL,
//		.userCounter = 0,
//		.usersBlocks = 1,
//		.usersCount = 0,
//		.longContent = NULL,
//		.working = 1
//};

void usage()
{
	fprintf(stderr, "Nie prawidlowe użycie\n");
}

void
setHandlers();

sig_atomic_t working = 1;

void sigHandler(int nr)
{
	switch (nr)
	{
	case SIGTERM:
		working = 0;
		debug("SIGTERM\n");
		break;
	case SIGINT:
		working = 0;
		debug("SIGINT\n");
		break;
	case SIGPIPE:
		debug("SIGPIPE\n");
		break;

	case SIGABRT:
		debug("ABORTED");

#ifdef NDEBUG
		dump();
#endif

		break;
	}

}

int
doServer(int fdListening, struct sockaddr_in *addr);

int main(int argc, char **argv)
{

	setHandlers();

	if (argc != 2)
	{
		usage();
		exit(1);
	}

	int port = atoi(argv[1]);

	debug("Port na ktorym server bedzie nasluchiwal: %d\n", port);

	int fd = socket(AF_INET, SOCK_STREAM, 0);

	if (fd < 0)
	{
		perror("socket: ");
		abort();
	}

	struct sockaddr_in addr;

	if (bindSocket(fd, port, &addr) < 0)
	{
		perror("bindSocket: ");
		if (errno == EADDRINUSE || errno == EACCES || errno == EINVAL)
			exit(1);
		abort();
	}

	if (listen(fd, BACKLOG) < 0)
	{
		perror("listen");
		if (errno == EADDRINUSE)
			exit(1);
		abort();
	}

	if (doServer(fd, &addr) < 0)
	{
		perror("Sth wrong: ");
		exit(3);
	}

	if (close(fd) < 0)
	{
		perror("close");
	}

	printf("Allocated memory: %d\n", memoryCounter);

	return 0;
}

void addNewClient(serverState *state2, int i, int newFd)
{
	debug("Nowy klient ma idx: %d\n", i);

	state2->clients[i].fd = newFd;
	state2->clients[i].id = -1;
	state2->clients[i].rank = LOGGED_OUT;
	state2->clients[i].state = READY;
	// zajmujemy dany slot
	// i czekamy aż coś wypisze
}

void reallocateclients(serverState *state2, int clientsBlocks, client* clients)
{
	clients = (client *) realloc(clients,
			sizeof(client) * CLIENT_BLOCK_SIZE * state2->clientsBlocks);

	if (clients == NULL )
	{
		perror("Bad allocation");
		abort();
	}

	memset(&(state2->clients[CLIENT_BLOCK_SIZE * (state2->clientsBlocks - 1)]), 0,
			sizeof(client) * CLIENT_BLOCK_SIZE);

	// TODO state2->longContent

	state2->longContent = (char **) realloc(state2->longContent,
			sizeof(char *) * CLIENT_BLOCK_SIZE * state2->clientsBlocks);

	if (state2->longContent == NULL )
	{
		perror("Bad allocation");
		abort();
	}

	int i;

	for (i = (state2->clientsBlocks - 1) * CLIENT_BLOCK_SIZE;
			i < state2->clientsBlocks * CLIENT_BLOCK_SIZE; i++)
	{
		state2->longContent[i] = (char *) malloc(
				sizeof(char) * LONG_INPUT_BUFFER_LENGTH);
		if (state2->longContent[i] == NULL )
		{
			perror("Memory allocation failed: ");
			abort();
		}
	}

}

int insertNewFd(serverState *state2, int newFd)
{
	// szukamy wolnego miejsca dla tegoż deskryptora

	if (state2->clientsCount >= state2->clientsBlocks * CLIENT_BLOCK_SIZE)
	{
		debug("Allocating new memory");
		++state2->clientsBlocks;
		reallocCommentsIfNeeded(state2);
	}

	++state2->clientsCount;

	debug("Client nr. %d\n", state2->clientsCount);

	for (int i = 1; i <= state2->clientsBlocks * state2->clientsCount; i++)
	{
		if (state2->clients[i].fd < 0)
		{
			addNewClient(state2,i, newFd);
			return i;
		}
	}
	return -1;
}

socklen_t acceptHandle(serverState *state2, int fdListening, client *clients,
		struct sockaddr_in* addr)
{
	// accepting work
	socklen_t len = sizeof(struct sockaddr_in);
	int newFd = -1;
	if ((newFd = TEMP_FAILURE_RETRY(
			accept(fdListening, (struct sockaddr*) addr, &len))) < 0)
	{
		// tu nie powinno być blokujące
		perror("accept: ");
		abort();
	}debug("New socket: %d\n", newFd);
	// szukamy wolnego miejsca dla tegoż deskryptora
	if (insertNewFd(state2, newFd) < 0)
	{
		fprintf(stderr, "Insertion failed");
		TEMP_FAILURE_RETRY(close(newFd));
	}
	return len;
}

void prepareEnvironment(serverState *state2, int fdListening)
{

	memset(&threadsToJoin, 0, sizeof(threadsToJoin));

	state2->clients = (client *) malloc(
			sizeof(client) * state2->clientsBlocks * CLIENT_BLOCK_SIZE);
	memset(state2->clients, 0, sizeof(client) * CLIENT_BLOCK_SIZE * state2->clientsBlocks);
	state2->users = (user *) malloc(sizeof(user) * state2->usersBlocks * USER_BLOCK_SIZE);
	memset(state2->users, 0, sizeof(user) * USER_BLOCK_SIZE * state2->usersBlocks);
	state2->comments = (comment *) malloc(
			sizeof(comment) * state2->commentsBlocks * COMMENT_BLOCK_SIZE);
	state2->bugs = (bug *) malloc(sizeof(bug) * state2->bugsBlocks * BUG_BLOCK_SIZE);
	state2->longContent = (char **) malloc(
			sizeof(comment *) * state2->clientsBlocks * CLIENT_BLOCK_SIZE);

	if (state2->clients == NULL || state2->users == NULL || state2->comments == NULL
			|| state2->longContent == NULL )
	{
		perror("allocation memory failed");
		exit(3);
	}

	for (int i = 0; i < state2->clientsBlocks * CLIENT_BLOCK_SIZE; i++)
	{
		state2->longContent[i] = (char *) malloc(
				sizeof(char) * LONG_INPUT_BUFFER_LENGTH);
		if (state2->longContent == NULL )
		{
			perror("failed memory allocation");
			exit(3);
		}
	}

	if (pthread_mutex_init(&mutex, NULL ) < 0)
	{
		perror("Error by initializing global mutex: ");
		abort();
	}

	memset(state2->bugs, 0, sizeof(bug) * (state2->bugsBlocks * BUG_BLOCK_SIZE));
	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
		state2->bugs[i].id = -1;
	memset(state2->clients, 0, sizeof(client) * state2->clientsBlocks * CLIENT_BLOCK_SIZE);
	memset(state2->users, 0, sizeof(user) * state2->usersBlocks * USER_BLOCK_SIZE);
	memset(state2->comments, 0, sizeof(comment) * state2->commentsBlocks * COMMENT_BLOCK_SIZE);

	for (int i = 0; i < state2->clientsBlocks * CLIENT_BLOCK_SIZE; i++)
	{
		state2->clients[i].fd = -1;
		state2->clients[i].id = -1;
		state2->clients[i].state = LOGGED_OUT;
		memset(state2->longContent[i], 0, sizeof(char) * LONG_INPUT_BUFFER_LENGTH);
	}

	for (int i = 0; i < state2->clientsBlocks * CLIENT_BLOCK_SIZE; i++)
	{
		state2->comments[i].idBug = -1;
	}

}

void closeFds(serverState *state2)
{
	for (int i = 1; i < state2->clientsBlocks * CLIENT_BLOCK_SIZE; i++)
	{
		if (state2->clients[i].fd > 2)
			TEMP_FAILURE_RETRY(close(state2->clients[i].fd));
	}
}

void deallocateMemory(serverState *state2)
{
	free(state2->clients);
	free(state2->users);
	free(state2->comments);
	free(state2->bugs);
	for (int i = 0; i < state2->clientsBlocks * CLIENT_BLOCK_SIZE; i++)
	{
		free(state2->longContent[i]);
	}
	free(state2->longContent);
}

void closeAndDestroyMessageQueues(mqd_t pMsgQueue, char name[])
{
	if (TEMP_FAILURE_RETRY(mq_close(pMsgQueue)) < 0)
	{
		perror("mq_close");
	}
	if (TEMP_FAILURE_RETRY(mq_unlink(name)) < 0)
	{
		perror("mq_unlink");
	}
}

void spawnNewThreads(serverState *state2, int newFd, int idx, pthread_attr_t* attr)
{
	struct thread_info *info1 = (struct thread_info*) malloc(
			sizeof(struct thread_info));
	comListParam *param1 = (comListParam*) malloc(sizeof(comListParam));

	memset(info1, 0, sizeof(struct thread_info));
	memset(param1, 0, sizeof(comListParam));

	debug("new Fd: %d\n", newFd);
	param1->fd = newFd;
	param1->idx = idx;
	param1->serverState = state2;
	info1->arg = param1;

	if (pthread_create(&(info1->thread_id), attr, commandHandler, info1) < 0)
	{
		perror("Failed to create thread: ");
		abort();
	}

	state2->clients[idx].listener = info1->thread_id;

	struct thread_info* info2 = (struct thread_info*) malloc(
			sizeof(struct thread_info));
	comListParam *param2 = (comListParam*) malloc(sizeof(comListParam));

	memset(info2, 0, sizeof(struct thread_info));
	memset(param2, 0, sizeof(comListParam));
	debug("new Fd: %d\n", newFd);
	param2->fd = newFd;
	param2->idx = idx;
	info2->arg = param2;
	param2->serverState = state2;

	if (pthread_create(&(info2->thread_id), attr, outputHandler, info2) < 0)
	{
		perror("Failed to create thread: ");
		abort();
	}

	state2->clients[idx].sender = info2->thread_id;

}

void joinDeadThreads()
{

	struct thread_info *temp = threadsToJoin.next;
	struct thread_info *old = NULL;

	while (temp != NULL )
	{
		printf ("Joining dead thread: %d\n", (int)temp->thread_id);
		if ((errno = pthread_join(temp->thread_id, NULL )) < 0)
		{
			perror("pthread_join");
			abort();
		}

		old = temp;
		temp = temp->next;

		if (old != &threadsToJoin)
			free(old);
	}

	threadsToJoin.next = NULL;

}

int doAccepterWork(serverState *state2, pthread_mutex_t* mutex, int idx, int newFd,
		pthread_attr_t* attr)
{
	if (pthread_mutex_lock(&*mutex) < 0)
	{
		perror("Error locking");
		abort();
	}
	if ((idx = insertNewFd(state2, newFd)) < 0)
	{
		fprintf(stderr, "Insertion failed");
		TEMP_FAILURE_RETRY(close(newFd));
	}
	// tworzymy wątki
	//spawnNewThreads(newFd, idx, &*attr);
	spawnNewThreads(state2,newFd, idx, attr);
	joinDeadThreads();
	if (pthread_mutex_unlock(&*mutex) < 0)
	{
		perror("Error locking");
		abort();
	}
	return idx;
}

void closeSafelyThreads(serverState *state2)
{
	for (int i = 0; i < state2->clientsBlocks * CLIENT_BLOCK_SIZE; i++)
	{
		if (state2->clients[i].fd > 0)
		{
			debug("Wanting to close up client idx: %d\n", i);

			if ((errno = TEMP_FAILURE_RETRY(pthread_cancel(state2->clients[i].listener)))
					!= 0)
			{
				perror("pthread_cancel");
			}

			if ((errno = TEMP_FAILURE_RETRY(pthread_cancel(state2->clients[i].sender)))
					!= 0)
			{
				perror("pthread_cancel");
			}

			debug("Listener CLOSING: %u\n", (unsigned int)state2->clients[i].listener);

			void *ret = NULL;

			if ((errno = TEMP_FAILURE_RETRY(
					pthread_join(state2->clients[i].listener, &ret))) != 0)
			{
				perror("pthread_join");
				abort();
			}

			debug("Sender CLOSING: %u\n", (unsigned int)state2->clients[i].sender);

			// FIXME join does not waits for finish

			if ((errno = TEMP_FAILURE_RETRY(
					pthread_join(state2->clients[i].sender, &ret))) != 0)
			{
				perror("pthread_join");
				abort();
			}

		}
	}
}

void closeSafelyResources(pthread_attr_t* attr, pthread_mutex_t* mutex)
{
	if ((errno = pthread_attr_destroy(attr)) != 0)
	{
		perror("pthread_attr_destroy");
		abort();
	}
	if ((errno = pthread_mutex_lock(mutex)) != 0)
	{
		perror("pthread_mutex_lock");
		abort();
	}
	joinDeadThreads();
	if ((errno = pthread_mutex_unlock(mutex)) != 0)
	{
		perror("pthread_mutex_lock");
		abort();
	}
}

mqd_t openQueue(char name[])
{
	mqd_t pMsgQueue = TEMP_FAILURE_RETRY(
			mq_open(name, O_RDWR | O_CREAT, 0755, NULL ));
	if (pMsgQueue < 0)
	{
		perror("mq_open");
		abort();
	}
	return pMsgQueue;
}

void destroyMutex(pthread_mutex_t* mutex)
{
	if (pthread_mutex_destroy(&*mutex) < 0)
	{
		perror("Error by destroying mutex");
	}
}

int doServer(int fdListening, struct sockaddr_in *addr)
{
	serverState *state = (serverState *)malloc(sizeof(serverState));

	if (state == NULL)
	{
		perror("malloc allocation failed");
		abort();
	}

	memset(state, 0, sizeof(serverState));

	state->bugCounter = 0;
	state->bugs = NULL;
	state->bugsBlocks = 1;
	state->bugsCount = 0;
	state->clientPrefix = "cli_";
	state->supportPrefix = "sup_";
	state->commentsCounter = 0;
	state->comments = NULL;
	state->commentsBlocks = 1;
	state->commentsCount = 0;
	state->clients = NULL;
	state->clientsBlocks = 1;
	state->clientsCount = 0;
	state->users = NULL;
	state->userCounter = 0;
	state->usersBlocks = 1;
	state->usersCount = 0;
	state->longContent = NULL;
	state->working = 1;


	prepareEnvironment(state,fdListening);

	socklen_t len = sizeof(struct sockaddr_in);
	int newFd = -1;

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	//pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	char name[] = "/zdarowie";


	mqd_t pMsgQueue = openQueue(name);

	while (working)
	{
		if ((newFd = accept(fdListening, (struct sockaddr*) addr, &len)) < 0)
		{
			// tu nie powinno być blokujące
			if (errno == EINTR)
				break;
			perror("accept: ");
			abort();
		}

		debug("New socket: %d\n", newFd);
		// szukamy wolnego miejsca dla tegoż deskryptora
		int idx = -1;

		idx = doAccepterWork(state, &mutex, idx, newFd, &attr);
	}

	closeSafelyThreads(state);

	debug("Closed threads\n");


	closeFds(state);
	deallocateMemory(state);
	closeSafelyResources(&attr, &mutex);
	//sleep(5);

	closeAndDestroyMessageQueues(pMsgQueue, name);
	free(state);

	destroyMutex(&mutex);

	return 0;
}

void setHandlers()
{
	struct sigaction action;
	sigset_t mask;
	memset(&action, 0, sizeof(action));
	memset(&mask, 0, sizeof(mask));
	sigemptyset(&mask);
	action.sa_handler = sigHandler;
	action.sa_mask = mask;

	if (sigaction(SIGTERM, &action, NULL ) < 0)
	{
		perror("sigaction");
		exit(3);
	}

	if (sigaction(SIGINT, &action, NULL ) < 0)
	{
		perror("sigaction");
		exit(3);
	}

	if (sigaction(SIGPIPE, &action, NULL ) < 0)
	{
		perror("sigaction");
		exit(3);
	}

	if (sigaction(SIGABRT, &action, NULL ) < 0)
	{
		perror("sigaction");
		exit(3);
	}

}
