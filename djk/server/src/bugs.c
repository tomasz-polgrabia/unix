#include <stdio.h>
#include <bugs.h>

void handleLongContent(char *buffer, int i, char *longContent, client *clients)
{
	debug("Long content: %s\n", buffer);
	strncat(longContent, buffer, 0x4000);
	longContent[LONG_INPUT_BUFFER_LENGTH - 1] = '\0';
	char *finish = strstr(longContent, "EOFEOFEOF");
	if (finish != NULL )
	{
		*finish = '\0';
		debug("Koniec dlugiego opisu\n");
		strncpy(clients[i].tempBug.longDescription, longContent,
				MAX_BUG_LONG_DESCRIPTION_LENGTH);

		debug("Bug reported:\n");debug("Kind: %s\n", clients[i].tempBug.kind);debug("Tool: %s\n", clients[i].tempBug.tool);debug("Description: %s\n", clients[i].tempBug.shortDescription);debug("Long description: '%s'\n", clients[i].tempBug.longDescription);debug("End of bug\n");

		clients[i].state = BUG_ACCEPT;
		sprintf(buffer, "BUGACCEPT (YES or NO):\n");
	}
}

int listExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	int support =
			(c->rank == LOGGED_IN
					&& strncmp(c->u.login, state2->supportPrefix,
							min(strlen(c->u.login), strlen(state2->supportPrefix)))
							== 0) ? 1 : 0;

	debug("Support: %d\n", support);debug("Client: %d\n", c->id);

	memset(output, 0, INPUT_BUFFER_LENGTH);
	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
	{
		if (state2->bugs[i].idSupport < 1)
			continue;

		if (support)
		{
			if (state2->bugs[i].idSupport != c->id)
				continue;
		}
		else
		{
			debug("Author of %d: %d\n", i, state2->bugs[i].idAuthor);
			if (state2->bugs[i].idAuthor != -1) // publiczne może każdy patrzeć
			{
				if (state2->bugs[i].idAuthor != c->id)
					continue;
			}
		}

		sprintf(output + strlen(output), "%d,", state2->bugs[i].id);
	}

	sprintf(output + strlen(output), "\n");
	return 0;
}
int bugExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	if (c->state != READY)
	{
		sprintf(output, "Not supposed to be used here\n");
		return 0;
	}

	if (c->rank != LOGGED_IN)
	{
		sprintf(output, "You are not logged\n");
		return 0;
	}

	sprintf(output, "Bug OK, give now kind of bug\n");
	c->state = BUG_KIND;
	return 0;
}

int kindExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	if (c->state == BUG_KIND)
	{
		sprintf(output, "Bug kind OK, give now tool description\n");
		char *trimmedKind = trim(params);
		strncpy(c->tempBug.kind, trimmedKind, MAX_BUG_KIND_LENGTH);
		c->state = BUG_TOOL;
	}
	else
	{
		sprintf(output, "Not supposed to be used here\n");
	}
	return 0;
}

int toolExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	if (c->state == BUG_TOOL)
	{
		sprintf(output, "Bug Tool OK, give now short description\n");
		char *trimmedTool = trim(params);
		strncpy(c->tempBug.tool, trimmedTool, MAX_BUG_TOOL_LENGTH);
		c->state = BUG_SHORT_DESCRIPTION;
	}
	else
	{
		sprintf(output, "Not supposed to be used here\n");
	}
	return 0;
}

int shortDescExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	if (c->state == BUG_SHORT_DESCRIPTION)
	{
		sprintf(output,
				"BUG completed OK, print EOFEOFEOF to end long description, and after that BUGACCEPT YES to accept\n");
		char *trimmedDescription = trim(params);
		strncpy(c->tempBug.shortDescription, trimmedDescription,
				MAX_BUG_SHORT_DESCRIPTION_LENGTH);
		c->state = BUG_LONG_DESCRIPTION;
	}
	else
	{
		sprintf(output, "Not supposed to be used here\n");
	}
	return 0;
}

int acceptExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	assert(c != NULL && state2->users != NULL && params != NULL && output != NULL);

	int idBug = atoi(trim(params));

	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
	{
		if (state2->bugs[i].id == idBug)
		{
			if (state2->bugs[i].idAuthor == c->id)
			{

				for (int j = 0; j < state2->usersBlocks * USER_BLOCK_SIZE; j++)
				{
					if (state2->users[i].id == state2->bugs[i].idSupport)
					{
						state2->users[i].handlingBugs--;
						break;
					}
				}

				sprintf(output, "Bug accepted and is supposed to be removed\n");
				memset(&state2->bugs[i], 0, sizeof(bug));
				state2->bugs[i].id = -1;
				state2->bugs[i].idAuthor = -1;
				state2->bugs[i].idSupport = -1;

				--state2->bugsCount;

				for (int j = 0; j < state2->commentsBlocks * COMMENT_BLOCK_SIZE; j++)
				{
					if (state2->comments[j].idBug == idBug)
					{
						--state2->commentsCount;
						memset(&state2->comments[j], 0, sizeof(comment));
						state2->comments[j].idAuthor = -1;
						state2->comments[j].idBug = -1;
					}
				}
				bugsCleanUp(&state2->bugs, state2->bugsCount,&(state2->bugsBlocks));
				return 0;
			}
			else
			{
				sprintf(output,
						"You are not the author of this bug, aborting\n");
				return 0;
			}
		}
	}

	sprintf(output, "No such bug\n");

	return 0;
}

int viewExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{

	int idBug = atoi(trim(params));
	int idx = -1;

	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
	{
		if (state2->bugs[i].id == idBug)
		{
			if (c->id != state2->bugs[i].idAuthor && c->id != state2->bugs[i].idSupport)
			{
				sprintf(output, "Access denied\n");
				return 0;
			}
			idx = i;
		}
	}

	if (idx < 0)
	{
		sprintf(output, "No such bug\n");
		return 0;
	}

	sprintf(output,
			"Bug id: %d\nAuthor: %d\nSupport: %d\nKind: %s\nTool: %s\nDescription: %s\n, Content: %s\n",
			state2->bugs[idx].id, state2->bugs[idx].idAuthor, state2->bugs[idx].idSupport,
			state2->bugs[idx].kind, state2->bugs[idx].tool, state2->bugs[idx].shortDescription,
			state2->bugs[idx].longDescription);

	return 0;
}

int bugAcceptExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	debug("BUG ACCEPT\n");
	if (c->state == BUG_ACCEPT)
	{
		if (strcmp(trim(params), "YES") == 0)
		{
			sprintf(output,
					"Bug will be sent to the tester.\n Thank for your attribution\n");

			/**
			 * FIXME
			 */

			c->state = READY;
			return REQUEST_RES_BUG_ACCEPT;
		}
		else
		{
			sprintf(output, "Bug discarded\n");
			c->state = READY;
			return REQUEST_RES_BUG_DEFERRED;
		}
	}
	else
	{
		sprintf(output, "Not expected to be used here\n");
	}
	return 0;
}

void bugsCleanUp(bug **bugs, int bugsCount, int *bugBlocks)
{
	bug* bugsPtr = *bugs;

	int n = BUG_BLOCK_SIZE * (*bugBlocks);

	int needed = (bugsCount - 1) / BUG_BLOCK_SIZE + 1;

	if (needed >= n)
		return;

	debug("Cleaning up bugs\n");

	int freeIdx = 0;
	int busyIdx = n-1;

	while (freeIdx < busyIdx)
	{
		while (bugsPtr[freeIdx].id >= 1 && freeIdx < n)
			++freeIdx;

		while (bugsPtr[busyIdx].id < 1 && busyIdx >= 0)
			--busyIdx;

		if (freeIdx < busyIdx)
		{
			debug("Exchanging free %d with busy: %d\n", freeIdx, busyIdx);
			bugsPtr[freeIdx] = bugsPtr[busyIdx];
			memset (&bugsPtr[busyIdx], 0, sizeof(bug));
			++freeIdx;
			--busyIdx;
		}

	}

	*bugs = realloc(bugsPtr, sizeof(bug)*BUG_BLOCK_SIZE*needed);
	if (*bugs == NULL)
	{
		perror("realloc");
		abort();
	}

	*bugBlocks = needed;

}

