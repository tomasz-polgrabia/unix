#include <comments.h>

int commentsExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	// FIXME samo state.comments przy żadnych parametrach wysypuje
	if (c->state != READY)
	{
		sprintf(output, "Not supposed to be used here\n");
		return 0;
	}

	int idBug = atoi(trim(params));

	// sprawdzamy czy ma dostęp do buga

	int idx = -1;

	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
	{
		if (state2->bugs[i].idSupport < 1)
			continue;

		if (state2->bugs[i].id == idBug)
		{
			idx = i;
			break;
		}

	}

	if (idx < 0)
	{
		sprintf(output, "No such bug\n");
		return 0;
	}

	if (state2->bugs[idx].idAuthor != c->id && state2->bugs[idx].idSupport != c->id)
	{
		sprintf(output, "No access, sorry\n");
		return 0;
	}

	// mamy dostęp, jedziemy z koksem

	sprintf(output, "Comments: ");

	for (int i = 0; i < state2->commentsBlocks * COMMENT_BLOCK_SIZE; i++)
	{
		if (state2->comments[i].idBug == idBug)
		{
			sprintf(output + strlen(output), "%d, ", state2->comments[i].id);
		}
	}

	sprintf(output + strlen(output), "\n");

	return 0;
}

int statusExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{

	output[0] = '\0';
	char *statusLogin = NULL;
	if (c->rank == LOGGED_OUT)
		statusLogin = "Logged out";
	else
		statusLogin = "Logged in";

	if (strncasecmp(c->u.login, state2->supportPrefix, strlen(state2->supportPrefix)) == 0)
	{

		sprintf(output, "Your status: %s\nuser id: %d, login: %s\n", statusLogin,
				c->id, c->u.login);
		sprintf(output + strlen(output),
				"state.bugs: %d\nstate.bugsBlocks: %d\nBug block size: %d\n", state2->bugsCount,
				state2->bugsBlocks, BUG_BLOCK_SIZE);
		sprintf(output + strlen(output),
				"Comments: %d\nstate.commentsBlocks: %d\nComment block size: %d\n",
				state2->commentsCount, state2->commentsBlocks, COMMENT_BLOCK_SIZE);
		sprintf(output + strlen(output),
				"Users: %d\nUsers Blocks: %d\nUsers block size: %d\n",
				state2->usersCount, state2->usersBlocks, USER_BLOCK_SIZE);

		sprintf(output + strlen(output),
				"Clients: %d\nClients Blocks: %d\nClients block size: %d\n",
				state2->clientsCount, state2->clientsBlocks, USER_BLOCK_SIZE);
	}
	return 0;
}

int showCommentExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{
	int idComment = atoi(trim(params));

	int idx = -1;

	for (int i = 0; i < state2->commentsBlocks * COMMENT_BLOCK_SIZE; i++)
	{
		if (state2->comments[i].id == idComment)
		{
			idx = i;
			break;
		}
	}

	if (idx < 0)
	{
		sprintf(output, "No such comment\n");
		return 0;
	}

	debug("I have found comment on place(idx): %d\n, Comment id: %d\n", idx,
			idComment);

	int idxBug = -1;

	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
	{
		if (state2->bugs[i].id == state2->comments[idx].idBug)
		{
			idxBug = i;
			break;
		}
	}

	if (idxBug < 0)
	{
		sprintf(output,
				"Database is corrupt, comment is linked to the non-existing bug\n");
		return 0;
	}

	debug("Bug idx: %d\n", idxBug);

	debug("Id author: %d\nId support: %d\n", state2->bugs[idxBug].idAuthor,
			state2->bugs[idxBug].idSupport);

	if (state2->bugs[idxBug].idAuthor != c->id && state2->bugs[idxBug].idSupport != c->id)
	{
		sprintf(output, "Accessed denied\n");
		return 0;
	}

	sprintf(output,
			"Comment id: %d, Bug: %d\nAuthor: %d\nTimestamp: %d\nContent: '%s'\n",
			state2->comments[idx].id, state2->comments[idx].idBug, state2->comments[idx].idAuthor,
			state2->comments[idx].timestamp, state2->comments[idx].buffer);
	return 0;
}

int commentExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param)
{

	if (c->state != READY)
	{
		sprintf(output, "Not supposed to be here\n");
		return 0;
	}

	int idBug = atoi(trim(params));

	int idx = -1;

	for (int i = 0; i < state2->bugsBlocks * BUG_BLOCK_SIZE; i++)
	{
		if (state2->bugs[i].id == idBug)
		{
			idx = i;
			break;
		}
	}

	if (idx < 0)
	{
		sprintf(output, "No such bug\n");
		return 0;
	}

	if (state2->bugs[idx].idAuthor != c->id && state2->bugs[idx].idSupport != c->id)
	{
		sprintf(output, "No access, aborted\n");
		return 0;
	}

	c->commentedIdBug = idBug;
	c->state = COMMENT_INPUT;

	sprintf(output, "Comment: ");
	return 0;
}

void reallocCommentsIfNeeded(serverState* state2)
{
	if (state2->commentsCount >= state2->commentsBlocks * COMMENT_BLOCK_SIZE)
	{
		++state2->commentsBlocks;
		state2->comments = (comment *) realloc(state2->comments,
				sizeof(comment) * state2->commentsBlocks * COMMENT_BLOCK_SIZE);
		if (state2->comments == NULL )
		{
			perror("realloc");
			abort();
		}

		memset(&(state2->comments[(state2->commentsBlocks - 1) * COMMENT_BLOCK_SIZE]), 0,
				sizeof(comment) * COMMENT_BLOCK_SIZE);

	}
}

int findIdOffreeCommentSlot(serverState *state2, int idx)
{
	for (int i = 0; i < state2->commentsBlocks * COMMENT_BLOCK_SIZE; i++)
	{
		if (state2->comments[i].idAuthor < 1)
		{
			idx = i;
			break;
		}
	}
	return idx;
}

int handleCommentInput(char *buffer, char *output, char *commentInput,
		client *c, serverState *state2, comListParam *param)
{
	char message[INPUT_BUFFER_LENGTH];
	debug("%s\n", buffer);
	strcat(commentInput, buffer);

	char *finish = strstr(commentInput, "EOFEOFEOF");
	if (finish != NULL )
	{
		*finish = '\0';
		// TODO dodaj faktycznie

		int idx = -1;

		reallocCommentsIfNeeded(state2);

		idx = findIdOffreeCommentSlot(state2, idx);

		++state2->commentsCount;

		c->state = READY; // koniec komentowania

		if (idx < 0)
		{
			sprintf(output, "Sorry no memory\n");
			return REQUEST_RES_NORMAL;
		}

		state2->comments[idx].id = state2->commentsCounter++;
		state2->comments[idx].idAuthor = c->id;
		state2->comments[idx].idBug = c->commentedIdBug; // FIXME
		state2->comments[idx].timestamp = time(0);
		strncpy(state2->comments[idx].buffer, commentInput, COMMENT_MAX_LENGTH);

		sprintf(message,
				"Inserted new comment\nId: %d\nAuthor: %d\nBug: %d\nTimestamp: %d\n,Contents: %s\n",
				state2->comments[idx].id, state2->comments[idx].idAuthor, state2->comments[idx].idBug,
				state2->comments[idx].timestamp, state2->comments[idx].buffer);

		if (mq_send(param->msgQueue, message, strlen(message), 0) < 0)
		{
			debug("msgsnd(%u,\"%s\",%d,%d)",param->msgQueue, message, strlen(message), 0);
			perror("failed");
			abort();
		}

		debug(
				"Inserted new comment\nComment id: %d\nIdx: %d\nAuthor: %d\nBug: %d\nTimestamp: %d\nContent: '%s'\n",
				state2->comments[idx].id, idx, state2->comments[idx].idAuthor,
				state2->comments[idx].idBug, state2->comments[idx].timestamp,
				state2->comments[idx].buffer);

		c->commentedIdBug = -1;

	}

	return REQUEST_RES_NORMAL;
}
