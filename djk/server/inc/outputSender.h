#ifndef __OUTPUT_SENDER__
#define __OUTPUT_SENDER__

#include <stdio.h>

#include <server.h>
#include <sendMessage.h>

#include <pthread.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>


void *
outputHandler(void *arg);

int handleSendingBug(serverState *state2, client *c, client *clients, char *output, user *users,
		int *idSuphport, comListParam *param, mqd_t queue);

#endif
