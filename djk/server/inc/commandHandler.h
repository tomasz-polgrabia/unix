#ifndef __COMMAND_HANDLER__
#define __COMMAND_HANDLER__

#include <stdio.h>
#include <unistd.h>

#include <server.h>

#include <outputSender.h>

#include <stringHelpers.h>

#include <comListenerParam.h>

#include <bugs.h>


#define INPUT_BUFFER_LENGTH 0x2000
#define LONG_INPUT_BUFFER_LENGTH 0x4000

void *
commandHandler(void *arg);

int
handleInput(char *buffer, int i, client *clients, user *users,
    char *longContent);

void handleShutdown(serverState *state2, int i);


int handleInteractiveMode(int res, char *buffer, int idx, client *clients,
		serverState *state2, char *longContent, comListParam *param, mqd_t queue);

int
executeRequest(char *str, client *c, user *users, user *loggedUsers, comListParam *param,mqd_t queue);

int
helpExecutor(client *c, serverState *state, char *params, char *output, comListParam *param);

int abortExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param);
int quitExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param);

extern char supportPrefix[];
extern char clientPrefix[];

#endif
