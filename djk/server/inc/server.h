/*
 * server.h
 *
 *  Created on: 04-05-2013
 *      Author: duga
 */

#ifndef SERVER_H_
#define SERVER_H_

#define MAX_INT 1000000

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sockets.h>
#include <unistd.h>
#include <poll.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>

#include <user.h>
#include <client.h>
#include <jTable.h>
#include <bug.h>
#include <comment.h>
#include <time.h>

#include <thread_info.h>

#include <sys/msg.h>

#include <mqueue.h>

#include <userServ.h>
#include <bugs.h>
#include <comments.h>

#include <stdarg.h>
#include <assert.h>

#include <serverState.h>

extern int userCounter;

#define CLIENT_BLOCK_SIZE 5
#define BUG_BLOCK_SIZE 2
#define COMMENT_BLOCK_SIZE 5
#define USER_BLOCK_SIZE 100

#define THREADS_BLOCK 0x1 // TESTING

#define BACKLOG 32

#define REQUEST_RES_USER_ARG_ERROR 2
#define REQUEST_RES_NORMAL 0
#define REQUEST_RES_QUIT 1

#define REQUEST_RES_BUG_ACCEPT 3
#define REQUEST_RES_BUG_DEFERRED 4
#define REQUEST_COMMENT_FINISHED 5

#include <commandHandler.h>
#include <outputSender.h>

#include <pthread.h>

extern serverState state;

void dump();

#define MAX_COMMANDS 19
extern jumpTable jTable[];

#define min(x,y) ((x) < (y)) ? (x) : (y)
#define max(x,y) ((x) > (y)) ? (x) : (y)

extern pthread_mutex_t mutex;

void plog(char *fmt, ...);

void *my_malloc(size_t size);
void my_free(void *ptr);

extern struct thread_info threadsToJoin;

#include <stringHelpers.h>


#endif /* SERVER_H_ */
