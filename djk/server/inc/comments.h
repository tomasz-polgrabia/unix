#ifndef __COMMENTS_H__
#define __COMMENTS_H__

#include <server.h>
#include <stdio.h>

#include <commandHandler.h>
#include <user.h>
#include <client.h>
#include <commandHandler.h>

int commentsExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param);
int statusExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param);
int showCommentExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param);
int commentExecutor(client *c, serverState *state, char *params, char *output,
		comListParam *param);
int handleCommentInput(char *buffer, char *output, char *commentInput, client *c, serverState *state2, comListParam *param);

void reallocCommentsIfNeeded(serverState *state2);

#endif
