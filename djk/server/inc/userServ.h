#ifndef __USER_H2__
#define __USER_H2__


#include <stdio.h>
#include <commandHandler.h>
#include <client.h>

int userExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

int passExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

int logoutExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

#endif
