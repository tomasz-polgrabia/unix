#ifndef __BUGS__
#define __BUGS__

#include <stdio.h>
#include <server.h>

#include <commandHandler.h>

void
handleLongContent(char *buffer, int i, char *longContent, client *clients);

int listExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);
int bugExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

int kindExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

int toolExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

int shortDescExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

int acceptExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

int viewExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

int bugAcceptExecutor(client *c, serverState *state2, char *params, char *output,
		comListParam *param);

void bugsCleanUp(bug **bugs, int bugsCount, int *bugBlocks);

#endif
