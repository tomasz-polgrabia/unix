#ifndef __OUT_SENDER_PARAM__
#define __OUT_SENDER_PARAM__

typedef struct
{
  int fd;
  int msgQueue;
  int idx;
} outSenderParam;

#endif
