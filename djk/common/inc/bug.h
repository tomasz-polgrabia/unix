/*
 * bug.h
 *
 *  Created on: 05-05-2013
 *      Author: duga
 */

#ifndef BUG_H_
#define BUG_H_

#define BUG_KIND_MAX_LENGTH 32
#define BUG_TOOL_MAX_LENGTH 32


#define MAX_BUG_KIND_LENGTH 64
#define MAX_BUG_TOOL_LENGTH 64
#define MAX_BUG_SHORT_DESCRIPTION_LENGTH 128
#define MAX_BUG_LONG_DESCRIPTION_LENGTH 0x4000

typedef struct bug_t
{
  int id;
  int idAuthor; // id user'a, który wysłał
  int idSupport; // id user'a support, który będzie ten bug rozpatrywał
  char kind[MAX_BUG_KIND_LENGTH];
  char tool[MAX_BUG_TOOL_LENGTH];
  char shortDescription[MAX_BUG_SHORT_DESCRIPTION_LENGTH];
  char longDescription[MAX_BUG_LONG_DESCRIPTION_LENGTH];
} bug;

#endif /* BUG_H_ */
