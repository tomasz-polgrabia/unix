#ifndef __THREAD_INFO__
#define __THREAD_INFO__

#include <pthread.h>

struct thread_info
{
  pthread_t thread_id;
  int thread_num;
  void *arg;
  struct thread_info *prev;
  struct thread_info *next;
};

#endif
