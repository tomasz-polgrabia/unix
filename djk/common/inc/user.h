/*
 * user.h
 *
 *  Created on: 04-05-2013
 *      Author: duga
 */

#ifndef USER_H_
#define USER_H_

#define MAX_LOGIN_LENGTH 256
#define MAX_PASSWORD_LENGTH 32

typedef struct user_t
{
  int id; // id usera
  char login[MAX_LOGIN_LENGTH];
  char password[MAX_PASSWORD_LENGTH];
  int loggedIn;
  int handlingBugs;
  int createPassword;
} user;

#endif /* USER_H_ */
