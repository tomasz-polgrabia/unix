/*
 * comment.h
 *
 *  Created on: 05-05-2013
 *      Author: duga
 */

#ifndef COMMENT_H_
#define COMMENT_H_

#define COMMENT_MAX_LENGTH 0x4000

typedef struct comment_t
{
  int id; // id commentu
  int idBug;
  int idAuthor;
  int timestamp; // in seconds
  char buffer[COMMENT_MAX_LENGTH];
} comment;

#endif /* COMMENT_H_ */
