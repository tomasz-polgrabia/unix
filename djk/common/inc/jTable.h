/*
 * jTable.h
 *
 *  Created on: 04-05-2013
 *      Author: duga
 */

#ifndef JTABLE_H_
#define JTABLE_H_

#define MAX_COMMAND_LENGTH 32

#include <client.h>
#include <serverState.h>
#include <comListenerParam.h>

typedef struct jumpTable_t
{
  // FIXME dołóż rozmiary buforów dla params i output
  int
  (*execute)(client *c, serverState *state, char *params, char *output, comListParam *param);
  char command[MAX_COMMAND_LENGTH];
} jumpTable;

#endif /* JTABLE_H_ */
