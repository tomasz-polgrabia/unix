#ifndef __STRING_HELPERS__
#define __STRING_HELPERS__

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>

#include <sys/time.h>
#include <pthread.h>

char *
trim(char *str);

void plog(char *fmt, ...);

#ifdef NDEBUG
	#define debug(...) plog(__VA_ARGS__)
#else
	#define debug(...)
#endif

#endif
