/*
 * serverState.h
 *
 *  Created on: 24-05-2013
 *      Author: duga
 */

#ifndef SERVERSTATE_H_
#define SERVERSTATE_H_

#include <stdio.h>
#include <client.h>
#include <user.h>
#include <bug.h>
#include <comment.h>

#include <signal.h>

typedef struct
{

	// FIXME zrób bez ograniczeń w ilości daj realloc
	char *supportPrefix;
	char *clientPrefix;
	sig_atomic_t working;
	sig_atomic_t commentsCounter;
	sig_atomic_t bugCounter;
	char **longContent;

	client *clients;
	user *users;

	int clientsCount;
	int clientsBlocks;

	int bugsCount;
	int bugsBlocks;

	int commentsCount;
	int commentsBlocks;

	int usersCount;
	int usersBlocks;

	int userCounter;

	bug *bugs;
	comment *comments;

} serverState;

#endif /* SERVERSTATE_H_ */
