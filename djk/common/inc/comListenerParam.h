#ifndef __COM_LISTENER_PARAM__
#define __COM_LISTENER_PARAM__

#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>


typedef struct comListParam_t
{
  int fd;
  mqd_t msgQueue;
  int idx;
  serverState *serverState;
  // typy skądś trzeba uzgadniać
} comListParam;

#endif
