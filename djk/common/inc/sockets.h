/*
 * sockets.h
 *
 *  Created on: 04-05-2013
 *      Author: duga
 */

#ifndef SOCKETS_H_
#define SOCKETS_H_

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>

#include <server.h>

int
bindSocket(int fd, int port, struct sockaddr_in *addr);
int
bulk_recv(int socket, void *buffer, size_t length, int flags);
int
bulk_send(int socket, void *buffer, size_t length, int flags);
void MakeSocketStruct(uint32_t host, int port, struct sockaddr_in* addr);

#endif /* SOCKETS_H_ */
