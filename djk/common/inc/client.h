/*
 * client.h
 *
 *  Created on: 04-05-2013
 *      Author: duga
 */

#ifndef CLIENT_H_
#define CLIENT_H_

#include <pthread.h>
#include <bug.h>

typedef struct client_t
{
  int fd;
  int id;
  //int idx;
  user u;
  int state;
  int rank;
  bug tempBug;
  int commentedIdBug;
  pthread_t listener;
  pthread_t sender;
} client;

#define LOGGED_OUT 0
#define LOGIN_REQUEST 1
#define LOGGED_IN 2

#define BUG_REQUEST 3
#define BUG_KIND 4
#define BUG_TOOL 5
#define BUG_SHORT_DESCRIPTION 6
#define BUG_LONG_DESCRIPTION 7
#define READY 8
#define BUG_ACCEPT 9
#define COMMENT_INPUT 10

#endif /* CLIENT_H_ */
