/*
 * sockets.c
 *
 *  Created on: 04-05-2013
 *      Author: duga
 */

#include <sockets.h>

void MakeSocketStruct(uint32_t host, int port, struct sockaddr_in* addr)
{
	memset(addr, 0, sizeof(struct sockaddr_in));
	addr->sin_family = AF_INET;
	addr->sin_port = htons(port);
	addr->sin_addr.s_addr = host;
}

int bindSocket(int fd, int port, struct sockaddr_in *addr)
{
	MakeSocketStruct(htonl(INADDR_ANY ), port, addr);
	return bind(fd, (struct sockaddr *) addr, sizeof(struct sockaddr_in));
}

int bulk_recv(int socket, void *buffer, size_t length, int flags)
{
	char *buff = (char *) buffer;
	int sum = 0;
	int count = -1;

	do
	{
		debug("Waiting for data\n");
		count = TEMP_FAILURE_RETRY(recv(socket, buff, length, flags));
		if (count < 0)
		{
			perror("recv");
			return count;
		}

		sum += count;
		length -= count;

		debug("I have read %d data - %s\n", count, buff);

	} while (strstr(buff, "\n") == NULL && count > 0);

	return sum;
}

int bulk_send(int socket, void *buffer, size_t length, int flags)
{

	int offset = 0;
	char *buff = (char *) buffer;
	int count = -1;

	do
	{
		count = TEMP_FAILURE_RETRY(send(socket, buff + offset, length, flags));
		if (count <= 0)
		{

			if (offset > 0)
			{
				/**
				 *  EOF koniec danych też tu się łapie
				 *  offset == 0
				 **/
				return offset;
			}
			else
			{
				return count;
			}
		}

		/*
		 * wyliczamy ile mu zostało do wczytania
		 */

		length -= count;
	} while (length > 0);


	return count;
}

