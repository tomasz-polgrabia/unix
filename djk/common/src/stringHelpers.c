#include <stringHelpers.h>

char *
trim(char *str)
{
  int beginning = 0;
  int length = strlen(str);
  for (beginning = 0; beginning < length && str[beginning] < 32; beginning++)
    ;

  if (beginning >= length)
    {
      str[0] = '\0';
      return str;
    }

  int end;
  for (end = length - 1; end > beginning && str[end] < 32; end--)
    ;
  str[end + 1] = '\0';
  return str + beginning;
}

void plog(char *fmt, ...)
{
	struct timeval time;
	gettimeofday(&time, NULL);
	long int nano = time.tv_usec;
	unsigned int tid = pthread_self();
	va_list args;
	fprintf (stderr,"[%lf(%u,%u)]: ", (double)time.tv_sec + (double)nano/1e9, (unsigned int)getpid(), (unsigned int)tid);
	va_start(args,fmt);
	vfprintf(stderr,fmt,args);
	va_end(args);
}
