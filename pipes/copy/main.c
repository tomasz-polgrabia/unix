#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <fcntl.h>

#define BUFFER_SIZE 0x1000
#if BUFFER_SIZE > PIPE_BUF
	#error Za duzy rozmiar bufora na ta platforme
#endif

void SafeExit(int n)
{
	long max = sysconf(_SC_OPEN_MAX);
	for (int i = 0; i <= max; i++)
		close(i);
	exit(n);
}

void usage()
{
	fprintf (stderr, "Poprawne uzycie ./program n k (1<=n<=1000 && 1<=k<=10)\n");
}

int sendMessage(int fd, char *buffer, int length)
{
	int writeCount = TEMP_FAILURE_RETRY(write(fd,buffer,length));
	if (writeCount < 0)
	{
		if (errno == EPIPE)
		{
			perror("EPIPE");
			return 0;
		}
		return -1;
	}

	if (writeCount != length)
		return -1;
	return 1;
}

int pollMessage(int fd, char *buffer, int length)
{
	// FIXME must be non-blocking
	
	int readCount = TEMP_FAILURE_RETRY(read(fd,buffer,length));
	if (readCount < 0)
	{
		if (errno == EAGAIN)
			return 0;
		return -1;
	}

	if (readCount == 0)
		return 2; // EOF

	return 1;
}

void sigHandler(int nr)
{
	switch (nr)
	{
		case SIGINT:
			SafeExit(0);

			break;
		case SIGCHLD:
		for (;;)
		{
			pid_t child = TEMP_FAILURE_RETRY(waitpid(0, NULL, WNOHANG));
			if (child < 0)
			{
				if (errno == ECHILD)
					break;
				perror("waitpid");
				SafeExit(2);
			}

			if (child <= 0)
				break;

			fprintf (stderr, "Handled child %d\n", child);
		}

			break;
	}
}

int setHandlers()
{
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = sigHandler;
	if (sigaction(SIGINT, &action, NULL) < 0)
		return -1;
	if (sigaction(SIGCHLD, &action, NULL) < 0)
		return -1;

	return 0;

}

int childWork(int filedes[2], int n, int nr)
{
	filedes = filedes;
	n = n;
	nr = nr;
	printf ("Child: %d started\n", nr);

	char buffer[BUFFER_SIZE];


	if (nr == n)
	{
		snprintf (buffer, BUFFER_SIZE, "%d", n);
		int res = sendMessage(filedes[1], buffer, BUFFER_SIZE) < 0;
		if (res < 0)
		{
			perror("sendMessage");
			SafeExit(0);
		}
		/*		
		if (res == 0)
		{
			printf ("Broken pipe\n");
			SafeExit(0);
		}
		*/
	}

	while(1)
	{
		printf ("Child %d polling message\n", nr);
		int res = pollMessage(filedes[0], buffer, BUFFER_SIZE);
		if (res < 0)
		{
			perror("pollMessage");
			SafeExit(2);
		}

		int x = atoi(buffer);
		printf ("Program %d. otrzymal %d\n", nr, x);
		if (x <= 0)
		{
			if (kill (0,SIGINT) < 0)
			{
				perror("kill");
				SafeExit(2);
			}

			SafeExit(0);
		}
		else
		{
			snprintf (buffer, BUFFER_SIZE, "%d", x-1);
			res = sendMessage(filedes[1], buffer, BUFFER_SIZE);
			if (res < 0 )
			{
				perror("sendMessage");
				SafeExit(2);
			}
		}
	}

	return 0;
}

int SpawnChilds(int n, int k)
{
	int filedes[2][k];
	for (int i = 0; i < k; i++)
	{
		int res = pipe(filedes[i]);
		if (res < 0)
		{
			perror("pipe");
			SafeExit(1);
		}
	}

	int param[2];


	for (int i = 0; i < k; i++)
	{
		if (i < k-1)
		{
			param[0] = filedes[i][0];
			param[1] = filedes[i+1][1];
		}
		else
		{
			param[0] = filedes[k-1][0];
			param[1] = filedes[0][1];
		}

		pid_t child = fork();
		if (child < 0)
		{
			perror("fork");
			SafeExit(2);
		}

		if (child == 0)
			SafeExit(childWork(param, n, i+1));

	}

	return 0;
}

int main (int argc, char **argv)
{
	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGPIPE);
	if (sigprocmask(SIG_BLOCK, &set, NULL) < 0)
	{
		perror("sigprocmask");
		SafeExit(0);
	}
	if (setHandlers() < 0)
	{
		perror("setHandlers");
		SafeExit(2);
	}

	if (argc != 3)
	{
		usage();
		SafeExit(1);
	}

	int n = atoi(argv[1]);
	int k = atoi(argv[2]);

	if (1 > n || n > 1000 || 1 > k || k > 10)
	{
		usage();
		SafeExit(1);
	}

	if (SpawnChilds(n,k) < 0)
	{
		perror("SpawnChilds");
		SafeExit(2);
	}

	sigemptyset(&set);
	sigaddset(&set,SIGINT);
	if (sigprocmask(SIG_BLOCK,&set, NULL) < 0)
	{
		perror("sigprocmask");
		SafeExit(0);
	}

	//sigprocmask

	for (;;)
	{
		pid_t child = TEMP_FAILURE_RETRY(waitpid(0, NULL, 0));
		if (child < 0)
		{
			if (errno == ECHILD)
				break;
			perror("waitpid");
			SafeExit(2);
		}

		if (child <= 0)
		{
			printf ("To nie powinno byc\n");
			SafeExit(2);
		}

		fprintf (stderr, "Handled child %d\n", child);
	}

	fprintf (stderr, "Closing app\n");

	return 0;
}
