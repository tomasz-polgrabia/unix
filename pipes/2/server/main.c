#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include "common.h"

#define MAX_CLIENTS 64

char *doubleSpaces(char *str)
{
	int length = strlen(str);
	char *rStr = (char *)malloc(sizeof(char)*length);
	int pos = 0;

	for (int i = 0; i < length; i++)
	{
		rStr[pos] = str[i];
		if (str[i] == ' ')
		{
			++pos;
			rStr[pos] = ' ';
		}
		++pos;
	}
	rStr[pos] = 0;
	return rStr;
}

void sigHandler(int nr)
{
	switch (nr)
	{
		case SIGINT:
			printf ("SIGINT\n");
			unlink("./fifo");
			SafeExit(0);
			break;
		case SIGTERM:
			printf ("SIGTERM\n");
			unlink("./fifo");
			SafeExit(0);
			break;
	}
}

int setHandlers()
{
	sigset_t set;
	sigemptyset(&set);
	struct sigaction action;
	memset (&action, 0, sizeof(action));
	action.sa_handler = SIG_IGN;

	if (sigaction(SIGPIPE,&action,NULL) < 0)
		return -1;

	action.sa_handler = sigHandler;
	if (sigaction(SIGINT,&action,NULL) < 0)
		return -1;

	if (sigaction(SIGTERM,&action,NULL) < 0)
		return -1;

	return 0;
	
}

int clientId(char **fifos, char *fifo, int n)
{
	for (int i = 0; i <= n; i++)
		if (strcmp(fifos[i], fifo) == 0)
		{
			printf ("TAK: %s\n", fifos[i]);
			return i;
		}
	return -1;
}

int main()
{
	ERR(setHandlers());
	int maxClient = -1;
	char buffer[BUFFOR_SIZE];
	int clients[MAX_CLIENTS];
	char **fifos = (char **)malloc(sizeof(char *)*MAX_CLIENTS);
	if (fifos == NULL)
		SafeExit(0);

	for (int i = 0; i < MAX_CLIENTS; i++)
		fifos[i] = (char *)malloc(sizeof(char)*PATH_MAX);

	if(mkfifo("./fifo", 0777) < 0 && errno != EEXIST)
	{
		perror("fifo creation");
		return 1;
	}

	int fd = TEMP_FAILURE_RETRY(open("./fifo", O_RDONLY));
	ERR(fd);

	ERR(unlink("./fifo") < 0);

	for (int i = 0; i < 30; i++)
	{

		memset (buffer, 0, sizeof(buffer));
		int readSize = TEMP_FAILURE_RETRY(read(fd,buffer,BUFFOR_SIZE));
		ERR(readSize < 0);

		if (readSize <= 0)
			break;

		int length = strlen(buffer);
		char *fifo = buffer;
		char *order = buffer+length+1;
//		printf ("order: '%s'\n",order);

		if (BUFFOR_SIZE != readSize)
		{
			perror("Cos sie na prawde stalo != \n");
			return 1;
		}

		if (strcasecmp(order,"HELLO") == 0)
		{
			int fdFifo = TEMP_FAILURE_RETRY(open(fifo,O_WRONLY));
			printf ("Client: %d\n, %d - maxClient",
					fdFifo,maxClient+1);
			clients[maxClient+1] = fdFifo;
			int client = ++maxClient;
			memcpy(fifos[client], fifo, BUFFOR_SIZE);
	//		printf ("%d: \n", fdFifo);
			order[0] = 0;
		}


		if (strcasecmp(order,"EXIT") == 0)
			break;

		char *response = doubleSpaces(order);
		int client = clientId(fifos,fifo,MAX_CLIENTS);
		ERR(client);
		printf ("Client: %d, fd: %d\n", client,clients[client]);

		int fdOutput = clients[client];

		printf ("FDOUTPUT: %d\n", fdOutput);

		printf ("TAK\n");
		int res = write(fdOutput,response,BUFFOR_SIZE);
		printf ("NIE\n");
		ERR(res);
		if (res != BUFFOR_SIZE)
		{
			printf ("Inny rozmiar\n");
			SafeExit(1);
		}

	}

	for (int i = 0; i < MAX_CLIENTS; i++)
		close(clients[i]);

	for (int i = 0; i < MAX_CLIENTS; i++)
		free(fifos[i]);
	free(fifos);

	if (TEMP_FAILURE_RETRY(close(fd)) < 0)
	{
		perror("close");
		return 1;
	}

	return 0;
}
