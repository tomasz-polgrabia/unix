#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <stdlib.h>

#define BUFFOR_SIZE PIPE_BUF

#define ERR(arg) if ((arg) < 0) \
	{ \
		perror("ERR: "); \
		SafeExit(arg); \
	} 

#if BUFFOR_SIZE > PIPE_BUF
	#error nie może być tak
#endif

void SafeExit(int nr);
size_t bulk_write(int fd, char *buf, size_t count);
size_t bulk_read(int fd, char *buf, size_t count);
