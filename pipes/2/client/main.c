#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "common.h"
#include "signal.h"
#include <string.h>

void sigHandler(int nr)
{
	switch (nr)
	{
		case SIGINT:
			printf ("SIGINT\n");
			unlink("./fifo");
			SafeExit(0);
			break;
		case SIGTERM:
			unlink("./fifo");
			SafeExit(0);
			break;
	}
}

int setHandlers()
{
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = SIG_IGN;
	ERR(sigaction(SIGPIPE,&action,NULL));

	action.sa_handler = sigHandler;

	ERR(sigaction(SIGTERM,&action,NULL));
	ERR(sigaction(SIGINT,&action,NULL));
	
	return 0;

}

int main(int argc, char **argv)
{
	ERR(setHandlers());

	printf ("Helo\n");
	if (argc != 2)
	{
		printf ("Nie podales parametru\n");
		return 1;
	}

	char buffer[BUFFOR_SIZE] = "Ala ma kota";
	char buffer2[BUFFOR_SIZE];

	printf ("Opening %s\n", argv[1]);

	int fd = TEMP_FAILURE_RETRY(open(argv[1], O_WRONLY));
	ERR(fd);

	printf ("Open %s\n", argv[1]);

	ERR(mkfifo("./fifo", 0777));
	char *realPath = canonicalize_file_name("./fifo");

	if (realPath == NULL)
	{
		unlink("./fifo");
		SafeExit(0);
	}

	printf ("Real path: %s\n", realPath);

	memset (buffer, 0, sizeof(buffer));

	ERR(snprintf(buffer, BUFFOR_SIZE,"%s", realPath));
	ERR(snprintf(buffer+strlen(realPath)+1, BUFFOR_SIZE-strlen(realPath)-1,
				"%s", "HELLO"));

	int res = write(fd,buffer,BUFFOR_SIZE);
	ERR(res);
	if (res != BUFFOR_SIZE)
	{
		printf ("Sth. odd\n");
		unlink("./fifo");
		SafeExit(1);
	}

	char *order = buffer+strlen(realPath)+1;
	int orderLength = BUFFOR_SIZE-strlen(buffer)-1;
	memset (order, 0, orderLength);

	printf ("Wating to open\n");

	int fdMine = TEMP_FAILURE_RETRY(open("./fifo", O_RDONLY));
	ERR(fdMine);

	ERR(unlink("./fifo"));

	memset(order, 0, orderLength);
	sprintf (order, "Ala ma kota");

	for (int i = 0; i < 40; i++)
	{
		
		int writeCount = TEMP_FAILURE_RETRY(write(fd,buffer,BUFFOR_SIZE));
		if (writeCount < 0)
		{
			if (errno == EPIPE)
			{
				printf ("Broken pipe\n");
				break;
			}
		}	

		if (writeCount != BUFFOR_SIZE)
		{
			perror("sth. wrong\n");
			return 1;
		}

		printf ("I have written %s\n", buffer);

		int readCount = TEMP_FAILURE_RETRY(read(fdMine,buffer2,
					BUFFOR_SIZE));
		ERR(readCount);
		if (readCount != BUFFOR_SIZE)
		{
			printf ("Liczba sie nie zgadza\n");
			SafeExit(1);
		}

		printf ("Tekst: %s\n", buffer2);
		
		sleep(1);
	}

	sleep(5);

	free (realPath);

	ERR(TEMP_FAILURE_RETRY(close(fd)));

	return 0;
}
