#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include "common.h"

int setHandlers()
{
	sigset_t set;
	sigemptyset(&set);
	struct sigaction action;
	memset (&action, 0, sizeof(action));
	action.sa_handler = SIG_IGN;
	return sigaction(SIGPIPE,&action,NULL);
	
}

int main()
{
	if (setHandlers() < 0)
	{
		perror("setHandlers");
		return 1;
	}

	char buffer[BUFFOR_SIZE];

	if (mkfifo("./fifo", 0777) < 0 && errno != EEXIST)
	{
		perror("fifo creation");
		return 1;
	}

	int fd = TEMP_FAILURE_RETRY(open("./fifo", O_RDONLY));
	if (fd < 0)
	{
		perror("open");
		return 1;
	}

	for (int i = 0; i < 30; i++)
	{
		int readSize = TEMP_FAILURE_RETRY(read(fd,buffer,BUFFOR_SIZE));
		if (readSize < 0)
		{
			perror("read");
			return 1;
		}

		if (readSize <= 0)
			break;

		if (BUFFOR_SIZE != readSize)
		{
			perror("Cos sie na prawde stalo != \n");
			return 1;
		}

		printf ("I have read: %s\n", buffer);
	}

	if (TEMP_FAILURE_RETRY(close(fd)) < 0)
	{
		perror("close");
		return 1;
	}

	if (unlink("./fifo") < 0)
	{
		perror("unlink fifo");
		return 1;
	}

	return 0;
}
