#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BUFFOR_SIZE 1024

size_t bulk_write(int fd, char *buf, size_t count);
