#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "common.h"
#include "signal.h"
#include <string.h>

int setHandlers()
{
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = SIG_IGN;
	return sigaction(SIGPIPE,&action,NULL);
}

int main(int argc, char **argv)
{
	if (setHandlers() < 0)
	{
		perror("setHandlers");
		return 1;
	}
	printf ("Helo\n");
	if (argc != 2)
	{
		printf ("Nie podales parametru\n");
		return 1;
	}

	char buffer[BUFFOR_SIZE] = "Ala ma kota";

	printf ("Opening %s\n", argv[1]);

	int fd = TEMP_FAILURE_RETRY(open(argv[1], O_WRONLY));
	if (fd < 0)
	{
		perror("open");
		return 1;
	}

	printf ("Open %s\n", argv[1]);

	for (int i = 0; i < 40; i++)
	{
		int writeCount = TEMP_FAILURE_RETRY(write(fd,buffer,BUFFOR_SIZE));
		if (writeCount < 0)
		{
			if (errno == EPIPE)
			{
				printf ("Broken pipe\n");
				break;
			}

			if (writeCount != BUFFOR_SIZE)
			{
				perror("sth. wrong\n");
				return 1;
			}

			printf ("I have written %s\n", buffer);
		}	
	}

	sleep(5);

	if (TEMP_FAILURE_RETRY(close(fd)) < 0)
	{
		perror("close");
		return 1;
	}

	return 0;
}
