#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <sys/msg.h>
#include <sys/sem.h>

#include <sys/wait.h>

#include <signal.h>
#include <string.h>

void usage()
{	
	fprintf (stderr, "Usage: \n");
	fprintf (stderr, "./Przesylacz file k n\n");
	fprintf (stderr, "1<=k<=10 - wielkość pakietu w bajtach\n");
	fprintf (stderr, "1<=n<=10 - liczba czytających\n");
}

#define MAX_LENGTH 10

sig_atomic_t queue;
sig_atomic_t sems;

int destroyQueue(int key)
{
	return msgctl(key,IPC_RMID,NULL);
}

int destroySemaphores(int key)
{
		return semctl(key,0,IPC_RMID);
}

void sigHandler(int nr)
{
	switch(nr) {
		case SIGINT:
			destroyQueue(queue);
			destroySemaphores(sems);
			break;
		case SIGTERM:
			destroyQueue(queue);
			destroySemaphores(sems);	
			break;
	}
}

int setHandlers()
{
	struct sigaction action;
	memset (&action, 0, sizeof(action));
	
	action.sa_handler = sigHandler;
	if (sigaction(SIGINT, &action, NULL) < 0)
		return -1;
	if (sigaction(SIGTERM, &action, NULL) < 0)
		return -1;		
		
	return 0;
	
	
}


typedef struct 
{
	long mtype;
	pid_t pid;
	char msg[MAX_LENGTH+1];
} message;

int createQueue()
{
	return msgget(IPC_PRIVATE, IPC_CREAT | 0755);
}

int createSemaphores(int n)
{
	int val = semget(IPC_PRIVATE, n, IPC_CREAT | 0755);
	if (val < 0)
		return val;
	
	for (int i = 0; i < n; i++)
	{
		if (semctl(val, i, SETVAL, (i == 0) ? 1 : 0) < 0)
		{
			destroySemaphores(val);
			perror("semctl");
			return -1;
		}
	}
	
	return val;
	
}

int semWait(int k)
{
	struct sembuf op;
	memset (&op, 0, sizeof(op));
	op.sem_num = k;
	op.sem_op = -1;
	if (TEMP_FAILURE_RETRY(semop(sems, &op, 1)) < 0)
		return -1;
	
	return 0;
}

int semRelease(int k)
{
	struct sembuf op;
	
	memset (&op, 0, sizeof(op));
	op.sem_num = k;
	op.sem_op = 1;	
	
	if (TEMP_FAILURE_RETRY(semop(sems, &op, 1)) < 0)
		return -1;
	
	return 0;
	
}

int childMain(int fd,int nr, int n, int k)
{
	
	fprintf (stderr, "child %d runned\n", nr);
	char buf[MAX_LENGTH+1];
	message msg;
	
	while (1)
	{
	
		if (semWait(nr) < 0)
		{
			if (errno == EIDRM)
				exit(0);
				
			perror("semWait");
			exit(3);
		}
		
		memset (buf, 0, sizeof(buf));
	
		int readCount = TEMP_FAILURE_RETRY(read(fd,buf,k));
		if (readCount < 0)
		{
			perror("read");
			exit(2);
		}
		
		if (readCount == 0)
		{
			destroySemaphores(sems);
			destroyQueue(queue);
			exit(0);
		}
		
		memset (&msg, 0, sizeof(msg));
		
		msg.mtype = 1;
		msg.pid = getpid();
		memcpy (&msg.msg, buf, sizeof(buf));
		
		if (msgsnd(queue, &msg, sizeof(msg), 0) < 0)
		{
			perror("msgsnd");
			exit(2);
		}
		
		
		if (semRelease((nr + 1) % n) < 0)
		{
			if (errno == EIDRM)
				exit(0);
				
			perror("semRelease");
			exit(2);			
		}
		
		
	
	}
	
	
	return 0;
}

int createReaders(int fd, int n, int k)
{
	
	for (int i = 0; i < n; i++)
	{
		pid_t child = fork();
		if (child < 0)
		{
			perror("fork");
			return -1;
		}
		
		if (child == 0)
			exit(childMain(fd, i, n,k));
		
	}
	
	return 0;
}

int main(int argc, char **argv)
{
	if (argc != 4)
	{
		usage();
		exit(1);
	}
	
	if (setHandlers() < 0)
	{
		perror("setHandlers");
		exit(2);
	}
	
	int k = atoi(argv[2]);
	int n = atoi(argv[3]);
	
	if (k < 1 || k > 10 || n < 1 || n > 10)
	{
		usage();
		exit(1);
	}
	
	
	printf ("%s - k : %d, n: %d\n",
		argv[1], k,n);
		
	int fd = TEMP_FAILURE_RETRY(open(argv[1], O_RDONLY));
	if (fd < 0)
	{
		perror("open");
		exit(2);
	}
	
	queue = -1;
	if ((queue = createQueue()) < 0)
	{
		perror("createQueue");
		exit(2);
	}
	
	sems = createSemaphores(n);
	if (sems < 0)
	{
		perror("createSemaphores");
		destroyQueue(queue);
		exit(2);
	}
	
	if (createReaders(fd,k,n) < 0)
		perror("createReaders");
	
	fprintf (stderr, "Waiting for messages\n");
	
	message msg;
	
	while (msgrcv(queue, &msg, sizeof(msg), 0, 0) >= 0)
		printf ("Message [%d]: %s\n", msg.pid, msg.msg);
		
		
	fprintf (stderr, "Waiting finished\n");
	
		
	for (;;)
	{
		int status = waitpid(0, NULL, 0);
		if (status <= 0)
		{
			
			if (errno == ECHILD)
				break;
			
			perror("waitpid");
			break;
		}
	}
	
	if (TEMP_FAILURE_RETRY(close(fd)) < 0)
	{
		perror("close");
		exit(2);
	}
	
	return 0;
}
