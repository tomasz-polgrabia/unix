/*******************************************************************************
Rozwiazanie zadania:
http://www.mini.pw.edu.pl/~marcinbo/strona/zadania/archunix4?.html
Marcin Borkowski
marcinbo (at) mini (dot) pw (dot) edu (dot) pl
********************************************************************************/

#define _GNU_SOURCE 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#define ERR(source) (fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
                     perror(source),kill(0,SIGKILL),\
		     		     exit(EXIT_FAILURE))

#define PID 8
#define MAXMSG PID+80+1

typedef struct {
	long mtype;
	char mtext[MAXMSG];
} packet;

union semun {			 
	int              val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf;  /* Buffer for IPC_INFO
				    (Linux specific) */
};

void semInit(int sem, int startValue, int semNum){
	union semun initUnion;
	initUnion.val = startValue;
	if (semctl(sem,semNum,SETVAL,initUnion) == -1)
		ERR("Semafore initialization:");
}

void removeSem(int sem) {
	if(semctl(sem,0,IPC_RMID)<0){
		if(EINVAL!=errno)ERR("Remove semaphores:");
		else exit(EXIT_SUCCESS);
	}
}

void removeMsq(int queue) {
	if(msgctl(queue,IPC_RMID,NULL)<0){
		if(EINVAL!=errno)ERR("Remove message queue:");
		else exit(EXIT_SUCCESS);
	}
}

void removeShm(int shm) {
	if(shmctl(shm,IPC_RMID,NULL)<0){
		if(EINVAL!=errno)ERR("Remove shared memory:");
		else exit(EXIT_SUCCESS);
	}
}

void semOp (int sem, int semNum,int value,short flag){
	struct sembuf buffer;
	buffer.sem_num = semNum;
	buffer.sem_op = value;
	buffer.sem_flg = flag;
	if (TEMP_FAILURE_RETRY(semop(sem,&buffer,1)) == -1){
		switch(errno){
		case EIDRM:
		case EINVAL: exit(EXIT_SUCCESS);
		default: ERR("Semaphore operation:");
		}
	}
}

int sethandler( void (*f)(int), int sigNo) {
	struct sigaction act;
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = f;
	if (-1==sigaction(sigNo, &act, NULL))
		return -1;
	return 0;
}

void sigchld_handler(int sig) {
	pid_t pid;
	for(;;){
		pid=waitpid(0, NULL, WNOHANG);
		if(0==pid) return;
		if(0>=pid) {
			if(ECHILD==errno) return;
			ERR("waitpid:");
		}
	}
}

void child_work(long num,int msg) {
	packet p1;	
	switch (fork()) {
		case 0:
			for(;;){
				if(TEMP_FAILURE_RETRY(msgrcv(msg,&p1,MAXMSG,num,0))<0) exit(EXIT_SUCCESS);
				printf("%s",p1.mtext);
			}  
		case -1: ERR("Fork:");
	}
}

void parent_work(long *sbuf,int sem,int msg,long num) {
	packet p1;
	ssize_t size=snprintf(p1.mtext,PID,"[%d] ",getpid());
	if(size<4) return;
	while(fgets(p1.mtext+size,MAXMSG-size,stdin)){
		semOp(sem,0,-1,SEM_UNDO);
			for(p1.mtype=1;p1.mtype<=*sbuf;p1.mtype++)
				if(p1.mtype!=num)
					if(TEMP_FAILURE_RETRY(msgsnd(msg,&p1,MAXMSG,0))<0)return;
		semOp(sem,0,1,SEM_UNDO);
	}
}

int makeSemZero(key_t key){
	int sem;
	if((sem = semget(key,1,IPC_CREAT|IPC_EXCL|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH))<0){
		if(EEXIST!=errno) ERR("Create semaphores:");
	}
	if(sem<0) {
		if((sem = semget(key,0,0))<0) ERR("Attach to semaphores:");
	} else semInit(sem,1,0);
	return sem;
}

int makeShm(size_t size, key_t key){
	int shm;
	if((shm = shmget(key,size,IPC_CREAT|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH))<0)
		 ERR("Create shared memory:");
	return shm;
}

int makeMsq(key_t key){
	int queue;
	if((queue = msgget(key,IPC_CREAT|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH))<0)
		ERR("Create queue:");
	return queue;
}

int main() {
	int sem,shm,msg;
	key_t key;
	long num,*buf;
	if((key=ftok(".",'s'))<0)ERR("ftok");
	sem=makeSemZero(key);
	msg=makeMsq(key);
	shm=makeShm(sizeof(long),key);
	if((buf=shmat(shm,NULL,0))==(void *)-1)ERR("shmat:");	
	if(sethandler(sigchld_handler,SIGCHLD)) ERR("Setting SIGCHLD:");
	semOp(sem,0,-1,SEM_UNDO);
		num=++(*buf);
	semOp(sem,0,1,SEM_UNDO);
	child_work(num,msg);
	parent_work(buf,sem,msg,num);
	if(shmdt(buf)<0)ERR("shmdt:");	
	removeSem(sem);
	removeMsq(msg);
	removeShm(shm);
	TEMP_FAILURE_RETRY(wait(NULL));
	return EXIT_SUCCESS;
}
