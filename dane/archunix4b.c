#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>

#define CONSUMPTION_TIME 1

union semun {
	int val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
};

volatile sig_atomic_t stop_work = 0;
volatile sig_atomic_t timeout = 0;

void sigusr1_hnd(int sig);
void sigalrm_hnd(int sig);
void xset_handler(void (*f)(int), int sigNum);

void parse_parameters(int argc,char ** argv, int *n, int * t, int *k);
void fatal(const char *message);
int xfflush(FILE * file, int b_fatal);
int wread(int fd, char * buffer, size_t s);
int wwrite(int fd, const char * buffer, size_t s);

void create_semaphores(int * set,int n);
void remove_semaphores(int sset);

int P(int sset, int s, int t, int *b_timeout);
int V(int sset, int s);

void create_and_run_workers( const int k, const int n, int t, int sset,char ** filenames);
int worker(const int n, int t, int sset, char * filename, int sem_i, int sem_ip1);
int open_output_file(const char * fielname, pid_t pid);
char * alloc_buffer( int size, pid_t pid);



int set_handler(void (*f)(int), int sigNum);


int main(int argc, char** argv)
{
	int k = 0, n = 0, t = 0,sset = 0;
	xset_handler(sigusr1_hnd, SIGUSR1);
	xset_handler(sigalrm_hnd, SIGALRM);
	parse_parameters(argc,argv,&n, &t, &k);
	create_semaphores(&sset, k);
	create_and_run_workers(k, n, t, sset, argv+3);

    pid_t cpid = TEMP_FAILURE_RETRY(wait(0));
	printf("PARENT: child process [%d] waited (exited oneself)\n", cpid);
    kill(0, SIGUSR1);
    remove_semaphores(sset);
    printf("PARENT: semaphores removed\n");

    while(1){
        cpid = TEMP_FAILURE_RETRY(wait(0));
        if( -1 == cpid)
            break;
        printf("PARENT: child process [%d] waited (after signal) \n", cpid);
        if(-1 == xfflush(stdout,0))
            fprintf(stderr, "Parent: cannot flush stdout: %s", strerror(errno));

    }
	printf("PARENT: all child processes waited\n");
    printf("PARENT: finish\n");
	return 0;
}

void parse_parameters(int argc, char ** argv, int *n, int * t, int *k)
{
    const char * usage = "Invalid usage. Try: nsplitter n t out_file_1 out_file_2 ... out_file_k  \n";
    (*k) = argc - 3;
	if((*k) < 2 ){
        fprintf(stderr,usage);
        exit(EXIT_FAILURE);
	}
	(*n) = atoi(argv[1]);
    if(*n<1){
        fprintf(stderr,usage);
        exit(EXIT_FAILURE);
    }
	(*t) = atoi(argv[2]);
    if(*t<1){
        fprintf(stderr,usage);
        exit(EXIT_FAILURE);
    }


}

void fatal(const char *message)
{
	perror(message);
	exit(EXIT_FAILURE);
}

int xfflush(FILE * file, int b_fatal)
{
	while( EOF == fflush(file) ){
		if(errno == EINTR )
			continue;
		perror("fflush failed");
		if(b_fatal)
			exit(EXIT_FAILURE);
		return -1;
	}
	return 0;
}

void create_semaphores(int * sset, int k)
{
	union semun su;

	if ((*sset = semget(IPC_PRIVATE, k, IPC_CREAT | 0666)) == -1)
		fatal("semget failed");
        //fprintf(stderr, "Created semset of %d semaphores, semset no: %d\n",k,*sset);

	su.val = 1;
	if (semctl(*sset, 0, SETVAL, su) == -1){
		remove_semaphores(*sset);
		fatal("Cannot initialize first semaphore");
	}

    su.val = 0;
    int i=1;
	for( ; i < k ; i++){
        if (semctl(*sset, i , SETVAL, su) == -1){
            remove_semaphores(*sset);
            fatal("Cannot initialize semaphores");
        }
	}
}

void remove_semaphores(int sset)
{
	if (semctl(sset, 0, IPC_RMID) == -1)
		perror("Cannot remove semaphores");
}

int P(int sset, int s, int t, int *b_timeout)
{
    if(stop_work ==1)
        return 0;
	static struct sembuf sbuf;
	sbuf.sem_op = -1;
	sbuf.sem_num = s;
	timeout = 0;
	alarm (t);
	while (semop(sset, &sbuf, 1) == -1)
	{
	    if(timeout | stop_work){
            alarm(0);
            return 0;
	    }
		if (errno == EINTR)
			continue;
		return -1;
	}
	alarm(0);
	return 0;
}

int V(int sset, int s)
{
	static struct sembuf sbuf;
	sbuf.sem_op = 1;
	sbuf.sem_num = s;
	return TEMP_FAILURE_RETRY(semop(sset, &sbuf, 1));
}


void create_and_run_workers(const int k, const int n, const int t, const int sset, char **filenames)
{
	int i = 0, ec = EXIT_SUCCESS;

	for(;i < k;i++){
		switch(fork()){
			case -1:
				fatal("fork failed");
			case 0:
				ec = worker(n,t, sset, filenames[i], i, (i+1)%k);
				exit(ec);
		}
	}
}


int open_output_file(const char * filename, pid_t pid)
{

    int fd = TEMP_FAILURE_RETRY(open(filename,O_WRONLY | O_EXCL | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
	if(fd == -1 ){
        fprintf(stderr, "Worker [%d]: cannot open input file [%s]: %s", pid, filename, strerror(errno));
	    exit(EXIT_FAILURE);
	}
	return fd;
}

char * alloc_buffer( int size, pid_t pid)
{
    char * buffer = (char *)malloc(size);
	if( buffer == NULL ){
        fprintf(stderr, "Worker [%d]: cannot allocate memory: %s", pid, strerror(errno));
	    exit(EXIT_FAILURE);
	}
	buffer[size-1]=0;
	return buffer;
}
int worker(const int n, int t, int sset, char * filename, int sem_i, int sem_ip1)
{
	pid_t pid = getpid();
	char * buffer = alloc_buffer(n, pid);
	int nn=0, b_timeout = 0, fd = open_output_file(filename, pid);
	while(1){
		if(P(sset,sem_i, t, &b_timeout) == -1 ) {
            fprintf(stderr, "Worker [%d]: Error of the P semaphore operation: %s.", pid, strerror(errno));
            break;
		}
            if( timeout == 1 ){
                fprintf(stderr,"[%d, plik %s] TIMEOUT - KONIEC PRZETWARZANIA\n",pid, filename);
                break;
            }
            nn =  wread(STDIN_FILENO,buffer,n);
            if(stop_work ==1 ){
                fprintf(stderr,"[%d, plik %s] KONIEC PRZETWARZANIA\n",pid, filename);
                break;
            }
        if( n == nn ) {
            if( V(sset, sem_ip1) == -1 ) {
                fprintf(stderr, "Worker [%d]: Error of the V semaphore operation: %s.", pid, strerror(errno));
                break;
            }
        } else fprintf(stderr, "[%d, plik %s] KONIEC WEJŚCIA \n",pid, filename);
        if( wwrite(fd, buffer, nn) == -1 ){
            fprintf(stderr, "Worker [%d]: cannot write to the output file: %s", pid, strerror(errno));
            break;
        }
        if(nn<n) break;
	}
    if(-1 == xfflush(stderr,0))
        fprintf(stderr, "Worker [%d]: cannot flush stderr: %s", pid, strerror(errno));
    if(-1 == TEMP_FAILURE_RETRY(close(fd)))
        fprintf(stderr, "Worker [%d]: cannot close input file [%s]: %s", pid, filename, strerror(errno));
    free(buffer);
	return EXIT_SUCCESS;
}

void xset_handler(void (*f)(int), int sigNum){
    struct sigaction sa;
    memset( (&sa),0, sizeof(struct sigaction));
    sa.sa_handler = f;
    if(-1 == sigaction(sigNum, &sa, NULL) )
        fatal("sigacton failed");
}

void sigusr1_hnd(int sig){
    stop_work = 1;
}

void sigalrm_hnd(int sig){
    timeout = 1;
}

int wread(int fd, char * buffer, size_t s)
{
    if(stop_work || s==0)
        return 0;
	size_t off = 0, i = 0;
	while(1){
		i = read(fd, buffer+off, s);
		if(i < 0){
			if(errno == EINTR ){
			    if(stop_work)
                    return off;
				continue;
			}
			perror("read failed");
			exit(EXIT_FAILURE);
		}
		if(i == 0)
			s = 0;
		s -= i;
		off += i;
		if(s == 0 || stop_work)
			return off;
	}
	return off;
}

int wwrite(int fd, const char * buffer, size_t s)
{
    if( stop_work || s==0 )
        return 0;
    size_t off = 0, i = 0;
    while(1){
        i = write(fd, buffer+off, s);
        if(i < 1){
            if(errno == EINTR ){
                if(stop_work )
                    return 0;
                continue;
            }
            return -1;
        }
        if( stop_work )
            return 0;
        s -= i;
        if(s > 0)
            off += i;
        else
            return 0;
    }
}
