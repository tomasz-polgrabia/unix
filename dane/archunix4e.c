#define _GNU_SOURCE 
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <wait.h>
#include <string.h>
#define ERR(source) (fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
                     perror(source),kill(0,SIGKILL),\
		     		     exit(EXIT_FAILURE))

union semun
{			 
	int              val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf;  /* Buffer for IPC_INFO
				    (Linux specific) */
};

void sigchld_handler(int sig) 
{
	pid_t pid;
	for(;;)
	{
		pid=waitpid(0, NULL, WNOHANG);
		if(0==pid) return;
		if(0>=pid) 
		{
			if(ECHILD==errno) return;
			ERR("waitpid:");
		}
	}
}

int sethandler( void (*f)(int), int sigNo) 
{
	struct sigaction act;
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = f;
	if (-1==sigaction(sigNo, &act, NULL))
		return -1;
	return 0;
}

void usage(void)
{
	fprintf(stderr,"USAGE: zad2  c s\n");
	fprintf(stderr,"c - of clients\n");
	fprintf(stderr,"s - of cashdesks>0\n");
	exit(EXIT_FAILURE);
}

void semInit(int sem, int startValue, int semNum)
{
	union semun initUnion;
	initUnion.val = startValue;
	if (semctl(sem,semNum,SETVAL,initUnion) == -1)
		ERR("Semafore initialization:");
}

void semOp (int sem, int semNum,int value)
{
	struct sembuf buffer;
	buffer.sem_num = semNum;
	buffer.sem_op = value;
	buffer.sem_flg = 0;
	if (TEMP_FAILURE_RETRY(semop(sem,&buffer,1)) == -1)
		ERR("Semaphore operation:");
}

void supermarket(int sem,int* buf,int cashdesks)
{
	int i;
	printf("Otwarcie kas:\n");
	for(i=0;i<=cashdesks;++i)semOp(sem,i+1,1);
	semOp(sem,0,0);//zainicjowane iloscia klientow, po zakonczeniu zmniejszaja licznik
	printf("Wszyscy skonczyli zakupy. Zamkniecie sklepu\n");
	printf("Bilans dnia to:%d zlotych\n",*buf);
}

void client_process(int sem,int* buf, int nr,int cashdesks) 
{
	int cashdesk_nr,bill, tt, t = 1;
	srand(getpid());	
	cashdesk_nr=rand()%(cashdesks);
	bill=rand()%100;
	semOp(sem,cashdesk_nr+2,-1);//ustawiam sie w chaotycznej kolejce
	//dostaje sie do kasy
	printf("[%d] Zaczynam zakupy przy kasie nr:%d\n",nr, cashdesk_nr);
	for (tt = t; tt > 0; tt = sleep(tt));
	semOp(sem,1,-1);
	(*buf)+=bill;	
	printf("[%d] place %d, zwalniam kase nr %d \n",nr,bill, cashdesk_nr);
	semOp(sem,1,1);
	semOp(sem,cashdesk_nr+2,1);//kasa zwolniona
	semOp(sem,0,-1);//wychodze ze sklepu
	if(shmdt(buf)<0) ERR("shmdt:");
	exit(EXIT_SUCCESS);
}

void makeIPC(int *sem,int *shm, int count, size_t size, int clients)
{
	int i;
	if((*sem = semget(IPC_PRIVATE,count,IPC_CREAT|IPC_EXCL|S_IRUSR|S_IWUSR))<0)
		ERR("Create semaphores:");
	
	semInit(*sem,clients,0);//zainicjowane iloscia klientow, potem na tej podstawie zamykamy jak juz nie ma chetnych
	for(i=1;i<count;i++) semInit(*sem,0,i);
		
	if((*shm = shmget(IPC_PRIVATE,size,IPC_CREAT|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH))<0)
		ERR("Create shared memory:");
	
}

void removeIPC(int sem,int shm) 
{
	if(semctl(sem,0,IPC_RMID)<0)
		ERR("Remove semaphores:");
		
	if(shmctl(shm,IPC_RMID,NULL)<0)
		ERR("Remove shared memory:");
		
}

int main(int argc, char** argv)
{
	int i, clients, cashdesks, sem, shm;
	pid_t child;
	int *buf;
	
	if(argc!=3) usage();
	clients = atoi(argv[1]);
	cashdesks=atoi(argv[2]);
	if(cashdesks==0)usage();

	if(sethandler(sigchld_handler,SIGCHLD)) 
		ERR("Setting SIGCHLD:");	

	makeIPC(&sem,&shm,cashdesks+2, sizeof(int),clients);	
	if((buf=shmat(shm,NULL,0))==(void *)-1)ERR("shmat:");

	*buf=0;
	for(i=0;i<clients;i++)
	{
		child = fork();
		if(child < 0) ERR("fork:");
		if(child==0) client_process(sem,buf,i, cashdesks);
	}
	supermarket(sem,buf,cashdesks);
	if(shmdt(buf)<0) ERR("shmdt:");
	while(TEMP_FAILURE_RETRY(wait(NULL))>0);
	removeIPC(sem,shm);
	return EXIT_SUCCESS;
}

