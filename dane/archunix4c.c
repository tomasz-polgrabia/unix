/*******************************************************************************
Rozwiazanie zadania:
http://www.mini.pw.edu.pl/~marcinbo/strona/zadania/archunix4?.html
Marcin Borkowski
marcinbo (at) mini (dot) pw (dot) edu (dot) pl
********************************************************************************/

#define _GNU_SOURCE 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#define ERR(source) (fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
                     perror(source),kill(0,SIGKILL),\
		     		     exit(EXIT_FAILURE))
#define MAXMSG 81

typedef struct {
	pid_t pid;
	char buf[MAXMSG];
}shbuf;

union semun {			 
	int              val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf;  /* Buffer for IPC_INFO
				    (Linux specific) */
};

void semInit(int sem, int startValue, int semNum){
	union semun initUnion;
	initUnion.val = startValue;
	if (semctl(sem,semNum,SETVAL,initUnion) == -1)
		ERR("Semafore initialization:");
}

void semOp (int sem, int semNum,int value,short flag){
	struct sembuf buffer;
	buffer.sem_num = semNum;
	buffer.sem_op = value;
	buffer.sem_flg = flag;
	if (TEMP_FAILURE_RETRY(semop(sem,&buffer,1)) == -1)
		ERR("Semaphore operation:");
}

int sethandler( void (*f)(int), int sigNo) {
	struct sigaction act;
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = f;
	if (-1==sigaction(sigNo, &act, NULL))
		return -1;
	return 0;
}

void sigchld_handler(int sig) {
	pid_t pid;
	for(;;){
		pid=waitpid(0, NULL, WNOHANG);
		if(0==pid) return;
		if(0>=pid) {
			if(ECHILD==errno) return;
			ERR("waitpid:");
		}
	}
}

void child_work(shbuf *sbuf,int sem) {
	while (1) {
		semOp(sem,1,-1,0);
		semOp(sem,0,-1,SEM_UNDO);
		if(getppid()!=sbuf->pid)
			printf("[%d] %s",sbuf->pid,sbuf->buf);
		semOp(sem,0,1,SEM_UNDO);
	}
}

int getNoWait(int sem, int no) {
	int ret;
	if((ret=semctl(sem,no,GETNCNT))<0) ERR("semctl");
	return ret;
}

void parent_work(shbuf *sbuf,int sem) {
	char lbuf[MAXMSG];
	sigset_t mask;
	sigemptyset (&mask);
	sigaddset (&mask, SIGINT);
	while(fgets(lbuf,MAXMSG,stdin)){
		sigprocmask (SIG_BLOCK, &mask,NULL);
		semOp(sem,0,-1,0);
		memcpy(sbuf->buf,lbuf,MAXMSG);
		sbuf->pid=getpid();
		semOp(sem,1,getNoWait(sem,1),0);
		semOp(sem,0,1,0);
		sigprocmask (SIG_UNBLOCK, &mask,NULL);
	}
	if(kill(0,SIGINT)<0)ERR("kill:");
}

void create_child(shbuf *sbuf, int sem) {
	switch (fork()) {
		case 0:
			child_work(sbuf,sem);
			exit(EXIT_SUCCESS);
		case -1: ERR("Fork:");
	}
}

int makeSemZero(key_t key){
	int sem;
	if((sem = semget(key,2,IPC_CREAT|IPC_EXCL|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH))<0){
		if(EEXIST!=errno) ERR("Create semaphores:");
	}
	if(sem<0) {
		if((sem = semget(key,0,0))<0) ERR("Attach to semaphores:");
	} else {
		semInit(sem,1,0);
		semInit(sem,0,1);
	}
	return sem;
}

int makeShm(size_t size, key_t key){
	int shm;
	if((shm = shmget(key,size,IPC_CREAT|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH))<0)
		 ERR("Create shared memory:");
	return shm;
}

int main() {
	int sem,shm;
	key_t key;
	shbuf *buf;
	if((key=ftok(".",'s'))<0)ERR("ftok");
	sem=makeSemZero(key);
	shm=makeShm(sizeof(shbuf),key);
	if((buf=shmat(shm,NULL,0))==(void *)-1)ERR("shmat:");	
	if(sethandler(sigchld_handler,SIGCHLD)) ERR("Setting SIGCHLD:");
	create_child(buf,sem);
	parent_work(buf,sem);
	if(shmdt(buf)<0)ERR("shmdt:");	
	return EXIT_SUCCESS;
}
