#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>

#define CONSUMPTION_TIME 1

union semun {
	int val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
};
volatile sig_atomic_t stop_work = 0;


int set_handler(void (*f)(int), int sigNum);
void sigusr1_hnd(int sig);
void parse_parameters(int argc,char ** argv, int *n, int *k);
void fatal(const char *message);
void * xmalloc (size_t size);
int xopen(const char * filename, int flags);
int wread(int fd, char * buffer, size_t s);
int xfflush(FILE * file, int b_fatal);

void create_semaphores(int * set,int n);
void remove_semaphores(int sset);
int P(int sset, int s);
int V(int sset, int s);

void create_and_run_workers( const int k, const int n, int sset,char ** filenames);
int worker(const int n,int sset, char * filename, int sem_i, int sem_ip1);
int print_to_stdout(int nn, int n, int pid, char * filename, char * buffer);

int main(int argc, char** argv)
{
	int k = 0, n = 0, sset = 0;
	int i = 0;
	pid_t cpid = 0;

	set_handler(sigusr1_hnd, SIGUSR1);
	parse_parameters(argc,argv,&n,&k);
	create_semaphores(&sset, k);
	create_and_run_workers(k, n, sset, argv+2);

    cpid = TEMP_FAILURE_RETRY(wait(0));
	printf("PARENT: child process [%d] waited (exited oneself)\n", cpid);

    kill(0, SIGUSR1);
    remove_semaphores(sset);
    printf("PARENT: semaphores removed\n");

    while(1){
        cpid = TEMP_FAILURE_RETRY(wait(0));
        if( -1 == cpid)
            break;
        printf("PARENT: child process [%d] waited (after SIGUSR1) \n", cpid);
        if(-1 == xfflush(stdout,0))
            fprintf(stderr, "Parent: cannot flush stdout: %s", strerror(errno));

    }
	printf("PARENT: all child processes waited\n");
    printf("PARENT: finish\n");
	return i;
}


int set_handler(void (*f)(int), int sigNum){
    struct sigaction sa;
    memset( (&sa),0, sizeof(struct sigaction));
    sa.sa_handler = f;
    if(-1 == sigaction(sigNum, &sa, NULL) )
        return -1;
    return 0;
}

void sigusr1_hnd(int sig){
    stop_work = 1;
}


void parse_parameters(int argc, char ** argv, int *n, int *k)
{
    const char * usage = "Invalid usage. Try: nmixer n in_file_1 in_file_2 ... in_file_k\n";
    (*k) = argc - 2;
	if((*k) < 2 ){
        fprintf(stderr,usage);
        exit(EXIT_FAILURE);
	}
	(*n) = atoi(argv[1]);
    if(*n<1){
        fprintf(stderr,usage);
        exit(EXIT_FAILURE);
    }

}

void create_semaphores(int * sset, int k)
{
	union semun su;

	if ((*sset = semget(IPC_PRIVATE, k, IPC_CREAT | 0666)) == -1)
		fatal("semget failed");

	su.val = 1;
	if (semctl(*sset, 0, SETVAL, su) == -1){
		remove_semaphores(*sset);
		fatal("Cannot initialize first semaphore");
	}

    su.val = 0;
    int i=1;
	for( ; i < k ; i++){
        if (semctl(*sset, i , SETVAL, su) == -1){
            remove_semaphores(*sset);
            fatal("Cannot initialize semaphores");
        }
	}
}

void create_and_run_workers(const int k, const int n, const int sset, char **filenames)
{
	int i = 0, ec = EXIT_SUCCESS;

	for(;i < k;i++){
		switch(fork()){
			case -1:
				fatal("fork failed");
			case 0:
				ec = worker(n,sset, filenames[i], i, (i+1)%k);
				exit(ec);
		}
	}
}

int worker(const int n,int sset, char * filename, int sem_i, int sem_ip1)
{
	int nn=0;
	pid_t pid = getpid();
	char * buffer = (char *) xmalloc(n+1);
	int fd = xopen(filename, O_RDONLY);
	while(1){
	    if(!stop_work){
            nn =  wread(fd,buffer,n);
            buffer[nn] = 0;
	    }
		if(P(sset,sem_i) == -1){
            fprintf(stderr, "Worker [%d]: Error of the P semaphore operation: %s.", pid, strerror(errno));
            break;
		}

            if( print_to_stdout(nn, n, pid, filename, buffer) == -1 )
                break;
            if(stop_work || nn<n)
                break;

		if( -1 == V(sset, sem_ip1) ) {
            fprintf(stderr, "Worker [%d]: Error of the V semaphore operation: %s.", pid, strerror(errno));
            break;
        }
	}
    if(-1 == TEMP_FAILURE_RETRY(close(fd)))
        fprintf(stderr, "Worker [%d]: cannot close input file [%s]: %s", pid, filename, strerror(errno));
    free(buffer);
	return EXIT_SUCCESS;
}


int print_to_stdout(int nn, int n, int pid, char * filename, char * buffer)
{
    int res = 0;
    if(stop_work ==1 ){
        res = printf("[%d, plik %s] KONIEC PRZETWARZANIA \n",pid, filename);
    } else {
        if(nn<n)
            res = printf("[%d, plik %s] KONIEC PLIKU \n",pid, filename);
        else
            res = printf("[%d, plik %s] [%s] \n",pid, filename, buffer);
    }
    if( res < 0 ){
        fprintf(stderr, "Worker [%d]: cannot printf to the stdout: %s", pid, strerror(errno));
        return -1;
    }
    if(-1 == xfflush(stdout,0)){
        fprintf(stderr, "Worker [%d]: cannot flush the stdout: %s", pid, strerror(errno));
        return -1;
    }
    return 0;
}


void remove_semaphores(int sset)
{
	if (semctl(sset, 0, IPC_RMID) == -1)
		perror("Cannot remove semaphores");
}


int P(int sset, int s)
{
	static struct sembuf sbuf;
	sbuf.sem_op = -1;
	sbuf.sem_num = s;
	while (semop(sset, &sbuf, 1) == -1)
	{
	    if(stop_work == 1)
            return 0;
//        if( errno == EIDRM){
//            stop_work = 1;
//            return 0;
//        }
		if (errno == EINTR){
			continue;
		} else {
            fprintf(stderr, "Semaphore %d, sem_num: %d, sem_op: %d: ",sset, sbuf.sem_num, sbuf.sem_op);
            fatal("cannot P a semaphore - semop failed");
            return -1;
		}
	}
	return 0;
}

int V(int sset, int s)
{
	static struct sembuf sbuf;
	sbuf.sem_op = 1;
	sbuf.sem_num = s;
	return TEMP_FAILURE_RETRY(semop(sset, &sbuf, 1));
}

int xopen(const char * filename, int flags)
{
	int fd = TEMP_FAILURE_RETRY(open(filename,flags));
	if(fd == -1 ){
        fprintf(stderr, "Process [%d]: cannot open file [%s]: %s. Processing terminated.\n", getpid(), filename, strerror(errno));
	    exit(EXIT_FAILURE);
	}
	return fd;
}

int wread(int fd, char * buffer, size_t s)
{
	size_t off = 0, i = 0;
	while(1){
		i = read(fd, buffer+off, s);
		if(i < 0){
			if(errno == EINTR ){
			    if(stop_work)
                    break;
				continue;
            }
			perror("read failed");
			exit(EXIT_FAILURE);
		}
		if(i == 0)
			s = 0;
		s -= i;
		off += i;
		if(s == 0 || stop_work)
			return off;
	}
	return off;
}


void fatal(const char *message)
{
	perror(message);
    fflush(stderr);
	exit(EXIT_FAILURE);
}

int xfflush(FILE * file, int b_fatal)
{
	while( EOF == fflush(file) ){
		if(errno == EINTR )
			continue;
		if(b_fatal){
			perror("fflush failed");
			exit(EXIT_FAILURE);
		}
		return -1;
	}
	return 0;
}

void * xmalloc (size_t size)
{
    void * buffer = malloc(size);
	if( buffer == NULL ){
        fprintf(stderr, "Process [%d]: cannot allocate memory: %s", getpid(), strerror(errno));
	    exit(EXIT_FAILURE);
	}
	return buffer;
}
