#include "file.h"

ssize_t bulk_read(int fd, char *buf, size_t count) {
	int c;
	size_t len=0;
	do {
		c = TEMP_FAILURE_RETRY(read(fd, buf, count));
		if (c < 0) return c;
		if (0 == c) return len;
		buf += c;
		len += c;
		count -= c;
	} while(count > 0);
	return 0;
}

ssize_t bulk_write(int fd, char *buf, size_t count) {
	int c;
	size_t len = 0;
	do {
		c = TEMP_FAILURE_RETRY(write(fd, buf, count));
		if (c < 0) return c;
		buf += c;
		len += c;
		count -= c;
	} while(count > 0);
	return len ;
}

void SetWrLock(int fd, int start, int len) {
	struct flock l;
	l.l_whence = SEEK_SET;
	l.l_start = start;
	l.l_len = len;
	l.l_type = F_WRLCK;
	if (-1 == TEMP_FAILURE_RETRY(fcntl(fd, F_SETLKW,
					&l)))
	{
		perror("fcntl");
		return;
	}
}

void SetReLock(int fd, int start, int len) {
	struct flock l;
	l.l_whence = SEEK_SET;
	l.l_start = start;
	l.l_len = len;
	l.l_type = F_RDLCK;
	if (-1 == TEMP_FAILURE_RETRY(fcntl(fd, F_SETLKW,
					&l)))
	{
		perror("fcntl");
		return;
	}
}

void SetUnLock(int fd, int start, int len) {
	struct flock l;
	l.l_whence = SEEK_SET;
	l.l_start = start;
	l.l_len = len;
	l.l_type = F_UNLCK;
	if (-1 == TEMP_FAILURE_RETRY(fcntl(fd, F_SETLKW,
					&l)))
	{
		perror("fcntl");
		return;
	}
}
