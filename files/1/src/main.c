#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <assert.h>
#include <fcntl.h>
#include "lib.h"
#include "file.h"

#define MAX_BUFFER_LENGTH 1024 

void sigHandler(int nr)
{
	switch (nr)
	{
		case SIGINT:
			puts("Wait a second, I'm working");
			break;
	}
}

int dirContentLister(int f, char *dir)
{
	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGINT);
	if (sigprocmask(SIG_SETMASK, &set, NULL) < 0)
	{
		perror("sigprocmask");
		return 1;
	}
	debug("dir Desktyptor pliku %d\n", f);
	debug("Otwieram katalog '%s'\n", dir);
	DIR *d = opendir(dir);
	struct dirent *entry = NULL;
	if (d == NULL)
	{
		perror("opendir");
		return 1;
	}

	char buffer[MAX_BUFFER_LENGTH];
	struct stat stats;
	char *types[] = 
	{
		"F",
		"D",
		"S",
		"0"
	};


	char *canon = realpath(dir, NULL);

	do
	{
		errno = 0;
		memset(&buffer, 0, MAX_BUFFER_LENGTH);
		memset(&stats, 0, sizeof(stats));

		entry = readdir(d);
		if (entry == NULL)
			break;


		snprintf (buffer,MAX_BUFFER_LENGTH,
				"%s/%s", dir, entry->d_name);

		if (stat(buffer, &stats) < 0)
		{
			perror("stat");
			break;
		}

		debug("Type: %o\n", stats.st_mode);

		int type = 3;
		if (stats.st_mode & S_IFDIR)
			type = 1;
		if (stats.st_mode & S_IFLNK)
			type = 2;
		if (stats.st_mode & S_IFREG)
			type = 0;


		memset (&buffer, 0, sizeof(buffer));
		
		snprintf (buffer, MAX_BUFFER_LENGTH,
				"%s: %s %s\n", dir, types[type],
				entry->d_name);

		debug ("Line: %s\n",buffer);

//		SetWrLock(f, 0, 0);
	
		if (write(f, buffer, strlen(buffer)) < 0)
		{
			perror("fwrite");
			break;
		}

//		SetUnLock(f, 0,0);


	} while (1);

	if (errno != 0)
	{
		perror ("readdir");
		return 1;
	}

	closedir(d);

	free(canon);

	return 0;
}

int main (int argc, char **argv)
{
	debug("Dir content launched\n");	
	setHandler(SIGINT, sigHandler, 0);

	if (argc < 2)
	{
		puts("Nie podales parametrow");
		return 1;
	}

	debug("openning file %s\n",argv[1]);

	int file = open(argv[1], O_APPEND | O_RDWR | O_CREAT, 0755);
	if (file < 0)
	{
		perror("fopen");
		return 1;
	}

	debug("Deskryptor pliku %d\n", file);

	int n = argc-2;

	for (int i = 0; i < n; i++)
	{
		debug("Tworze %s listera\n", argv[i+2]);
		pid_t lister = fork();
		if (lister < 0)
		{
			perror("fork");
			return 1;
		}
		if (lister == 0)
			exit(dirContentLister(file,argv[i+2]));
	}

	while (1)
	{
		pid_t child;
		TEMP_FAILURE_RETRY(child = waitpid(0, NULL, 0));
		if (child < 0)
		{
			if (errno != ECHILD)
				perror("waitpid");
			break;
		}
	}

	debug("Closing file\n");
	close(file);

	debug("Dir content exited\n");
	return 0;
}
