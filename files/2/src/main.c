#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include "lib.h"
#include "file.h"

#define MAX_BUFFER_LENGTH 1024 

char *trim(char *str)
{
	int start = 0;
	int end = strlen(str)-1;
	while (str[start] <= ' ' && start < end)
		++start;
	while (str[end] <= ' ' && start < end)
		--end;
	str[end+1] = 0;
	return str+start;
}

void parse(char *command)
{
	debug("Command: %s\n", command);
	if (strncmp(command, "F", 1) == 0)
	{
		debug("FILE\n");

		char *fileName = trim(command+2);
		char *sep = strstr(fileName, " ");
		if (sep == NULL)
		{
			fprintf (stderr, "Not supported format\n");
			return;
		}

		sep[0] = 0;
		fileName = trim(fileName);
		int size = atoi(sep+1);

		int fd = open(trim(command+2), O_CREAT | O_RDWR, 0755);
		if (fd < 0)
		{
			perror("open");
			return;
		}

		debug("Rozmiar %d\n", size);

		if (size > 0)
		{

			if (lseek(fd, size-1, SEEK_SET) < 0)
			{
				perror("lseek");
				return;
			}
	
			if (bulk_write(fd, "\0", 1) < 0)
			{
				perror("Bulk_write");
	
			}

			if (close (fd) < 0)
			{
				perror("close");
				return;
			}
		}
	}
	else if (strncmp(command, "S", 1) == 0)
	{
		debug("Symbolic\n");
		char *pos = strstr(command+2, "->");
		if (pos == NULL)
		{
			fprintf (stderr, "Not supported file format\n");
			return;
		}	

		*pos = 0;
		char *first = trim(command+2);
		char *second = trim(pos+2);
		if (symlink(second,first) < 0)
		{
			perror("symlink");
			return;
		}
	}
	else if (strncmp(command, "D", 1) == 0)
	{
		debug("Directory\n");
		if (mkdir (trim(command+2), 0755) < 0)
		{
			perror("mkdir");
			return;
		}
	} 
	else debug("Sth.\n");

}

int main (int argc, char **argv)
{
	debug("Dir content launched\n");	

	if (argc != 3)
	{
		puts ("Nie wywolales poprawnie programu");
		puts ("dir_creator INPUT_FILE DIRNAME");
		return 1;
	}

	debug ("INPUT FILE: %s, DIRNAME: %s\n", argv[1], argv[2]);

	if (mkdir (argv[1], 0755) < 0)
	{
		perror("mkdir");
		return 1;
	}

	int fd = open (argv[2], O_RDONLY);
	if (fd < 0)	
	{
		perror("open");
		return 1;
	}

	int size = lseek(fd, 0, SEEK_END);
	if (size < 0)
	{
		perror("lseek");
		return 1;
	}

	if (lseek(fd, 0, SEEK_SET) < 0)
	{
		perror("lseek");
		return 1;
	}

	debug("Rozmiar pliku %d\n", size);

	char *buffer = (char *)malloc(sizeof(char)*size);

	int size_read = bulk_read(fd, buffer, size);
	if (size_read < 0)
	{
		perror("bulk_read");
		return 1;
	}

	debug("Rozmiar wczytany %d\n", size_read);
	// debug("Dane wczytane: '%s'\n", buffer);

	if (size_read != size)
	{
		printf ("Rozmiar sie nie zgadzaja\n");
		return 1;
	}

	if (close(fd) < 0)
	{
		perror("close");
		return 1;
	}

	if (chdir(argv[1]) < 0)
	{
		perror("chdir");
		return 1;
	}

	debug("Splitting...\n");

	char *saveptr = NULL;

	char *str = strtok_r(buffer, "\n",  &saveptr);
	parse(trim(str));
	while ((str = strtok_r(NULL, "\n", &saveptr)) != NULL)
			parse(trim(str));

	free(buffer);

	debug("Dir content exited\n");
	return 0;
}

