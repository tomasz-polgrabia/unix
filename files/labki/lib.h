#ifndef __LIB_H__
#define __LIB_H__

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdlib.h>

#define ERR(...) if ((__VA_ARGS__) < 0) \
	{ \
		debug("ERR: %s\n", strerror(errno)); \
		exit(EXIT_FAILURE); \
	} 
	
#define ERRNull(...) if ((__VA_ARGS__) == NULL) \
	{ \
		debug("ERRNull: %s\n", strerror(errno)); \
		exit(EXIT_FAILURE); \
	} 	

#ifdef DEBUG
	#define debug(...) plog(__VA_ARGS__)
#else
	#define debug(...)
#endif

void SafeSleep(int k);
void plog(char *fmt, ...);
void HandleChildren();
int setHandler(int nr, void (* handler)(int nr), int flags);
int SafeSleep2(struct timespec t);
void sigChildHandler(int);

#endif

