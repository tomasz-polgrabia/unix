#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <limits.h>
#include <assert.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include "lib.h"
#include "file.h"

#define EXIT_INPUT_FAILURE 1

void usage();
int spawn_childs(int fd, int argc, char **argv);
int childMain (int fd, char *dir);

int main(int argc, char **argv)
{
	setHandler(SIGCHLD, sigChildHandler, 0);
	
		if (argc <= 2)
		{
			usage();
			return EXIT_INPUT_FAILURE;
		}
		
		int n = argc-2;
		
		debug("Liczba katalogow wynosi: %d\n", n);
		debug("Plik wyjsciowy: %s\n", argv[1]);
		
		for (int i = 0; i < n; i++)
			debug("Katalog wejsciowy %d. to %s\n", i, argv[i+2]);
			
		int fd = -1;
		ERR(fd = TEMP_FAILURE_RETRY(open(argv[1], 
			O_CREAT | O_RDWR | O_TRUNC | O_APPEND, 0755)));
			
		ERR(spawn_childs(fd, argc, argv));
		
		debug("Waiting for childs to finish work\n");
		
		while (1)
		{
			pid_t child = waitpid(0, NULL, 0);
			if (child <= 0)
			{
				if (child == 0)
				{
					debug("Tego tu nie powinno byc: %s\n", strerror(errno));
					exit(1);
				}
				
				if (child < 0 && errno == ECHILD)
					break;
					
				debug("Waitpid: %s\n", strerror(errno));
				exit(1);
				
			}
		}
		
		debug("Waiting finished\n");
		
		debug("Closing file descriptor\n");
			
		ERR(close(fd));
		
		return 0;
}

void usage()
{
		fprintf (stderr, "./files out dir1 dir2 ... dirn");
		fprintf (stderr, "n >= 1");
}

int spawn_childs(int fd, int argc, char **argv)
{
		int n = argc-2;
		
		for (int i = 0; i < n; i++)
		{
			debug("Uruchamiam dziecko %i-te\n", i+1);
			pid_t child = fork();
			ERR(child);
			
			if (child == 0)
				exit(childMain(fd,argv[i+2]));
			
		}
		
		return 0;
}

int childMain (int fd, char *dir)
{
		assert(fd >= 0 && dir != NULL);
		debug("File descriptor: %d\n", fd);
		
		char buffer[PATH_MAX];	
	
		DIR *d = opendir(dir);
		ERRNull(d);
		
		debug("Otwarlem katalog %s\n", dir);
		struct dirent* pos = NULL;
		
		do
		{
			errno = 0;
			pos = readdir(d);
			if (pos == NULL && errno != 0)
			{
				fprintf(stderr, "readdir: %s\n", strerror(errno));
				exit(1);
			}
			
			if (pos != NULL)
			{
				debug("Wejscie : %s\n", pos->d_name);
				struct stat stats;
				memset(&stats, 0, sizeof(stats));
				
				snprintf(buffer, PATH_MAX, "%s/%s", dir, pos->d_name);
				char *path = canonicalize_file_name(buffer);
				debug("Canon filename: %s\n", path);
				
				ERR(stat(path, &stats));
				
				if (S_ISREG(stats.st_mode))
				{
					printf ("Regular file: %s\n", path);
					
					int fd2 = open(path, 0);
					ERR(fd2);
					
					char contents[16];
					memset(&contents, 0, sizeof(contents));
				
					int readCounts = bulk_read(fd2, contents, 16);
					ERR(readCounts);
					
					debug("Contents: %s\n", contents);
				
					int writeCounts = -1;
					if (strncasecmp(contents, "xy", 2) == 0)
					{
						debug("TAK\n");
						SetWrLock(fd, 0, 0);
						ERR(writeCounts = bulk_write(fd,  
							contents, strlen(contents)));
						SetUnLock(fd, 0, 0);
						ERR(close(fd2));
					}
				
					close(fd2);
				
					free(path);
				}
				
			}
			
		} while (pos != NULL);
		
		ERR(closedir(d));
		
		debug("Zamknalem katalog\n");
	
		return 0;
}
