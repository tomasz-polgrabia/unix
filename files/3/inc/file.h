#ifndef __FILE_H__
#define __FILE_H__

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

ssize_t bulk_read(int fd, char *buf, size_t count);
ssize_t bulk_write(int fd, char *buf, size_t count);
void SetWrLock(int fd, int start, int len);
void SetReLock(int fd, int start, int len);
void SetUnLock(int fd, int start, int len);

#endif
