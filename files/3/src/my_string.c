#include <my_string.h>

char *trim(char *str)
{
	int start = 0;
	int end = strlen(str)-1;
	while (str[start] <= ' ' && start < end)
		++start;
	while (str[end] <= ' ' && start < end)
		--end;
	str[end+1] = 0;
	return str+start;
}

