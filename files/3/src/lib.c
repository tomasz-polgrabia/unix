#include "lib.h"

void SafeSleep(int k)
{
	int tt,t = k;
	for (tt = t; tt > 0; tt = sleep(tt));
}

void plog(char *fmt, ...)
{
	struct timeval t;
	gettimeofday (&t, NULL);
	va_list args;
	fprintf (stderr,"[%d %lf]: ", getpid(),
			(double)t.tv_sec + ((double)t.tv_usec /(double)1e6)) ;
	va_start(args,fmt);
	vfprintf(stderr,fmt,args);
	va_end(args);
}

void HandleChildren()
{
	int res = -1;
	do
	{
		TEMP_FAILURE_RETRY(res = waitpid(0, NULL, WNOHANG));
	} while (res >= 0 || (res < 0 && errno == EINTR));

	// res < 0 

	if (errno != ECHILD)
	{
		fprintf (stderr, "waitpid failed with error: '%s'\n",
			strerror(errno));
		return;
	}

}

int setHandler(int nr, void (* handler)(int nr), int flags)
{
	struct sigaction action; 
	memset (&action, 0, sizeof(action));
	action.sa_handler = handler;
	action.sa_flags = flags;
	return sigaction(nr, &action,NULL);
}

int SafeSleep2(struct timespec t)
{
	struct timespec t2 = t;
	int result = -1;
	while ((result = nanosleep(&t2,&t2)) < 0 && errno == EINTR)
		printf ("Breaked: remained %ds %luns\n", (int)t2.tv_sec,
				t2.tv_nsec);
	return result;
}


