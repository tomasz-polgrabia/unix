#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <dirent.h>
#include "lib.h"
#include "file.h"
#include "my_string.h"

#define MAX_BUFFER_LENGTH 1024 

#define ERR(...) if ((__VA_ARGS__) < 0) \
{ \
	perror("Error: "); \
	kill(getppid(), SIGRTMIN); \
	exit(1); \
}

#define ERR2(...) if ((__VA_ARGS__) < 0) \
{ \
	perror("Error: "); \
}

#define ErrNULL(...) if ((__VA_ARGS__) == NULL) \
{ \
	perror("Error: "); \
	ERR(kill(getppid(), SIGRTMIN)); \
	exit(1); \
}




sig_atomic_t childCounter = 0;
sig_atomic_t working = 1;

void parentHandler(int nr)
{
	debug("Parent handler started %d\n", nr);
	switch (nr)
	{
		case SIGINT:
			debug("SIGINT handled\n");
//			ERR(kill (0, SIGTERM));
			debug("Waiting to finish childs\n");
			working = 0;
			break;
		case SIGCHLD:
			for (;;)
			{
				int status = 0;
				pid_t child = waitpid(0, &status, WNOHANG);
				if (child < 0)
				{
					if (errno == ECHILD)
						return;
					ERR2(child);
				}

				if (child == 0)
					return;

				childCounter--;

				debug("Child %d handled\n", child);

			}
			break;
		default:
			if (nr == SIGRTMIN)
			{
				debug("SIGRTMIN received\n");
				debug("Child count %d\n", childCounter);
				childCounter--;
				debug("Child count: %d\n", childCounter);
			}
		break;
	}

	debug("Parent handler exited\n");
}

int childMain(char *dir)
{
	debug("Child process started\n");

	debug("Dir to execute %s\n", dir);

	DIR *d = NULL;
	struct dirent* pos = NULL;
	int counter = 0;

	d = opendir(dir);

	sleep(1);

	if (d != NULL)
	{

	do
	{
		errno = 0;
		pos = readdir(d);
		if (pos == NULL && errno != 0)
		{
			perror("rewinddir");
			break;
		}

		debug("Child loop\n");
		debug("Pos: 0x%x\n", pos);

		if (pos != NULL)
		{
			debug("Directory entry: %s\n", pos->d_name);
			char buffer[MAX_BUFFER_LENGTH];
			struct stat stats;
			memset(&stats, 0, sizeof(stats));
			
			snprintf (buffer, MAX_BUFFER_LENGTH, "%s/%s",  dir, pos->d_name);
			char *path = realpath(buffer, NULL);
			debug("Path: %s\n", path);
		
			if (stat(path, &stats) < 0)
			{
				perror("stat");
				free(path);
				continue;
			}

			if (stats.st_mode & S_IFLNK)
			{
				debug("SYMBOLIC %s: %d\n", path, ++counter);
			}


			free(path);
			

		}

	} while (pos != NULL);

	debug("Finished\n");

		if (closedir(d) < 0)
			perror("closedir");

	debug("Closed\n");

	} else 
	{

		perror("blad opendir");
	}

	debug("Falling asleep\n");

	sleep(1);

	printf ("Count of symbolic links %d\n", counter);

	return 0;
}

void help()
{
	fprintf (stderr, "Niepoprawne wywolanie:\n");
	fprintf (stderr, "program n [dirs]\n");

}

int main (int argc, char **argv)
{
	debug("Dir content launched\n");

	sigset_t defaultMask;
	sigset_t waitingMask;

	sigfillset(&defaultMask);
	ERR(sigprocmask(SIG_SETMASK, &defaultMask, NULL));
	sigemptyset(&waitingMask);

	ERR(setHandler(SIGTERM, parentHandler, 0));
	ERR(setHandler(SIGINT, parentHandler, 0));
	ERR(setHandler(SIGCHLD, parentHandler, 0));
	ERR(setHandler(SIGRTMIN, parentHandler, 0));

	if (argc < 2)
	{
		help();
		return 1;
	}

	int n = atoi(argv[1]);

	debug("n: %d\n", n);

	int nDirs = argc-2;

	debug("liczba katalogow: %d\n", nDirs);

	for (int i = 0; i < nDirs; i++)
		debug("Dir%d - %s\n", i, argv[i+2]);

	debug("Spawning direcotries readers\n");

	for (int i = 0; i < nDirs && working; i++)
	{
		debug("Try to spawn %d. reader\n", i);
		debug("Count: %d\n", childCounter);
		while (childCounter >= n && working)
		{
			debug("Waiting for SIGRTMIN\n");
			if (sigsuspend(&waitingMask) < 0 && errno != EINTR)
				ERR(-1);
			debug("Tak\n");
			debug("ChildCounter: %d\n", childCounter);
		}

		if (!working)
			break;

		pid_t child = fork();
		ERR(child);
		if (child == 0)
			exit(childMain(argv[i+2]));
		else
			++childCounter;
	}
	
	debug("Waiting for childs\n");

	for (;;)
	{
		int status = 0;
		pid_t child = TEMP_FAILURE_RETRY(waitpid(0, &status,0));
		if (child < 0)
		{
			if (errno == ECHILD)
				break;
			ERR(child);
		}

		debug("Child %d handled\n", child);

	}

	debug("Dir content exited\n");
	return 0;
}

