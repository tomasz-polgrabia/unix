#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include "file.h"
#include "lib.h"

void usage()
{
	fprintf(stderr, "Niepoprawne uzycie. Uruchom ./files dir1 dir2 \
		dir3 dir4 ... dirn\n");
}

int check(char *fileName)
{
	int fInput = open(fileName, O_RDWR, 0);
	if (fInput < 0)
	{
		fprintf(stderr, "open - %s\n", strerror(errno));
		return 0;
	}
	
	size_t size = lseek(fInput, 0, SEEK_END);
	debug("Rozmiar pliku: %d\n", size);
	size_t pos = lseek(fInput, 0, SEEK_SET);
	if (size == 0 || pos != 0)
		return 0;
	
	char *contents = malloc(sizeof(char)*128);
	ERRNull(contents);
	
	unsigned int toRead = 128;
	if (size < 128)
		toRead = size;
	
	
	unsigned int readCount = bulk_read(fInput, contents, toRead);
	if (readCount < toRead)
	{
		fprintf(stderr, "Cos dziwnego bulk_read wczytal mniejsza \
		liczbe nizby wynikalo to z rozmiaru pliku - %s\n",
			strerror(errno));
		free(contents);
		return 0;
	}
	
	
//	fprintf(stderr, "Zawartosc %s\n", contents);
	
	if (strstr(contents, "a") == NULL)
	{
		free(contents);
		return 0;
	}
	
	free(contents);
	
	ERR(close(fInput));
	return 1;
}

void writeFile(int fd, char *fileName)
{
	char filename[PATH_MAX];
	
	if (!check(fileName))
		return;
	
	
	
	SetWrLock(fd, 0, 0);
	off_t nr = lseek(fd, 0, SEEK_END);
	
	ssize_t res = write(fd, "x", 1);
	if (res == 0)
	{
		fprintf (stderr, "Write nie zapisal odpowiedniej ilosci bajtow \
			, konczymy\n %s", strerror(errno));
	}
	
	if (fsync(fd) < 0)
	{
			SetUnLock(fd, 0, 0);
			return;
	}
	
	SetUnLock(fd, 0, 0);
	
	debug("Zdobylismy numerek %d\n", (int)nr);
	
	snprintf(filename, PATH_MAX, "%d", (int)nr);
	
	int fOutput = open(filename, O_CREAT | O_RDWR | O_TRUNC, 0755);
	unsigned int writeCount = bulk_write(fOutput, fileName, strlen(fileName));
	if (writeCount < strlen(fileName))
	{
		fprintf(stderr, "bulk_write zwrocil mniejsz liczbe \
			zapisywanych bajtow - %s\n", strerror(errno));
		return;
	}
	
	ERR(fOutput);	
	
	return;
}

int childMain(int fd, char *dir)
{
	char fileName[PATH_MAX];
	memset(fileName, 0, sizeof(fileName));
	
	DIR *d = opendir(dir);
	ERRNull(d);
	
	struct dirent* entry = NULL;
	
	do
	{
		errno = 0;
		entry = readdir(d);
		if (entry == NULL)
		{
			if (errno != 0)
				ERR(-1);
			continue;
		}
		
		debug("Entry: %s\n", entry->d_name);
		snprintf(fileName, PATH_MAX, "%s/%s", dir, entry->d_name);
		
		struct stat stats;
		memset(&stats, 0, sizeof(stats));
		
		ERR(stat(fileName, &stats));
		if (S_ISREG(stats.st_mode))
		{
			char *fileCanon = canonicalize_file_name(fileName);
			ERRNull(fileCanon);
			writeFile(fd, fileCanon);
			free(fileCanon);
		}
		
	} while (entry != NULL);
	
	ERR(closedir(d));	
	
	return 0;
}

int main(int argc, char **argv)
{
	setHandler(SIGCHLD, sigChildHandler, 0);
	if (argc <= 1)
	{
		usage();
		return 1;
	}
	
	int n = argc-1;
	debug("Liczba katalogow - %d\n", n);
	
	for (int i = 0; i < n; i++)
		debug("Katalog %d. %s\n", i, argv[i+1]);
		
	int fd = open("counter.txt", O_CREAT | O_APPEND | O_WRONLY | O_TRUNC, 0755);
	ERR(fd);
		
	for (int i = 0; i < n; i++)
	{
		pid_t child = fork();
		ERR(child);
		if (child == 0)
			exit(childMain(fd, argv[i+1]));
	}
	
	
	for (;;)
	{
		int status = 0;
		pid_t child = TEMP_FAILURE_RETRY(waitpid(0, &status, 0));
		if (child <= 0)
		{
			if (errno == ECHILD)
				break;
			ERR(-1);
		}
		
		debug("Child %d handled\n", child);
		
	}
	
	ERR(close(fd));
	
	return 0;
}
