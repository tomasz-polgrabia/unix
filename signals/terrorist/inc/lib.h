#ifndef __LIB_H__
#define __LIB_H__

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#ifdef DEBUG
	#define debug(...) plog(__VA_ARGS__)
#else
	#define debug(...)
#endif

void SafeSleep(int k);
void plog(char *fmt, ...);
void HandleChildren();
int setHandler(int nr, void (* handler)(int nr), int flags);
int SafeSleep2(struct timespec t);

#endif

