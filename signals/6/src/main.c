#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/time.h>
#include "lib.h"

#ifdef DEBUG
	#define debug(fmt, ...) plog(fmt, ##__VA_ARGS__)
#else
	#define debug(...)
#endif

volatile sig_atomic_t handled = 0;

char *sigNames[] = 
{
	"", // 0
	"SIGHUP", // 1
	"SIGINT", // 2
	"SIGQUIT", // 3
	"SIGILL", // 4
	"",  // 5
	"SIGABRT", // 6
	"", // 7
	"SIGFPE", // 8
	"SIGKILL", // 9
	"SIGUSR1", // 10
	"SIGSEGV", // 11
	"SIGUSR2", // 12
	"SIGPIPE", // 13
	"SIGALRM", // 14
	"SIGTERM", // 15
	"SIGUSR1", // 16
	"SIGUSR2", // 17
	"SIGCHLD", // 18
	"SIGCONT", // 19

};

void sigHandler(int nr)
{
	handled = 1;
	int max = sizeof(sigNames) / sizeof(char *);
	if (nr < max) 
		plog ("Otrzymalem sygnal %s o numerze %d\n",
 			   	sigNames[nr], nr);
	else
		plog ("Otrzymalem sygnal o numerze %d\n", nr);
	switch (nr)
	{
		case SIGINT:
			plog ("Wychodze na zyczenie operatora\n");
			exit(0);
			break;
	}
}

int SafeSleep2(struct timespec t)
{
	struct timespec t2 = t;
	int result = -1;
	while ((result = nanosleep(&t2,&t2)) < 0 && errno == EINTR)
		printf ("Breaked: remained %ds %luns\n", (int)t2.tv_sec,
				t2.tv_nsec);
	return result;
}

void block(sigset_t *setOld)
{
	sigset_t set;
	sigfillset(&set);
	sigprocmask(SIG_BLOCK, &set, setOld);
}

void unblock(sigset_t *setOld)
{
	sigset_t set;
	sigfillset(&set);
	sigprocmask(SIG_UNBLOCK, &set, setOld);
}

int main (int argc, char **argv)
{	
	while (1)
	{
		fd_set set;
		FD_ZERO(&set);
		FD_SET(STDIN_FILENO,&set);
		struct timeval timeout;
		memset (&timeout, 0, sizeof(timeout));
		int val = select(STDIN_FILENO+1, &set, NULL, NULL, &timeout);
		if (val < 0)
		{
			perror("select");
			return 1;
		} else
		{
			if (FD_ISSET(STDIN_FILENO, &set))
				debug ("TAK\n");
			else
				debug ("NIE\n");
		}

		debug ("Tick\n");
		SafeSleep(1);

	}

	return 0;
}
