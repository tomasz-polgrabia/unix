#include "lib.h"

#define DEBUG

void SafeSleep(int k)
{
	int tt,t = k;
	for (tt = t; tt > 0; tt = sleep(tt));
}

void plog(char *fmt, ...)
{
	#ifdef DEBUG
	va_list args;
	fprintf (stderr,"[%d]: ", getpid());
	va_start(args,fmt);
	vfprintf(stderr,fmt,args);
	va_end(args);
	#endif
}

int setHandler(int nr, void (* handler)(int nr), int flags)
{
	struct sigaction action; 
	memset (&action, 0, sizeof(action));
	action.sa_handler = handler;
	action.sa_flags = flags;
	return sigaction(nr, &action,NULL);
}

