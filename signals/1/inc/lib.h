#ifndef __LIB_H__
#define __LIB_H__

#define _POSIX_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>

void SafeSleep(int k);
void plog(char *fmt, ...);
int setHandler(int nr, void (* handler)(int nr), int flags);

#endif

